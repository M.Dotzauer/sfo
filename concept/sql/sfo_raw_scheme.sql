
    /* COMMENT SECTION
    Scheme of database for A_bincombs optimisation for 24 h.
    date and time (CET) of creation: '2021-07-18 11:26:23' .
    */

    DROP TABLE IF EXISTS "bincomb";
    CREATE TABLE IF NOT EXISTS "bincomb" (
        "ID"                INTEGER PRIMARY KEY UNIQUE ,
        "bin24h"            TEXT,
        "runtime"           INTEGER,
        "intervals"         REAL,
        "store_min"         REAL,
        "store_max"         REAL,
        "store_delta"       REAL)
		
		
	CREATE INDEX "main_selectors" ON "bincomb" (
	"runtime"	ASC,
	"intervals"	ASC,
	"store_min"	DESC,
	"store_max"	ASC);
        