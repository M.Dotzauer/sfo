"""Module for plotting a perpspective drawing of a 3-dimensional matrix"""

from matplotlib import pyplot as plt


def array_3d(av=None, ai=None, i1=None, i2=None, i3=None):
    """
    Function for a 3D-Matrix.

    Parameters:
    ===========
    av:str
        array variable

    ai:str
        index for array variable

    i1:str
        index #1

    i2:str
        index #2

    i3:str
        index #3

    """

    #conditional parameters for a test run
    if av == None:
        av  = 'A'

    #using array variable also for the elements
    iv  = av.lower()

    if i1 == None:
        i1  = 'i'

    if i2 == None:
        i2  = 'j'

    if i3 == None:
        i3  = 'k'


    #formated text for array variable with indices
    if ai == None:
        A   = f'${av}$='
    else:
        A   = f'${av}_{{{ai}}}$='

    #formatted texts for front array
    fA_ul_t     = f'${iv}_{{0, 0, 0}}$'
    fA_um_t     = '···'
    fA_ur_t     = f'${iv}_{{{i1}, 0, 0}}$'
    fA_ml_t     = '···'
    fA_mr_t     = '···'
    fA_ll_t     = f'${iv}_{{0, {i2}, 0}}$'
    fA_lm_t     = '···'
    fA_lr_t     = f'${iv}_{{{i1}, {i2}, 0}}$'


    #formatted texts for back array
    bA_ul_t     = f'${iv}_{{0, 0, {i3}}}$'
    bA_um_t     = '···'
    bA_ur_t     = f'${iv}_{{{i1}, 0, {i3}}}$'
    bA_ml_t     = '' #'···'
    bA_mr_t     = '···'
    bA_ll_t     = '' #f'${iv}_{{0, {i2}, {i3}}}$'
    bA_lm_t     = '' #'···'
    bA_lr_t     = f'${iv}_{{{i1}, {i2}, {i3}}}$'


    #positions for front array elements
    fA_ul_p     = (0,3)
    fA_um_p     = (1.5, 3)
    fA_ur_p     = (3,3)
    fA_ml_p     = (0, 1.5)
    fA_mr_p     = (3, 1.5)
    fA_ll_p     = (0,0)
    fA_lm_p     = (1.5, 0)
    fA_lr_p     = (3,0)

    fA_pos = [fA_ul_p, fA_um_p, fA_ur_p, fA_ml_p, fA_mr_p, fA_ll_p, fA_lm_p, fA_lr_p]
    fA_txt = [fA_ul_t, fA_um_t, fA_ur_t, fA_ml_t, fA_mr_t, fA_ll_t, fA_lm_t, fA_lr_t]


    bA_txt = [bA_ul_t, bA_um_t, bA_ur_t, bA_ml_t, bA_mr_t, bA_ll_t, bA_lm_t, bA_lr_t]


    fA_shift    = (1, 0)
    a_scale     = 2.5
    dd_diff_fA  = (1 / a_scale, 1 / a_scale)
    bA_diff_fA  = (2 / a_scale, 2  / a_scale)


    dia_dots    = '···'
    dd_pos      = [(1, 4), (4, 4), (1, 1), (4, 1)]


    fig     = plt.figure(f'3D_array-plot_{av}_{ai}-{i1}{i2}{i3}', (4,2))
    ax      = fig.add_subplot(111)

    #left side of the equation
    plt.annotate(A, xy=(0.5, 1), va='center', ha='right')


    #front array
    for n, i in enumerate(fA_txt):
        pos = [x/a_scale for x in fA_pos[n]]
        pos = [pos[0] + fA_shift[0], pos[1] + fA_shift[1]]

        if fA_pos[n][1] ==1.5:
            rot = 90
        else:
            rot = 0
        plt.annotate(i, xy=pos,  va='center', ha='center', rotation=rot)


    #plot diagonal dots
    for p in dd_pos:

        if p == (1,1):
            continue

        else:

            pos = [pos[0] + dd_diff_fA[0], pos[1] + dd_diff_fA[1]]
            pos = [x/a_scale for x in p]
            pos = [pos[0] + fA_shift[0], pos[1] + fA_shift[1]]

            plt.annotate(dia_dots, xy=pos, va='center', ha='center', rotation=45)


    #back array
    for n, i in enumerate(bA_txt):

        pos = [x/a_scale for x in fA_pos[n]]
        pos = [pos[0] + bA_diff_fA[0], pos[1] + bA_diff_fA[1]]
        pos = [pos[0] + fA_shift[0], pos[1] + fA_shift[1]]

        if fA_pos[n][1] ==1.5:
            rot = 90
        else:
            rot = 0
        plt.annotate(i, xy=pos,  va='center', ha='center', rotation=rot)


    figsize =    fig.get_size_inches()


    ax.axis([0, figsize[0], 0, figsize[1]])
    ax.axis('off')
    ax.grid()
    plt.show()