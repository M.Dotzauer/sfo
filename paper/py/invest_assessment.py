"""module with classes and functions for profitability analysis by net-present values"""




#import nessecary libraries
import pandas               as pd
import numpy                as np
import matplotlib.pyplot    as plt

from dataclasses            import dataclass



@dataclass
class Investment_Object():
    """ Create data class for Investment Opjects

    Parameters:
    ==========

    name_de:str
        german description

    name_en:str
        english description

    present_v:float
        present value [€]

    ltime:int
        life time

    maint_spe
        specific maintanence amount (0 - 1)

    maint_abs
        absolute maintanence amount [€/a]
    """

    #initial attributes of an Investment Object
    name_de     : str
    name_en     : str
    present_v   : float
    ltime       : int
    maint_spe   : float = 0
    maint_abs   : float = 0
    residual_v  : bool  = True


    #post init function to compute missing value for absolute maintanence
    def __post_init__(self):
        if self.maint_spe > 0 and self.maint_abs == 0:
            self.maint_abs = self.present_v * self.maint_spe




@dataclass
class Expenditure_Object():
    """ Create data class for Expenditure Opjects

    Parameters:
    ==========

    name_de:str
        german description

    name_en:str
        english description

    exp_pa:float
        annual expenditure [€]

    infl_adj:int = 1
        inflation adjustment

    exp_list:list
        expenditure list (alternatively for axp_an + infl_daj)
    """

    #initial attributes of an Expenditure Object
    name_de     : str
    name_en     : str
    exp_pa      : float
    infl_adj    : float = 1
    exp_list    : list  = None



@dataclass
class Revenue_Object():
    """ Create data class for Revenue Opjects

    Parameters:
    ==========

    name_de:str
        german description

    name_en:str
        english description

    rev_pa:float
        annual revenue [€]

    infl_adj:int
        inflation adjustment

    rev_list
        list of anual revenues (alternatively for rev_pa + infl_adj)
    """

    #initial attributes of a Revenue Object
    name_de     : str
    name_en     : str
    rev_pa      : float
    infl_adj    : float = 1
    rev_list    : list  = None



class Investment_Project():
    """Class for investment Projects"""


    #documentation dictionary for the main attributes
    docd    = { 'poo'       : 'period of observation',
                'ir_eq'     : 'interest rate for equity capital',
                'ir_ex'     : 'intrest rate for external capital',
                'shr_ex'    : 'share of external capital',
                'inflr'     : 'inflation rate' ,
                'shr_ex'    : 'share of external capital',
                'wacc'      : 'weighted averave costs of capital',
                'ir_real'   : 'real intrest rate',
                'acf'       : 'accumulation factor',
                'anf'       : 'annuity factor',
                'pcf'       : 'price change factor',
                'pvf'       : 'present value factor',
                'pdanf'     : 'price dynamic annuity factor',
                }


    #class variables
    def __init__(self, name_de='Investitionsprojekt', name_en='Investment Project',
                    poo=20, ir_eq=0.08, shr_eq=0.3, ir_ex=0.03, inflr=0.02):
        """
        Initialise base class for Investment Projects.

        Parameters:
        ===========

        name_de:str
            german description for the investment project

        name_en:str
            english name for the investment project

        poo:int
            periods of observation

        ir_eq:float
            intrest rate for equity capital

        shr_eq:float
            share of equity capital

        ir_ex:float
            intereset rate for external capital

        inflr:float
            inflation rate

        .names_dict:dict
            dictionary for names of objects (Investments, Expenditures, Revenues)
        """


        #basic parameters for an investment project
        self.name_de    = name_de
        self.name_en    = name_en
        self.poo        = poo
        self.ir_eq      = ir_eq
        self.shr_eq     = shr_eq
        self.ir_ex      = ir_ex
        self.inflr      = inflr
        self.names_dict = {}


        #call seconary calculation within method definitions below
        self.secondary_keyvals()
        self.DFf_capex_init()
        self.DFf_opex_init()
        self.DFf_revs_init()
        self.DFf_npvs_init()
        self.DFf_ants_init()


        #assertations for infaltion rate less or equal to external intrest rate
        assert ir_ex >= inflr, 'intrest rate for external capital should be larger or equal to inflation rate'



    def secondary_keyvals(self):
        """Compute secondary parameters

        Parameters:
        ==========

        .shr_ex
            share of external capital

        .wacc
            weighted average costs of capital (mixed intrest rate)

        .ir_real
            real intrest rate

        .acf:float
            accumulation factor

        .anf:float
            anuity factor

        .ir_real
            real intrest rate

        .pcf:float
            price change factor

        .pvf
            present-value factor

        .pdanf
            price dynamic anuity factor

        """

        #calculation of secondary key values
        self.shr_ex     = 1 - self.shr_eq
        self.wacc       = self.ir_eq * self.shr_eq + self.ir_ex * self.shr_ex
        self.ir_real    = (1 + self.wacc) / (1 + self.inflr) - 1
        self.acf        = 1 + self.wacc
        self.anf        = self.wacc / (1 - pow(self.acf, - self.poo))
        self.pcf        = 1 + self.inflr
        self.pvf        = (1 - pow(self.pcf / self.acf, self.poo)) / (self.acf - self.pcf)
        self.pdanf      = self.anf * self.pvf



    def DFf_capex_init(self):
        """ DataFrame-function for initializing DF_capex (capital expenditures) """
        columns         = ['infl_factor']
        index           = list(range(0, self.poo + 1)) + ['RV']

        irf_real        = [1 * pow(1 + self.inflr, x) for x in range(0, self.poo + 1)] + [1]
        zip_list        = list(zip(irf_real))

        self.DF_capex   = pd.DataFrame(zip_list, columns=columns, index=index)



    def DFf_opex_init(self):
        """ DataFrame-function for initializing DF_opex (operational expenditures)"""
        columns         = []
        index           = list(range(0, self.poo + 1))
        self.DF_opex    = pd.DataFrame(index=index)



    def DFf_revs_init(self):
        """ DataFrame-function for initializing DF_revs (revenues)"""
        columns         = []
        index           = list(range(0, self.poo + 1))
        self.DF_revs    = pd.DataFrame(index=index)



    def DFf_npvs_init(self):
        """ DataFrame-function for initializing DF_npv (net present values for capex, opex and revenues)"""
        columns         = ['rir_factor']
        index           = list(range(0, self.poo + 1))

        irf_real        = [1 * pow(1 + self.ir_real, x) for x in range(0, self.poo + 1)]
        zip_list        = list(zip(irf_real))

        self.DF_npvs    = pd.DataFrame(zip_list, columns=columns, index=index)



    def DFf_ants_init(self):
        """ DataFrame-function for initializing DF_ants (annuities of net present values)"""

        index           = ['type_de', 'type_en', 'name_de', 'name_en', 'npv_total', 'annuity']
        self.DF_ants    = pd.DataFrame(index=index)



    def DFf_capex_from_IO_list(self, IO_list, plot_ricycles=False, lang='en'):
        """DataFrame-function to compute reinvestment cycles and transfer its results plus maintenance to DF_capex"""

        periods     = list(range(0, self.poo +1 ))
        IO_blocks   = []


        for IO in IO_list:
            ri_cycles   = self.poo // IO.ltime
            short_cycle = self.poo % IO.ltime

            if short_cycle > 0:
                residual_t  = IO.ltime - short_cycle
                short_block = [[ri_cycles, self.poo - short_cycle, short_cycle, 'sc']]
                end_tail    = [[ri_cycles, self.poo, residual_t, 'et']]
            else:
                short_block = [[]]
                end_tail    = [[]]

            ri_blocks   = [ [i, i * IO.ltime, IO.ltime, 'fc'] for i in range(0,ri_cycles)] + short_block + end_tail


            IO_blocks.append(ri_blocks)


        #set IO.attributes
        self.IO_list    = IO_list
        self.IO_blocks  = IO_blocks


        #loop over all IO's in the given list
        for i, IO in enumerate(self.IO_list):

            #differenciate between Investments (I) and Maintenece (M) costs
            self.names_dict[f'IOi{i + 1}'] = { 'de' : IO.name_de + ' / I', 'en' : IO.name_en + ' / I'}
            self.names_dict[f'IOm{i + 1}'] = { 'de' : IO.name_de + ' / W', 'en' : IO.name_en + ' / M'}

            new_column_iv       =  f'IOi{i + 1}'
            new_column_mt       =  f'IOm{i + 1}'

            investment_list     = []


            #calculate reinvestment cycles by given observation period and life times of IO's
            for y in range(0, self.poo):

                if y % IO.ltime == 0:
                    investment_list.append(-IO.present_v)
                else:
                    investment_list.append(0)

                #residual value
                if self.IO_blocks[i][-1]  != []:
                    pcf_sc      = pow(self.pcf, self.IO_blocks[i][-2][1])
                    ltime_share = self.IO_blocks[i][-1][2] / IO.ltime
                    RV          = IO.present_v * pcf_sc * ltime_share
                else:
                    RV = 0

            investment_list += [0, RV]

            self.DF_capex[new_column_iv] = self.DF_capex.infl_factor * investment_list
            self.DF_capex[new_column_mt] = self.DF_capex.infl_factor * -IO.maint_abs
            self.DF_capex.loc['RV', new_column_mt] = 0


        #plotting reinvestment cycles
        if plot_ricycles == True:

            color_map   = { 'fc' : 'steelblue',
                            'sc' : 'lightsteelblue',
                            'et' : 'seagreen' }

            lbls = { 'ttl'      : { 'en'    : 'reinvesting cycles for main components',
                                    'de'    : 'Reinvestitionszyklen für Hauptkomponenten'},
                    'xlbl'      : { 'en'    : 'years',
                                    'de'    : 'Jahre'},
                    'ylbl'      : { 'en'    : 'components',
                                    'de'    : 'Komponenten'},
                    'RV_short'  : { 'en'    : 'RV',
                                    'de'    : 'RW'},
                    'full_c'    : { 'en'    : 'full life cyle',
                                    'de'    : 'voller Lebenszyklus'},
                    'short_c'   : { 'en'    : 'reduced life cycle',
                                    'de'    : 'verkürzter Lebenszyklus'},
                    'res_val'   : { 'en'    : 'residual value',
                                    'de'    : 'Restwert'},}


            hight = 2 + len(IO_list) / 4

            fig     = plt.figure('reinvestment_cycles',figsize=[15,hight])
            fig.suptitle(lbls['ttl'][lang], fontsize=16, fontweight='bold')
            ax      = fig.add_subplot(111)

            xticks_pos = []
            xticks_lbl = []

            for i, b in enumerate(self.IO_blocks):

                xticks_pos.append(i)
                if lang == 'en':
                    xticks_lbl.append(IO_list[i].name_en)
                else:
                    xticks_lbl.append(IO_list[i].name_de)


                left = 0
                for sb in b:
                    if sb == []:
                        continue

                    ax.barh(i, sb[2], height=0.66, left=sb[1],
                            color= color_map[sb[3]], edgecolor='dimgrey', zorder=2)

                    if sb[3] != 'et':
                        ax.annotate(f'{sb[0]}', xy=(sb[1] + sb[2] / 2, i), textcoords='data',
                                    va='center', ha='center', fontweight='bold')
                    else:
                        ax.annotate( lbls['RV_short'][lang], xy=(sb[1] + sb[2] / 2, i), textcoords='data',
                                    va='center', ha='center', fontweight='bold')

            #fake items for leged
            ax.barh(0, 0, color=color_map['fc'], label = lbls['full_c'][lang])
            ax.barh(0, 0, color=color_map['sc'], label = lbls['short_c'][lang])
            ax.barh(0, 0, color=color_map['et'], label = lbls['res_val'][lang])

            ax.set_yticks(xticks_pos)
            ax.set_yticklabels(xticks_lbl)
            ax.axes.xaxis.set_tick_params(labelsize=12)
            ax.axes.yaxis.set_tick_params(labelsize=12)
            ax.set_xlabel(lbls['xlbl'][lang], fontsize = 12)
            ax.set_ylabel(lbls['ylbl'][lang], fontsize = 12)

            plt.legend(loc='upper left')
            plt.grid(ls=':', axis='x', zorder=1)
            plt.tight_layout()
            plt.show()



    def DFf_opex_from_EO_list(self, EO_list):
        """ DataFrame-function for transfer a list of Expenditure Objects to DF_opex"""

        self.EO_list    = EO_list


        for i, EO in enumerate(EO_list):

            self.names_dict[f'EO_{i+1}'] = { 'de' : EO.name_de, 'en' : EO.name_en}

            if EO.exp_list != None:
                self.DF_opex[f'EO_{i+1}'] = [-x for x in EO.exp_list]

            else:
                exp_list = []

                for y in list(range(0, self.poo + 1)):
                    if y == 0:
                        exp = 0
                    else:
                        exp = -EO.exp_pa * pow(EO.infl_adj, y)

                    exp_list.append(exp)

                self.DF_opex[f'EO_{i+1}'] = exp_list



    def DFf_revs_from_RO_list(self, RO_list):
        """ DataFrame-function for transfer a list of Expenditure Objects to DF_opex"""

        self.RO_list    = RO_list


        for i, RO in enumerate(RO_list):

            self.names_dict[f'RO_{i+1}'] = { 'de' : RO.name_de, 'en' : RO.name_en}

            if RO.rev_list != None:
                self.DF_revs[f'RO_{i+1}'] = RO.exp_list

            else:
                rev_list = []

                for y in list(range(0, self.poo + 1)):
                    if y == 0:
                        rev = 0
                    else:
                        rev = RO.rev_pa * pow(RO.infl_adj, y)

                    rev_list.append(rev)

                self.DF_revs[f'RO_{i+1}'] = rev_list



    def DFf_npvs_ants_from_cor(self):
        """ DataFrame-function for computing net present values for capex, opex and revenues"""

        #pressing capex (investments as well as maintenence costs)
        for i, c in enumerate(self.DF_capex.columns):
            if i == 0:
                continue
            self.DF_npvs[c]    = self.DF_capex[c] / self.DF_npvs['rir_factor']

        #pressing opex
        for c in self.DF_opex.columns:
            self.DF_npvs[c]    = self.DF_opex[c] / self.DF_npvs['rir_factor']

        #pressing revenues
        for c in self.DF_revs.columns:
            self.DF_npvs[c]    = self.DF_revs[c] / self.DF_npvs['rir_factor']


        #computing annuities
        types_dict = {  'IOi'   : { 'en' : 'Investments',
                                    'de' : 'Investitionen'},
                        'IOm'   : { 'en' : 'Maintenance',
                                    'de' : 'Instandhaltung'},
                        'EO_'   : { 'en' : 'operational expenditures',
                                    'de' : 'Betriebsausgaben'},
                        'RO_'   : { 'en' : 'Revenues',
                                    'de' : 'Einnahmen'} }


        for i, c in enumerate(self.DF_npvs.columns):
            if i == 0:
                continue

            prefix      = c[:3]

            type_de     = types_dict[prefix]['de']
            type_en     = types_dict[prefix]['en']

            name_de     = self.names_dict[c]['de']
            name_en     = self.names_dict[c]['en']

            npv_total   = self.DF_npvs[c].sum()
            annuity     = npv_total * self.anf

            self.DF_ants[c] = [type_de, type_en, name_de, name_en, npv_total, annuity]



    def DFf_ants_plot(self, lang='en'):
        """Plotting function for annuities

        Parameter:
        ==========

        lang:str
            language selector 'en' | 'de'
        """
        names   = []

        for c in self.DF_ants.columns:
            name = self.names_dict[c][lang]
            names.append(name)

        values  = self.DF_ants.loc['annuity',:].values.tolist()


        lbls = { 'ttl'  : { 'en'    : f'annuities for {self.name_en}',
                            'de'    : f'Annuitäten für {self.name_de}'},
                'xlbl'  : { 'en'    : 'years',
                            'de'    : 'Jahre'},
                'ylbl'  : { 'en'    : 'Annuities [€]',
                            'de'    : 'Annuitäten [€]'},
                'blnc'  : { 'en'    : 'net-annuity',
                            'de'    : 'Nettoannuität'},
                'lgd_I' : { 'en'    : 'I: investment' , 'de' : 'I: Investition'},
                'lgd_M' : { 'en'    : 'M: maintenece' , 'de' : 'W: Wartung'}}

        fig     = plt.figure('annuity',figsize=[5,5])
        fig.suptitle(lbls['ttl'][lang], fontsize=16, fontweight='bold')
        ax      = fig.add_subplot(141)

        bottom_pos = 0
        bottom_neg = 0


        for i, v in enumerate(values):

            if v >= 0:
                bottom = bottom_pos
            else:
                bottom = bottom_neg

            ax.bar(0.5, v, width=0.5, bottom=bottom, label=names[i], zorder=1)

            if v >= 0:
                bottom_pos += v
            else:
                bottom_neg += v

        #empty dummy bars for abbraviation declaration
        ax.bar(0.5, 0, color='white', alpha=0, label=lbls['lgd_I'][lang])
        ax.bar(0.5, 0, color='white', alpha=0, label=lbls['lgd_M'][lang])

        ax.scatter(0.5, sum(values), s=120, c='white', marker='o', alpha=0.5, zorder=2)
        ax.scatter(0.5, sum(values), s=80, c='black', marker='x', label=lbls['blnc'][lang], zorder=3)

        ax.set_ylabel(lbls['ylbl'][lang], fontsize = 12)
        ax.set_xlim(0,1)
        ax.xaxis.set_visible(False)

        fig.legend(loc='center left', bbox_to_anchor=(0.4, 0.5))
        plt.grid(ls=':', alpha=0.5)
        plt.tight_layout()
        plt.show()




##  test script
def test_func():
    """Function for testing the class methods"""

    IO1     = Investment_Object('BHKW-Dummy', 'CHPU-dummy', 3500 * 1000, 15, 0.02)
    IO2     = Investment_Object('Gasspeicher-Dummy', 'gas-storage-dummy', 100E3, 8, 0, 5000)
    IO3     = Investment_Object('Wärmespeicher-Dummy', 'heat-storage-dummy', 250E3, 30, 0.02)
    IO_list = [IO1, IO2, IO3]


    EO1     = Expenditure_Object('Betriebsbedingte Zahlungen', 'operational expenditures', 36E3, 1.02)
    EO_list = [EO1]

    RO1     = Revenue_Object('Flexzuschlag', 'flexibility surcharge', 65E3)
    RO2     = Revenue_Object('EPEX-Mehrerlös', 'EPEC additional revenue', 15E3, 1.01)
    RO_list = [RO1, RO2]


    IP = Investment_Project('Investitionsprojekt 0', 'investment project 0')

    IP.DFf_capex_from_IO_list(IO_list, plot_ricycles=True, lang='de')
    IP.DFf_opex_from_EO_list(EO_list)
    IP.DFf_revs_from_RO_list(RO_list)

    IP.DFf_npvs_ants_from_cor()

    IP.DFf_ants_plot(lang='de')