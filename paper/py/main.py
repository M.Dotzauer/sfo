"""Script for all steps of prodcedure to set up the research design and generate the results"""


import supplemental_functions   as smf
from os import path             as osp

ldir    = osp.dirname(__file__)
fig_dir = osp.join(osp.dirname(ldir), 'Figures')


def workflow():

    #create database scheme
    smf.SFO_results_db_setup()


    #create research design for biogas plant variations
    BGP_multi = smf.research_design_BGP_variations()


    #compute research variants based on BGP_multi and write them into sfo_results.db
    smf.define_variants(BGP_multi)



    #compute sfo for 2021-timesries input data (power prices and heat demand profile, 2020 is possible too)
    #based on the variante encoded in the sfo_results.db
    smf.sfo_application(2021, [1,2])


    #analyse sfo performance for solely power price driven (ppd) schedules
    png_path1 = osp.join(fig_dir, 'meta_bincomb_ppd_Smax3.png')
    png_path2 = osp.join(fig_dir, 'meta_ctime_ppd_Smax3.png')
    smf.meta_analysis(1, show=False, save=[png_path1, png_path2])


    #analyse sfo performance for cogeneration driven (cgd) optimization
    png_path1 = osp.join(fig_dir, 'meta_bincomb_cgd_Smax3.png')
    png_path2 = osp.join(fig_dir, 'meta_ctime_cgd_Smax3.png')
    smf.meta_analysis(2, show=False, save=[png_path1, png_path2])


    #compute component costs
    smf.component_costs([1, 2])


    #plot final reults (SFO-PRM-ratio & total annuities)
    png_path_pf = osp.join(fig_dir, 'final_results')
    smf.plot_results(show=False, save=png_path_pf)


    #plot a digsest of the core results
    png_path    =  osp.join(fig_dir, 'results_digest.png')
    smf.results_digest(BGP_multi, CHPU_service_life=40E3, show=False, save=png_path)