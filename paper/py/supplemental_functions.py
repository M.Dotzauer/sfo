"""Collection of scripts for set up the research design and generate results"""

import matplotlib
#matplotlib.use('Agg')

import sqlite3                  as sq3
import pandas                   as pd
import numpy                    as np
import sys

from datetime       import datetime     as dt
from os             import path         as osp
from matplotlib     import pyplot       as plt
from matplotlib     import colors       as clr
from mpl_toolkits.mplot3d import Axes3D as A3D


loc_dir     = osp.dirname(__file__)
par_dir     = osp.dirname(loc_dir)
par_par_dir = osp.dirname(par_dir)
py_dir      = osp.join(par_par_dir, 'py')
csv_dir     = osp.join(par_par_dir, 'csv')


#add python directory of parent-parent directory to PATH-variable
sys.path.insert(0, py_dir)

import invest_assessment        as iam
import sfo_bgp_flex             as flx
import sfo_bgp_assess           as ass
import plant_classes            as pcl


#other directories
rdb_dir     = osp.join(par_dir, 'results')
rdb_path    = osp.join(rdb_dir, 'SFO_results.db')


#create Ligh-Source opject
LS = clr.LightSource


def SFO_results_db_setup():
    """Set up database for storing intermediate and final results"""

    _SQL    = """   DROP TABLE IF EXISTS "optim_objectives";
                    CREATE TABLE IF NOT EXISTS "optim_objectives" (
                        "oo_id"         INTEGER PRIMARY KEY UNIQUE,
                        "name_de"	    TEXT,
                        "short_de"      TEXT,
                        "name_en"       TEXT,
                        "short_en"      TEXT);


                    DROP TABLE IF EXISTS "variants";
                    CREATE TABLE IF NOT EXISTS "variants" (
                        "id"            INTEGER PRIMARY KEY UNIQUE,
                        "oo_id"         INTEGER,
                        "runtime"       INTEGER,
                        "pq"            REAL,
                        "cap"	        REAL,
                        "gs_volume"     REAL,
                        "hs_volume"     REAL,
                        "strokes_max"   INTEGER,
                        "strokes_count" INTEGER,
                        "bc_count"      INTEGER,
                        "ctime"         TEXT );


                    DROP TABLE IF EXISTS "schedules";
                    CREATE TABLE IF NOT EXISTS "schedules" (
                        "id"            INTEGER,
                        "hour"	        INTEGER,
                        "chpu_bin"      INTEGER,
                        "plb_th"        REAL,
                        PRIMARY KEY(id, hour));


                    DROP TABLE IF EXISTS "economics";
                    CREATE TABLE IF NOT EXISTS "economics" (
                        "id"            INTEGER PRIMARY KEY UNIQUE,
                        "sfo_prm_ratio"	REAL,
                        "flex_surcharge"REAL,
                        "epex_extra"    REAL,
                        "fuel_costs"    REAL,
                        "chpu_invest"   REAL,
                        "chpu_maint"    REAL,
                        "gs_capex"      REAL,
                        "hs_capex"      REAL,
                        "total_annuity" REAL);



                    DROP TRIGGER IF EXISTS "annuity_balance";
                    CREATE TRIGGER T_total_annuity
                    AFTER UPDATE ON  economics
                    BEGIN
                        UPDATE economics
                        SET total_annuity = flex_surcharge + epex_extra + fuel_costs + chpu_invest + chpu_maint + gs_capex + hs_capex;
                    END"""


    con     = sq3.connect(rdb_path)
    con.close()
    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()

    con.executescript(_SQL)
    con.commit()
    con.close()

    print(f'create results data base @ {rdb_path}')



def research_design_BGP_variations():

    #define basic rated capacity [kW]
    Pel_brc     = 1700

    #define recent gas storage volume [m³] and heat storage volume [m³]
    GS_0        = 6000
    HS_0        = 300

    #define storage steps (for gas and heat as well)
    S_steps = list(range(0,15))

    #define list of discrete runtimes [1, ..., 24]
    runtimes = list(range(1,25))
    runtimes.reverse()

    #compute power quotients based on runtimes
    PQs = [24 / x for x in runtimes]

    #select 9 rutimes for further investigations
    RT_sel  = runtimes[13:22]
    PQ_sel  = PQs[13:22]

    #compute ranged for gas and heat storage variations
    GS_range    = [ GS_0 + (GS_0  * x / 3) for x in S_steps]
    HS_range    = [ HS_0 + (HS_0  * x / 3) for x in S_steps]

    print(f'create research designs / BGP vatiations')

    return(Pel_brc, RT_sel, PQ_sel, GS_range, HS_range)



def define_variants(BGP_multi):
    """Create research design variants and transfer them into SFO_results.variants"""


    #define obtimization objectives [[oo_id, name_de, short_de, name_en, short_en], ...]
    oo =    [   [1, 'Strompreis-geführte Betriebsweise', 'SP', 'power price driven operation', 'pp'],
                [2, 'Integrierte Strompreis- Wärmebedarfs-geführte Betriebsweise', 'ISW', 'integrated power price and heat demand driven operation', 'iph'] ]


    _SQL    = """ INSERT OR REPLACE INTO optim_objectives(oo_id, name_de, short_de, name_en, short_en)
                    VALUES(?, ?, ?, ?, ?) """


    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()


    con.executemany(_SQL, oo)
    con.commit()



    #variants id consists of 7 integers for 1:optimization objective, 2-3: runtime, 4-5: gas storage, 6-7: heat storage
    Pel_brc     = BGP_multi[0]

    oo_idl      = [i[0] for i in oo]

    variants    = []

    #loop over all optimization objective ids
    for oi in oo_idl:


        #loop over selected runtimes
        for rt in BGP_multi[1]:
            rt_str = f'{rt:02d}'

            #loop over all gas storage steps
            for gs in BGP_multi[3]:
                gs_str = f'{gs/1E3:02.0f}'

                #loop over all heat storage steps
                for hs in BGP_multi[4]:
                    if oi == 1:
                        hs_str  = '00'
                        hs      = 0

                    else:
                        hs_str = f'{hs/1E2:02.0f}'


                    PQ      = 24/rt
                    cap     = Pel_brc * PQ

                    sublist     = [ int(f'{oi}{rt_str}{gs_str}{hs_str}'), oi, rt, PQ, cap, gs, hs]

                    variants.append(sublist)

                    if oi == 1:
                        break


    _SQL    = """ INSERT OR REPLACE INTO variants(id, oo_id, runtime, pq, cap, gs_volume, hs_volume)
                    VALUES(?, ?, ?, ?, ?, ?, ?) """

    con.executemany(_SQL, variants)
    con.commit()

    con.close()

    print(f'create research design variants and write parameters to data base')




#cost functions for main components (CHPU investment, CHPU maintanence, gas storge investment, heat storage investment)
def f_CHPU_invest(Pe, a=784, b=-172521, c=-1.66, type='abs'):
    """ Cost function for CHPU's investment for different capcities [€],

    function
        C_chpu(Pe) = (a * Pe)^b

    Parameters:
    ===========
    x:float
        input parameter, CHPU capacity [kW]

    a:float = 784

    b:float = 172521

    c:float = -1.66

    type:str = 'abs'
        optional switch between absolute ('abs') and specific ('spc') costs.
    """

    sC_CHPU_i   = a + (b / (c * Pe))
    aC_CHPU_i   = sC_CHPU_i * Pe

    if type == 'abs':
        return(aC_CHPU_i)
    else:
        return(sC_CHPU_i)




def f_CHPU_maint(Wel, a=0.9686, b=146.5):
    """ Cost function for specifiv CHPU's maintanence for different capcities [€ / kWh],

    function
        C_chpu_Ms(Pe) = (a + b / Pe) 100

    Parameters:
    ===========
    Wel:float
        input parameter, CHPU electrical work

    a:float = 0.9686

    b:float = 146.5
    """

    C_CHPU_m    = (a + b / Wel) / 1E2

    return(C_CHPU_m)



def f_HS_invest(Vs, a=92.391, b=-1107814, c=-7.1572, type='abs'):
    """ Cost function for heat storage investments for different storage volumes [m³],

    function
        C_hs(Vs) = f(x) = a + (b/ (c * Vs)

    Parameters:
    ===========
    Vs:float
        input parameter, heat storage volume [m³]

    a:float = 92.391

    b:float = -1107814

    c:float = -7.1572

    type:str = 'abs'
        optional switch between absolute ('abs') and specific ('spc') costs.
    """

    sC_HS   = a + (b / (c * Vs))
    aC_HS   = sC_HS * Vs

    if type == 'abs':
        return(aC_HS)
    else:
        return(sC_HS)



def f_GS_invest(Vs, Pe, a=7.8536, b=-20879909, c=-244.37, d=4158218, e=-584.79, type='abs'):
    """
    Cost function for gas storage investments depending on storage volume (Vs) [m³] and CHPU-capacity [kW].

    function
        C_gs(Vs, Pe) = a + (b / (c * Vs) + (d / (e * Pe)

    Parameters:
    ===========
    Vs:float
        input parameter, gas storage volume [m³]

    Pe:float
        input parameter, CHPU installed electrical capacity [kW]

    a:float = 7.85

    b:float = -20879909

    c:float = -244.37

    d:float = 4158218

    e:float = -584.79

    type:str = 'abs'
        optional switch between absolute ('abs') and specific ('spc') costs.
    """
    sC_GS   = a + (b / (c * Vs))  +  (d / (e * Pe))
    aC_GS   = sC_GS * Vs

    if type == 'abs':
        return(aC_GS)
    else:
        return(sC_GS)



def component_costs(oo=[1,None], strokes='runtime_equivalent', flex_surcharge=True, CHPU_service_life=40E3):
    """Calculating the annuties for all considered components

    Parameter:
    ==========

    oo:list = [1, None] | [1, 2]
        optimization objectives
        1=solely power price oriented schedule, 2=multi objective - heat demand and power price

    strokes:str
        indicate if the number of strokes should be considered like runtime euqivalent (1:1)

    flex_surcharge:bool
        argument for in- or exculding the flexibility surcharge according to EEG2021

    CHPU_service_life:
        service life of the CHPU, default value 40.000 hours

    """

    #preset variables for code testing
    vars_d  = {'oo' : [1, None], 'strokes' : 'runtime_equivalent'}
    for v in vars_d.keys():
        if v not in globals() and v not in locals():
            locals()[v] = vars_d[v]

    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()


    _SQL    = """   SELECT          id,
                                    runtime,
                                    cap,
                                    strokes_count,
                                    gs_volume,
                                    hs_volume


                    FROM variants
                    WHERE   oo_id == '{oo1}'
                    OR      oo_id == '{oo2}'

                    """

    DF      = pd.read_sql(_SQL.format(oo1= oo[0], oo2=oo[1]), con)

    #SQL patter for write results
    _SQL        = """   UPDATE      economics
                        SET         flex_surcharge  = {flexs},
                                    chpu_invest     = {chpu_iv},
                                    chpu_maint      = {chpu_mt},
                                    gs_capex        = {gs_cp},
                                    hs_capex        = {hs_cp}
                        WHERE   id == {vi}"""



    #batch processing of all IDs
    for i in DF.index:

        vid         = DF.loc[i, 'id']

        #precalculations for the CHPU investment
        CHPU_cap    = DF.loc[i, 'cap']
        CHPU_inv    = f_CHPU_invest(CHPU_cap)

        #precalculations for the CHPU maintenence
        CHPU_flh    = DF.loc[i, 'runtime'] * 365
        CHPU_ltime  = int(CHPU_service_life // CHPU_flh)
        CHPU_Wel    = CHPU_cap * CHPU_flh
        CHPU_stc    = DF.loc[i, 'strokes_count']

        if strokes == 'runtime_equivalent':
            CHPU_rteq   = CHPU_stc * CHPU_cap
        else:
            CHPU_rteq   = 0

        CHPU_mnt    = f_CHPU_maint(CHPU_cap) * (CHPU_Wel + CHPU_rteq)

        #transfer parameters to an Investment Object (IO)
        IO1         = iam.Investment_Object('BHKW', 'CHPU', CHPU_inv, CHPU_ltime, 0, CHPU_mnt)


        #precalculations for the heat storage and transfer parameters to an Investment Object (IO)
        GS_inv      =  f_GS_invest(DF.loc[i, 'gs_volume'], DF.loc[i, 'cap'])
        IO2         = iam.Investment_Object('GS', 'GS', GS_inv, 10, 0.02)


        #preclculation for the heat storage if one is part of the variant
        if DF.loc[i, 'hs_volume'] > 0:
            HS_inv      = f_HS_invest(DF.loc[i, 'hs_volume'])
            IO3         = iam.Investment_Object('WS', 'HS', HS_inv, 30, 0.02)

            IO_list     = [IO1, IO2, IO3]

        else:
            IO_list     = [IO1, IO2]


        #if flex_surcharge == True create an Revenue Object (RO) for the annual payments of the flexibility surcharge
        if flex_surcharge == True:
            flexs_pa    = CHPU_cap * 65

            RO_list     = [iam.Revenue_Object('Flexzuschlag', 'flexibility surcharge', flexs_pa)]


        else:
            RO_list = [iam.Revenue_Object('Dummy', 'dummy', 0)]


        #create an Investment Project (IP)
        IP          = iam.Investment_Project()

        #put all investment object togehter and calculate capex and NPVs for investment objects
        IP.DFf_capex_from_IO_list(IO_list)
        IP.DFf_revs_from_RO_list(RO_list)
        IP.DFf_npvs_ants_from_cor()

        #get key values
        CHPU_m      = IP.DF_ants.loc['annuity', 'IOi1']
        CHPU_i      = IP.DF_ants.loc['annuity', 'IOm1']
        GS_cpx      = IP.DF_ants.loc['annuity', 'IOi2'] + IP.DF_ants.loc['annuity', 'IOm2']
        if DF.loc[i, 'hs_volume'] > 0:
            HS_cpx      = IP.DF_ants.loc['annuity', 'IOi3'] + IP.DF_ants.loc['annuity', 'IOm3']
        else:
            HS_cpx  = 0

        #transfer results to SFOr_results.economics
        con.execute(_SQL.format(flexs=flexs_pa, vi=vid, chpu_iv=CHPU_i, chpu_mt=CHPU_m, gs_cp=GS_cpx, hs_cp=HS_cpx))
        con.commit()

        #!final calculation of total annuities is done by a Trigger directly within the results database!

        print(f'processing component costs for: {vid}')

    con.close()



def sfo_application( year:str, oo=[1,None], strokes_preset=3):
    """
    Function for sfo application.

    Paramter:
    =========

    oo:list
        obtimization objective [{power price}, {power price & cogeneration}]
        oo = [1,None] | oo = [None, 2] | oo = [1, 2]

    strokes_preset:int = 3
        preset value of maximum strokes, if no possible schedules were found, maximum strokes will incrementally increased.
    """

    ppp_fname_dict  = { 2020 : 'PPP_EPEX_day_ahead_2020.csv',   2021 : 'PPP_EPEX_day_ahead_2021.csv'}
    hlp_fname_dict  = { 2020 : 'HLP_Jena_2020_1M8_075.csv',     2021 : 'HLP_Jena_2021_1M8_075.csv'}


    t0  = dt.now()

    #paths to power-price-profile (ppp) and heat-load-profile (hlp)
    ppp_path    = osp.join(csv_dir, ppp_fname_dict[year])
    hlp_path    = osp.join(csv_dir, hlp_fname_dict[year])



    #create nested lists for power price profile and heat load profile
    ppp, hlp    = flx.ppp_hlp_nl(ppp_path=ppp_path, hlp_path=hlp_path)

    Pth_PLB     = max([max(x) for x in hlp])
    hlp_Pth_max = max([max(x) for x in hlp])
    hlp_Wth_sum = sum([sum(x) for x in hlp])


    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()


    _SQL_oo    = """    SELECT  id      FROM variants
                        WHERE   oo_id == '{oo1}'
                        OR      oo_id == '{oo2}' """


    con_c.execute(_SQL_oo.format(oo1= oo[0], oo2=oo[1]))

    fetch   = con_c.fetchall()
    IDl     = [x[0] for x in fetch]


    _SQL_id     = "SELECT * FROM variants WHERE id == {id}"

    _SQL_vkv    = """   UPDATE variants
                            SET ctime           = '{ct}',
                                strokes_max     = {sm},
                                bc_count        = {bc}
                            WHERE   id=={id}"""

    _SQL_sched  = "INSERT OR REPLACE INTO schedules(id, hour, chpu_bin, plb_th) VALUES(?, ?, ?, ?)"

    _SQL_econ   = "INSERT OR REPLACE INTO economics(id, sfo_prm_ratio, epex_extra, fuel_costs)  VALUES(?, ?, ?, ?)"

    for ID in IDl:


        con_c.execute(_SQL_id.format(id=ID))
        fetch       = con_c.fetchall()
        var_pars    = fetch[0]


        brc         = var_pars[4] / var_pars[3]
        pq          = var_pars[3]
        cap         = var_pars[4]
        gs          = var_pars[5]
        hs          = var_pars[6]

        bgp_code    = f'PQ{pq:.1f}_GS{gs/1E3:02.0f}k'
        ta_de       = 'Variante: ' + bgp_code + '_xx'
        ta_en       = 'variant: ' + bgp_code + '_xx'


        BGP         = pcl.biogas_plant(ta_en, ta_de, brc, cap, 0.4, 0.4, gs)
        BGP.name    = bgp_code


        #set heat supply system object to None if optimization objective is just focused on power prices
        if str(ID)[0] == '1':
            HSS = None

        else:

            HSS     = pcl.heat_supply_system(   ta_en           = f'BGP - {BGP.ta_en} | HSS with {hs:,.0f}m³ HS',
                                                ta_de           = f'BGA - {BGP.ta_de} | WVS mit {hs:,.0f}m³ WS',
                                                Pt_CHPU_aic     = BGP.Pt_CHPU_aic,
                                                Pt_PLB          = Pth_PLB,
                                                Pt_eta_PLB      = 0.85,
                                                HS_vol          = hs,
                                                Fuel_type_en    = 'wood chips',
                                                Fuel_type_de    = 'Holzhackschnitzel',
                                                Fuel_price      = 30,
                                                HLP_Pt_max      = hlp_Pth_max,
                                                HLP_Wt_sum      = hlp_Wth_sum,
                                                HLP_name_en     = f'synthetic hlp - Jena {year}',
                                                HLP_name_de     = f'Synthetisches WLP - Jena {year}')

            #compute HSS capacity
            HSS.f_HS_vol2cap()


        #default strokes limit is 3, but if that is not sufficient for finding suitable schedules try also 3 or 4
        strokesl    = [3, 4, 5, 6]
        for s in strokesl:
            try:
                SFOr       = flx.optimization(BGP, HSS, ppp, hlp, strk_max=s)

                break
            except:
                continue

        bc_count    = SFOr.bc_population
        ctime       = f'{SFOr.SFO_time}'
        con_c.execute(_SQL_vkv.format(ct=ctime, sm=s, bc=bc_count, id=ID))
        con.commit()

        #primary keys for schedules table
        hours       = list(range(1, len(ppp)*24 + 1))
        IDs         = [ID] * len(hours)

        chpu_bin    = list(SFOr.CHPU_binopt.flatten())

        try:
            plb_sched   = list(SFOr.PLB_schedule.flatten())
            hs_course   = list(SFOr.PLB_schedule.flatten())
        except:
            plb_sched   = [None] * len(hours)
            hs_course   = [None] * len(hours)

        nl          = [[ID, h, int(chpu_bin[i]), plb_sched[i]] for i, h in enumerate(hours)]

        con_c.executemany(_SQL_sched, nl)
        con.commit()


        #create plot for SFO results
        plot_path = osp.join(par_dir, 'SFOr_plots', f'SFOr_{ID}.png')
        ass.SFOr_4plots(SFOr, lang='en', show=False, save=plot_path)


        SFOa    = ass.SFO_analytics(SFOr, BGP, 2020)

        if HSS != None:
            al      = [[ID, SFOa.sfo_xtr_spc/SFOa.prm_xtr_spc, SFOa.sfo_xtr_abs, -HSS.Fuel_price * SFOr.PLB_schedule.sum()/1E3]]
        else:
            al      = [[ID, SFOa.sfo_xtr_spc/SFOa.prm_xtr_spc, SFOa.sfo_xtr_abs, 0]]

        con_c.executemany(_SQL_econ, al)
        con.commit()


        con_c.execute("UPDATE variants SET strokes_count = {sr} WHERE id={ID}".format(sr=SFOa.strokes_real, ID=ID))
        con.commit()

        print(f'sfo application for {IDl.index(ID)+1} of {len(IDl)}')


    tt = dt.now() - t0

    print(f'finish sfo application for {len(IDl)} examples in {tt}')




def meta_analysis(oo, strokes_preset=3, lang='en', show=True, save=False):
    """Meta analysis for possible binary combinations for different plant layouts and computations times.

    oo:list
        obtimization objective [{power price}, {power price & cogeneration}]
        oo = [1,None] | oo = [None, 2] | oo = [1, 2]

    strokes_preset:int = 3
        preset value of maximum strokes, variants exceeding the present value will be coloured in pastel shades.

    lang:str
        language specifier 'en' | 'de'

    show:bool
        argument to show the plot

    save:bool / str
        argument to save the plot, if != None, its a file path
    """

    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()


    _SQL    = """   SELECT runtime, gs_volume,
                                    bc_count,
                                    strokes_max,
                                    avg(CAST (strftime('%f', '0'||ctime) AS REAL)) AS ctt
                    FROM variants
                    WHERE oo_id     = {oo}
                    GROUP BY runtime, gs_volume
                    """



    con_c.execute(_SQL.format(oo = oo))

    fetch   = con_c.fetchall()

    DF      = pd.DataFrame(fetch)

    wn      = 1
    dn      = 1000
    sf      = 0.66


    x3      = [i[0] for i in fetch]
    y3      = [i[1] for i in fetch]
    z3      = [0] * len(fetch)

    w3      = [wn * sf] * len(fetch)
    d3      = [dn * sf] * len(fetch)

    bc      = [i[2] for i in fetch]
    ct      = DF[4].values.tolist()


    #alternatig colors
    c1      = 'tab:blue'
    c12     = 'lightsteelblue'
    c2      = 'tab:orange'
    c22     = 'bisque'

    c = c1
    cm = {}
    for i in DF[1].unique():

        cm[i] = c

        if c == c1:
            c = c2
        else:
            c = c1

    DF['cc']    = DF[1].map(cm)

    DF['cc']    = np.where((DF[3] > strokes_preset) & (DF['cc'] == c1), c12, DF['cc'])
    DF['cc']    = np.where((DF[3] > strokes_preset) & (DF['cc'] == c2), c22, DF['cc'])

    cl = DF['cc'].values.tolist()

    oo_str  = { 1 : {   'en' : '- solely power price driven optimization schedules -',
                        'de' : '- ausschließlich strompreisoptimierter Fahrpläne -'},
                2 : {   'en' : '- schedules for power price optimization under heat obligation contraints -',
                        'de' : '- Fahrpläne für Strompreisoptimierung und Wärmelieferverpflichtungen -'}     }

    lbls    = { 'fig1_ttl'   : { 'en' : f'Amounts of binary combinations for different constraints \n{oo_str[oo]["en"]}',
                                 'de' : f'Binärkombinationen für verschiedene Nebenbedingungen \n{oo_str[oo]["de"]}'},
                'fig2_ttl'   : { 'en' : f'Amounts of computation time for different constraints \n{oo_str[oo]["en"]}',
                                 'de' : f'Berechnungsdauer für verschiedene Nebenbedingungen \n{oo_str[oo]["de"]}'},
                'fig_xl'    : {  'en' : 'runtime [h]', 'de' : 'Laufzeit [h]'},
                'fig_yl'    : {  'en' : 'gas storage volume [m³]',
                                 'de' : 'Gasspeichervolumen [m³]'},
                'fig1_zl'    : { 'en' : 'binary combinations [n]',
                                 'de' : 'Binärkombinationen [n]'},
                'fig2_zl'    : { 'en' : 'computation time [s]',
                                 'de' : 'Berechnungsdauer [n]'}   }

    #light source object
    ls = LS(315, 60)

    # setup the figure and axes for binary combinations count
    fig1 = plt.figure('bincomb_counts',figsize=(8, 8))
    fig1.suptitle(lbls['fig1_ttl'][lang])
    ax1 = fig1.add_subplot(111, projection='3d')

    ax1.bar3d(x3, y3, z3, w3, d3, bc, color=cl, lightsource=ls)
    ax1.view_init(elev=20, azim=-25)

    ax1.set_xlim((ax1.get_xlim()[1],ax1.get_xlim()[0]))

    ax1.set_xlabel(lbls['fig_xl'][lang])
    ax1.set_ylabel(lbls['fig_yl'][lang])
    ax1.set_zlabel(lbls['fig1_zl'][lang])
    plt.tight_layout()
    #plt.show()


    # setup the figure and axes for computation times
    fig2 = plt.figure('computation_time',figsize=(8, 8))
    fig2.suptitle(lbls['fig2_ttl'][lang])
    ax1 = fig2.add_subplot(111, projection='3d')

    ax1.bar3d(x3, y3, z3, w3, d3, ct, color=cl, lightsource=ls)
    ax1.view_init(elev=20, azim=-25)

    ax1.set_xlim((ax1.get_xlim()[1],ax1.get_xlim()[0]))

    ax1.set_xlabel(lbls['fig_xl'][lang])
    ax1.set_ylabel(lbls['fig_yl'][lang])
    ax1.set_zlabel(lbls['fig2_zl'][lang])

    plt.tight_layout()


    if show == True and save == False:
        plt.show()

    elif show == True and save != False:
        fig1.savefig(save[0])
        fig2.savefig(save[1])
        plt.show()

    elif show == False and save != False:
        fig1.savefig(save[0])
        fig2.savefig(save[1])
        plt.close(fig1)
        plt.close(fig2)

    else:
        pass



def DF2XYZ(DF_in, X_col, Y_col, Z_col, x_asc=True, y_asc=True):
    """Convert vectors of DataFrame velues into XYZ-array for 3D-plotting.

    Parameters:
    ===========

    DF_in:DataFrame
        Input-DataFrame

    X_col:str
        X column string as first index.

    Y_col:str
        Y column string for second index

    Z_col:str
        Z column string for "hight"-values

    x_asc:bool
        choose sorting rule for x-values, if x_asc == True: sort ascending else: descending

    y_asc:bool
        choose sorting rule for y-values, if x_asc == True: sort ascending else: descending
    """

    #genrating a list of tuples
    Xsteps  = DF_in[X_col].sort_values(ascending=x_asc).unique()
    Ysteps  = DF_in[Y_col].sort_values(ascending=y_asc).unique()

    DF_pvt  = DF_in.pivot(Y_col, X_col, Z_col)
    DF_pvt  = DF_pvt.sort_index(axis=1, level=1, ascending=x_asc)
    DF_pvt  = DF_pvt.sort_index(axis=0, level=1, ascending=y_asc)

    Z       = DF_pvt.values



    #create array of the shape Xsteps x Ysteps
    X, Y    = np.meshgrid(Xsteps, Ysteps)


    #return arrays
    return(X, Y, Z)




def plot_results(show=True, save=False):
    """ Results plots.

    Parameters:
    ===========

    show:bool
        argument to show the plot

    save:bool / str
        argument to save the plot, if != None, its a file path
        """


    #fontsize parameters
    fs_S    = 11
    fs_M    = 12
    fs_L    = 14
    lpad    = 10


    #parameters for contour plots(cp)
    cp_levels   = 25
    cp_lincol   = cp_levels * ['black']


    #SQL query for SFOr_results.db
    _SQL    = """   SELECT          id,
                                    cap,
                                    gs_volume,
                                    hs_volume,
                                    sfo_prm_ratio,
                                    epex_extra,
                                    total_annuity


                    FROM economics
                        JOIN variants USING (id) """


    #create database connection and read query results to a DataFrame
    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()
    DF      = pd.read_sql(_SQL, con)

    #convert data types to avoid rounding errors, which could make problems to find max_loc
    DF = DF.astype({'cap' : 'int', 'gs_volume' : 'int', 'hs_volume' : 'int', 'epex_extra' : 'int', 'total_annuity' : 'int'})

    #================single plots of SFO-PRM-ratio & total anuities for soley power price driven variants=====

    #subset for solely power price driven schedules (ppd)
    DF_ppd = DF.query('hs_volume == 0')


    #figure 1 for sfo-prm-ratios
    X, Y, Z     = DF2XYZ(DF_ppd, 'cap', 'gs_volume', 'sfo_prm_ratio')

    fig1     = plt.figure('results_sfo-prm-ratio', figsize=[14, 6], dpi=100)
    fig1.suptitle('SFO-PRM-ratio for solely power price driven schedules', fontsize=fs_L)

    #left: 3D surface plot
    ax1     = fig1.add_subplot(1,2,1, projection='3d')
    surf    = ax1.plot_surface(X/1E3, Y/1E3, Z, cmap='plasma', lw=0.5, edgecolor='black', vmin=0, vmax=1)
    ax1.set_zlim3d([0,1])
    ax1.set_title('3D-surface plot', fontsize=fs_M)
    ax1.view_init(20, -120)
    ax1.set_xlabel('CHPU cpacity $P_{e}$ [kW * 10³]', fontsize=fs_M, labelpad=lpad)
    ax1.set_ylabel('storage volume $V_{s}$ [m³ * 10³]', fontsize=fs_M, labelpad=lpad)
    ax1.set_zlabel('SFO-PRM-ratio $SPr$ [-]', fontsize=fs_M, labelpad=lpad)
    ax1.tick_params(labelsize=fs_S)


    #right: 2D contour subplot
    ax2     = fig1.add_subplot(1,2,2)
    cntl    = ax2.contour(X/1E3, Y/1E3, Z, levels=cp_levels, colors=cp_lincol, alpha=0.5, linewidths=0.5)
    cntf    = ax2.contourf(X/1E3, Y/1E3, Z, levels=cp_levels, cmap='plasma')
    ax2.set_title('2D-contour plot', fontsize=fs_M)
    ax2.set_xlabel('CHPU cpacity $P_{e}$ [kW * 10³]', fontsize=fs_M, labelpad=lpad)
    ax2.set_ylabel('storage volume $V_{s}$ [m³ * 10³]', fontsize=fs_M, labelpad=lpad)
    ax2.tick_params(labelsize=fs_S)

    cbar = fig1.colorbar(cntf, shrink=1, aspect=10, pad=0.15)
    cbar.ax.tick_params(labelsize=fs_S)

    plt.tight_layout()


    #figure2 for total annuities
    X, Y, Z     = DF2XYZ(DF_ppd,  'cap', 'gs_volume', 'total_annuity', x_asc=True)

    zmin    = Z.min()
    zmax    = Z.max()
    zdelta  = zmax - zmin
    ztop    = (zmax + zdelta * 0.05) / 1E6
    zbottom = (zmin - zdelta * 0.05) / 1E6


    max_loc = DF_ppd.query(f'total_annuity == {zmax}')[['cap', 'gs_volume']].values.tolist()[0]
    max_loc = [x/1E3 for x in max_loc]


    fig2     = plt.figure('results_total_annuity', figsize=[14, 6], dpi=100)
    fig2.suptitle('total annuities for solely power price driven schedules', fontsize=fs_L)

    #left: 3D surface plot
    ax1     = fig2.add_subplot(1,2,1, projection='3d')
    surf    = ax1.plot_surface(X/1E3, Y/1E3, Z/1E6, cmap='plasma', lw=0.5, edgecolor='black', alpha=1)
    ax1.set_zlim3d([zbottom, ztop])
    ax1.set_title('3D-surface plot', fontsize=fs_M)
    ax1.set_xlabel('CHPU cpacity $P_{e}$ [kW * 10³]', fontsize=fs_M, labelpad=lpad)
    ax1.set_ylabel('storage volume $V_{s}$ [m³ * 10³]', fontsize=fs_M, labelpad=lpad)
    ax1.set_zlabel('total annuity $a_{tot} \; \mathrm{[10^{6} €]}$', fontsize=fs_M, labelpad=lpad)
    ax1.tick_params(labelsize=fs_S)
    ax1.view_init(20, -120)



    #right: 2D contour plot
    ax2     = fig2.add_subplot(1,2,2)
    cntl    = ax2.contour(X/1E3, Y/1E3, Z/1E6, levels=cp_levels, colors=cp_lincol, alpha=0.5, linewidths=0.5)
    cntf    = ax2.contourf(X/1E3, Y/1E3, Z/1E6, levels=cp_levels, cmap='plasma', alpha=1)
    ax2.set_title('2D-contour plot', fontsize=fs_M)
    ax2.set_xlabel('CHPU cpacity $P_{e}$ [kW * 10³]', fontsize=fs_M, labelpad=lpad)
    ax2.set_ylabel('storage volume $V_{s}$ [m³ * 10³]', fontsize=fs_M, labelpad=lpad)
    ax2.tick_params(labelsize=fs_S)

    ax2.scatter(*max_loc, s=50, color='r')
    ax2.annotate(f'max \n{zmax/1E6:.2f}M€', max_loc, ha='right', va='top', fontsize=fs_M)

    cbar = fig2.colorbar(cntf, shrink=1, aspect=10, pad=0.15)
    cbar.ax.tick_params(labelsize=fs_S)

    plt.tight_layout()

    #=============3x3 plot array of SFO-PRM-ratio & total anuities for cogeneration driven schedules===============

    #subset for multiopjective schedules (mod)
    DF_cgn = DF.query('hs_volume > 0')


    #intervals for pivoting raw data
    cap_steps   = DF_cgn.cap.sort_values().unique()
    gsv_steps   = DF_cgn.gs_volume.sort_values().unique() / 1E3
    hsv_steps   = DF_cgn.hs_volume.sort_values().unique() / 1E3


    #figure 3plotting SFO-PRM-ratios

    fig3, axs = plt.subplots(3, 3, sharex='col', sharey='row', figsize=(11,9))
    fig3.suptitle('array of SFO-PRM-ratios by discrete CHPU-capacities', fontsize=fs_L)

    for c in range(0,3):

        for r in range(0,3):

            cap     = cap_steps[c + r * 3]

            DF_sel  = DF_cgn.query(f'cap == {cap}')
            DF_pvt  = DF_sel.pivot('gs_volume', 'hs_volume', 'sfo_prm_ratio')

            ax =    axs[r, c].contourf(gsv_steps, hsv_steps, DF_pvt.T.values, cmap='plasma',
                                    levels=cp_levels, vmin=0.5, vmax=1)
            cp =    axs[r, c].contour(gsv_steps, hsv_steps, DF_pvt.T.values, alpha=0.5,
                                    levels=cp_levels, colors=cp_lincol, vmin=0.5, vmax=1, linewidths=0.5)

            axs[r, c].set_title(f'$P_{{el}} = $ {cap/1E3:.1f}MW')

            if r == 2 and c ==1:
                axs[r, c].set_xlabel('gas storage volume $V_{GS} \; \mathrm{[10^{3} m³]}$', fontsize=fs_M)

            if c == 0 and r==1:
                axs[r, c].set_ylabel('heat storage volume $V_{HS} \; \mathrm{[10^{3} m³]}$', fontsize=fs_M)


    cbar    = fig3.colorbar(ax, ax=axs.ravel().tolist())
    cbar.set_label('SFO-PRM-ratio [-]', fontsize=fs_M)

    #plt.subplots_adjust(left=0.1, right=0.80)


    #plotting total annuities
    zmin    = DF_cgn.total_annuity.min()
    zmax    = DF_cgn.total_annuity.max()

    DF_cgn_max_ta   = DF_cgn.groupby(['cap'])['total_annuity'].max()

    fig4, axs = plt.subplots(3, 3, sharex='col', sharey='row', figsize=(10,9))
    fig4.suptitle('array of total annuties by discrete CHPU-capacities', fontsize=fs_L)

    for c in range(0,3):

        for r in range(0,3):

            cap     = cap_steps[c + r * 3 ]
            max_ta  = DF_cgn_max_ta[cap]

            DF_sel  = DF_cgn.query(f'cap == {cap}')
            DF_pvt  = DF_sel.pivot('gs_volume', 'hs_volume', 'total_annuity')

            ax =    axs[r, c].contourf(gsv_steps, hsv_steps, DF_pvt.T.values, cmap='plasma',
                                    levels=cp_levels)
            cp =    axs[r, c].contour(gsv_steps, hsv_steps, DF_pvt.T.values, alpha=0.5,
                                    levels=cp_levels, colors=cp_lincol, linewidths=0.5)


            max_loc= DF_sel.query(f'total_annuity == {max_ta}')[['hs_volume', 'gs_volume']].values.tolist()[0]
            max_xy = [x/1E3 for x in max_loc]
            axs[r, c].scatter(max_xy[1], max_xy[0], color='red', s=50)
            txt_xy = [max_xy[1], max_xy[0] - 0.05]
            axs[r, c].annotate(f'max \n{max_ta/1E6:.2f}M€', txt_xy, ha='center', va='top', fontsize=fs_M)

            axs[r, c].set_title(f'$P_{{el}} = $ {cap/1E3:.1f}MW')

            if r == 2 and c ==1:
                axs[r, c].set_xlabel('gas storage volume $V_{GS} \; \mathrm{[10^{3} m³]}$', fontsize=fs_M)

            if c == 0 and r==1:
                axs[r, c].set_ylabel('heat storage volume $V_{HS} \; \mathrm{[10^{3} m³]}$', fontsize=fs_M)

    # cbar    = fig4.colorbar(ax, ax=axs.ravel().tolist())
    # cbar.set_label('total annuity $a_{tot} \; \mathrm{[10^{6} €]}$', fontsize=fs_M)



    if show == True and save == False:
        plt.show()

    elif show == True and save != False:
        fig1.savefig(save + '_SPr_pprice.png')
        fig2.savefig(save + '_ta_pprice.png')
        fig3.savefig(save + '_SPr_cogen.png')
        fig4.savefig(save + '_ta_cogen.png')
        plt.show()

    elif show == False and save != False:
        fig1.savefig(save + '_SPr_pprice.png')
        fig2.savefig(save + '_ta_pprice.png')
        fig3.savefig(save + '_SPr_cogen.png')
        fig4.savefig(save + '_ta_cogen.png')
        plt.close(fig1)
        plt.close(fig2)
        plt.close(fig3)
        plt.close(fig4)

    else:
        pass




def results_digest(BGP_multi, CHPU_service_life=40E3, subplot_table=False, show=True, save=False):
    """
    Function for create an 3 by 1 array plot for the digest of core results.

    Parameters:
    ===========

    BGP_multi:tuple
        data sets of multiple biogas plant variants (P_rated, daily runtimes, power quotients, gas storage volumes, heat storage volumes)

    CHPU_service_life:int
        Service life of the CHPU in terms of full load hours


    subplot_table:bool
        argument for adding an optional subplot for the result digests

    show:bool
        argument to show the plot

    save:bool / str
        argument to save the plot, if != None, its a file path
    """

    #fontsize parameters
    fs_S    = 11
    fs_M    = 12
    fs_L    = 14
    lpad    = 10



    Pe_rated    = BGP_multi[0]

    PQ_steps    = BGP_multi[2]

    CHPU_steps  = [int(Pe_rated * x) for x in  PQ_steps]
    CHPU_MW     = [x/1E3 for x in CHPU_steps]

    CHPU_rivc   = [((20 / (CHPU_service_life // (x * 365)) - 1E-12) // 1) + 1   for x in BGP_multi[1]]


    #SQL query for SFOr_results.db
    _SQL    = """   SELECT          id,
                                    cap,
                                    gs_volume,
                                    hs_volume,
                                    sfo_prm_ratio,
                                    epex_extra,
                                    total_annuity


                    FROM economics
                        JOIN variants USING (id) """


    #create database connection and read query results to a DataFrame
    con     = sq3.connect(rdb_path)
    con_c   = con.cursor()
    DF      = pd.read_sql(_SQL, con)

    #convert data types to avoid rounding errors, which could make problems to find max_loc
    DF = DF.astype({'cap' : 'int', 'gs_volume' : 'int', 'hs_volume' : 'int', 'epex_extra' : 'int', 'total_annuity' : 'int'})


    #loop for optional configs at power price only
    ppo     = []

    for c in CHPU_steps:
        DF_qry  = DF.query(f'cap == {c} & hs_volume == 0')
        optim   = DF_qry[DF_qry['total_annuity'] == DF_qry['total_annuity'].max()]

        ppo.append([optim.cap.values[0]/1E3, optim.gs_volume.values[0]/1E3, optim.total_annuity.values[0]/1E6])

    ppo_max = DF.query('hs_volume == 0')['total_annuity'].max()/1E6
    ppo_min = DF.query('hs_volume == 0').groupby(['cap'])['total_annuity'].max().min()/1E6
    ppo_mdn = ppo_max - ((ppo_max - ppo_min) / 2)


    #loop for optimal configs at cogeneration
    cgo     = []

    for c in CHPU_steps:
        DF_qry  = DF.query(f'cap == {c} & hs_volume > 0')
        optim   = DF_qry[DF_qry['total_annuity'] == DF_qry['total_annuity'].max()]

        cgo.append([optim.cap.values[0]/1E3, optim.gs_volume.values[0]/1E3, optim.hs_volume.values[0]/1E3, optim.total_annuity.values[0]/1E6])

    cgo_max = DF.query('hs_volume > 0')['total_annuity'].max()/1E6
    cgo_min = DF.query('hs_volume > 0').groupby(['cap'])['total_annuity'].max().min()/1E6
    cgo_mdn = cgo_max - ((cgo_max - cgo_min) / 2)


    steps   = list(range(0, len(PQ_steps)))

    n_sub_plots = 3
    fig_heihgt  = 9

    if subplot_table == True:
        n_sub_plots += 1
        fig_heihgt  = fig_heihgt/3*4

    fig = plt.figure(figsize=(12, fig_heihgt))
    fig.suptitle('digest of core results', fontsize=fs_L)

    ax1 = fig.add_subplot(n_sub_plots,1,1)
    ax1.set_title('reinvestment cxyles for CHPU during 20 years', fontsize=fs_M)
    ax1.bar(steps, CHPU_rivc, width=0.8)
    ax1.set_xticks(steps)
    ax1.set_xticklabels([] * len(steps))
    ax1.set_ylabel('reinvestment cylces [-]')
    ax1_xlim = ax1.get_xlim()
    ax1.grid(axis='x', ls=':', alpha=0.75, zorder=0)


    ax2     = fig.add_subplot(n_sub_plots,1,2)
    ax2.set_title('total annuities  for solely power price driven schedlues | annotations for optimal gas storage volume (oGS)', fontsize=fs_M)
    for i, v in enumerate(ppo):
        ax2.bar(i + 0.2, v[2], color='grey', width=0.4)
        GS_str = f'{v[1]*1E3:.0f} m³'
        ax2.annotate(f'oGS: {GS_str}', (i - 0, ppo_mdn), rotation=90, ha='right', va='center', fontsize=fs_M)

    ax2.set_xticks(steps)
    ax2.set_xticklabels([] * len(steps), fontsize=fs_S)
    ax2.set_ylabel('total annuities [M€]', fontsize=fs_M)
    ax2.set_xlim(ax1_xlim)
    ax2.grid(axis='x', ls=':', alpha=0.75, zorder=0)


    ax3     = fig.add_subplot(n_sub_plots,1,3)
    ax3.set_title('total annuties for cogeneration schedules | annotations for optimal heat (oHS) and gas storage volumes (oGS)', fontsize=fs_M)
    for i, v in enumerate(cgo):
        ax3.bar(i + 0.2, v[3], color='orange', width=0.4)
        GS_str = f'{v[1]*1E3:.0f} m³'
        HS_str = f'{v[2]*1E3:.0f}  m³'
        ax3.annotate(f'oHS: {HS_str} \noGS: {GS_str} ', (i - 0, cgo_mdn), rotation=90, ha='right', va='center', fontsize=fs_M)

    ax3.set_xticks(steps)
    ax3.set_xticklabels([round(x, 2) for x in PQ_steps], fontsize=fs_S)
    ax3.set_xlabel('power quotient', fontsize=fs_M)
    ax3.set_ylabel('total annuities [M€]', fontsize=fs_M)
    ax3.set_xlim(ax1_xlim)
    ax3.grid(axis='x', ls=':', alpha=0.75, zorder=0)


    if subplot_table == True:
        ax4     = fig.add_subplot(n_sub_plots,1,4)


        tab_rows= [ r'$ \mathrm{PQ}$',
                    r'$ \mathrm{P_{el} [MW]}$',
                    r'$ \mathrm{n_{RIC\_CHPU}}$',
                    r'$ \mathrm{a_{opt\_power}[M€]}$',
                    r'$ \mathrm{GS_{opt\_power}[m³]}$',
                    r'$ \mathrm{a_{opt\_cogen}[M€]}$',
                    r'$ \mathrm{GS_{opt\_cogen}[m³]}$',
                    r'$ \mathrm{HS_{opt\_cogen}[m³]}$' ]

        nl_tab  =  [ [  f'{x:.2f}' for x in PQ_steps],
                        CHPU_MW,
                        CHPU_rivc,
                        [f'{x[2]:.2f}' for x in ppo],
                        [f'{x[1]*1E3:,.0f}' for x in ppo],
                        [f'{x[3]:.2f}' for x in cgo],
                        [f'{x[1]*1E3:,.0f}' for x in cgo],
                        [f'{x[2]*1E3:,.0f}' for x in cgo],]


        ax4.table( nl_tab, rowLabels=tab_rows, edges='open', loc='center', cellLoc='left', bbox=[0.075,0.3,0.925,0.67])
        ax4.set_title('data table')
        ax4.axis('off')
        ax4.axis('tight')


        #create DataFrame by table_data
        xl_path = osp.join(rdb_dir, 'results_digest.xlsx')
        DF_rows = ['PQ', 'P_el', 'n_RIC', 'a_op', 'GS_op', 'a_oc', 'GS_oc', 'HS_oc']
        DF_cols = [f'rt={x}h' for x in BGP_multi[1]]
        DF_overview = pd.DataFrame(nl_tab, index=DF_rows, columns=DF_cols)
        DF.to_excel(xl_path, sheet_name='overview')


    plt.tight_layout()

    if show == True and save == False:
        plt.show()

    elif show == True and save != False:
        fig.savefig(save)
        plt.show()

    elif show == False and save != False:
        fig.savefig(save)
        plt.close(fig)

    else:
        pass


