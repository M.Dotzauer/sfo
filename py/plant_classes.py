"""Module for cogeneration-heat-and-power (CHP) plant classes"""



import matplotlib.pyplot    as plt
import matplotlib.image     as img
import os.path              as osp

from dataclasses            import dataclass

##dir and path variables
local_dir           = osp.dirname(__file__)         #local directory
png_dir             = osp.join(osp.dirname(local_dir), 'png')    #directory for png


#fontsize parameters
fs_S    = 12
fs_M    = 14
fs_L    = 16
fs_XL   = 18
fs_ring = 20
lpad    = 10



##supprot functions
def radial_chart_1s(t_hrs:float, lang='en', color_map='Blues', labels=False, show=False, png=None):
    """
    Function for plotting a radial chart (ring diagram) with 1 segment.

    Parameters:
    -----------
    t_hrs:float
        time amount in hours, number would rounded to 1 digit, values could range between 0 to 24.

    Optional Parameters:
    --------------------
    lang='en'
        language selector. here used for different decimal separators: {'en' : '.', 'de' : ','}

    color_map='Blues'
        color map for the chart, best fit would be achieved by 'Greys', 'Prupels', 'Blues',
        'Greens', 'Oranges', 'Reds'

    labels=False
        Switch for radial labels of hours of 24, 6, 12 and 18.

    show=False
        Switch for showing te figure, suppressed by default.

    png=False
        Switch for save figure as png, suppressed by default. If not False, Parameter is used for
        file path (file dir + file name)
    """

    import matplotlib.pyplot as plt


    #basic styling parameter
    cmap        = plt.get_cmap(color_map)
    ring_width  = 0.08
    radius_1    = 0.3


    #creating single ring segments as list
    ring_array  = [t_hrs, 24-(t_hrs)]


    #set up figure
    fig, ax     = plt.subplots(figsize=[3,3])

    #ploting
    ring_1, _   = ax.pie(ring_array,radius=radius_1,
                    colors=[cmap(0.9), cmap(0.15)],
                    startangle = 90,
                    counterclock = False)
    plt.setp( ring_1, width=ring_width, edgecolor='white')


    #add labels and format plots
    label=str(round(t_hrs,1)) + ' h' + '\n' + '(PQ=' +  str(round(24/t_hrs, 2)) + ')'
    if lang=='de':
        label=label.replace('.', ',')
    plt.text(0, 0, label, fontsize = fs_ring, ha='center', va='center', )

    #optional radial labeling
    if labels==True:
        plt.text(0, radius_1 + ring_width/2, '24', fontsize = 12, ha='center', va='center')
        plt.text(radius_1 + ring_width/2, 0, '6', fontsize = 12, ha='center', va='center')
        plt.text(0, -radius_1 - ring_width/2, '12', fontsize = 12, ha='center', va='center')
        plt.text(-radius_1 - ring_width/2, 0, '18', fontsize = 12, ha='center', va='center')


    ax.axis('equal')
    plt.margins(0,0)
    plt.autoscale('enable')


    if show==True:  #switch for show the figure
        plt.show()


    if png!=None:   #switch for save the figure
        plt.savefig(png)


    #return(plt)    #could be used if the plot-object should be used as a return
    plt.close()     #close the plot if the function is called by another instance


def radial_chart_3s(t_hrs, color_map = 'Oranges', lang='en', labels=False, show=False, png=False):
    """
    Function for plotting a radial chart (ring diagram) with 3 segments (one for each day).

    Parameters:
    -----------
    t_hrs:float
        time amount in hours, number would rounded to 1 digit, values could range between 0 to 72.

    Optional Parameters:
    --------------------
    lang='en'
        language selector. here used for different decimal separators: {'en' : '.', 'de' : ','}

    color_map='Oranges'
        color map for the chart, best fit would be achieved by 'Greys', 'Prupels', 'Blues',
        'Greens', 'Oranges', 'Reds'

    labels=False
        Switch for radial labels of hours of 24, 6, 12 and 18.

    show=False
        Switch for showing te figure, suppressed by default.

    png=False
        Switch for save figure as png, suppressed by default. If not False, Parameter is used for
        file path (file dir + file name)
    """

    import matplotlib.pyplot as plt


    #basic styling parameters
    cmap        = plt.get_cmap(color_map)
    ring_width  = 0.05
    radius_1    = 0.3
    radius_2    = radius_1 - ring_width
    radius_3    = radius_2 - ring_width


    #create 3 ring segments, depending of input values (1, 2 or 3 days or rather 24, 48, 72 hours)
    if t_hrs > 48:
        ring_array  = [[24,0],[24, 0],[t_hrs-48, 24-(t_hrs-48)]]
    elif t_hrs > 24:
        ring_array  = [[24,0],[t_hrs-24, 24-(t_hrs-24)],[0,24]]
    else:
        ring_array  = [[t_hrs, 24-t_hrs],[0, 24],[0, 24]]


    # create figure
    fig, ax     = plt.subplots(figsize=[3,3])


    # plot rings
    ring_1, _ = ax.pie(ring_array[0],radius=radius_1, colors=[cmap(0.9), cmap(0.15)],
                    startangle = 90, counterclock = False)
    plt.setp( ring_1, width=ring_width, edgecolor='white')

    ring_2, _ = ax.pie(ring_array[1], radius=radius_2, colors=[cmap(0.6), cmap(0.05)],
                            startangle = 90, counterclock = False)
    plt.setp(ring_2, width=ring_width, edgecolor='white')

    ring_3, _ = ax.pie(ring_array[2], radius=radius_3, colors=[cmap(0.3), cmap(0.05)],
                            startangle = 90, counterclock = False)
    plt.setp(ring_3, width=ring_width, edgecolor='white')


    # add labels and format plots
    label=str(round(t_hrs,1)) + ' h' + '\n' + str(round(t_hrs/24,1)) + ' d'
    if lang=='de':
        label=label.replace('.', ',')
    plt.text(0, 0, label, ha='center', va='center', fontsize = fs_ring)

    if labels==True:
        plt.text(0, radius_1 + ring_width/2, '24', fontsize = 12, ha='center', va='center')
        plt.text(radius_1 + ring_width/2, 0, '6', fontsize = 12, ha='center', va='center')
        plt.text(0, -radius_1 - ring_width/2, '12', fontsize = 12, ha='center', va='center')
        plt.text(-radius_1 - ring_width/2, 0, '18', fontsize = 12, ha='center', va='center')


    ax.axis('equal')
    plt.margins(0,0)
    plt.autoscale('enable')


    if show==True:  #switch for show the figure
        plt.show()


    if png!=None:   #switch for save the figure
        plt.savefig(png)


    #return(plt)    #could be used if the plot-object should be used as a return
    plt.close()     #close the plot if the function is called by another instance


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class InputError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message


class biogas_plant:
    """ Class object for biogas plants
    ----------------------------------
    """

    #class variables (CV)
    CV_gas_norm_f       = 1.25      #factor for convert from operating to normal volume
    CV_methane_lhv      = 9.968     #lower heating value for methane
    CV_runtime_list     = list(range(1,25))
    CV_PQ_list          = [24 / t for t in CV_runtime_list]


    def __init__(self, ta_en:str, ta_de:str, Pe_brc:int, Pe_bic:int, Pe_eta:float, Pt_eta:float, GS_gov:int):
        """>>> Initialyze biogas plant object by given primary parameters and compute secondary parameters.

        Object parameters:
        ==================
        (self.)ta_en:str
            title apendix in "english"

        (self.)ta_de:str
            title apendix in "deutsch"

        (self.)Pe_brc:int
            Basic rated capacity elctrical

        (self.)Pe_bic:int
            Basic electrical installed capacity

        (self.)Pe_eta:float
            Electrical efficiency

        (self.)Pt_eta:float
            Thermal efficiency

        (self.)GS_gov:int
            Gas storage gros operating volume [m³]


        Defaults:
        =========
        GS_ds:float
            Gas storage dead space or rather safety margin, by defaul = 0.2

        RC_ulf:float
            Rated capacity upper limit factor

        RC_llf:float
            Rated capacity lower limit factor
        """

        #technical key values for biogas_plant
        self.ta_en      = ta_en
        self.ta_de      = ta_de
        self.Pe_brc     = Pe_brc
        self.Pe_bic     = Pe_bic
        self.Pe_eta     = Pe_eta
        self.Pt_eta     = Pt_eta
        self.GS_gov     = GS_gov

        #assertations for key values
        assert Pe_brc < Pe_bic, 'Basic installed electrical capacity (Pe_bic) should larger than basic rated elecgrical capacity (Pe_brc)'

        #default values
        self.GS_ds      = 0.2   #Gas storage dead space
        self.RC_ssf     = 0.5   #Rated Capacity - symmetrical scaling factor
        self.CH4_conc   = 0.52  #Methane concentration within the raw biogas


        #call seconary calculation within method definition
        self.f_secondary_key_vals()
        self.f_rc_ranges()


    def f_secondary_key_vals(self):
        """>>> Computing secondary key values within __init__

        Object Parameters:
        ==================

        self.P_brti:int
            Basic rated thermal input

        self.Pt_bic:int
            Basic rated thermal input

        self.PQ_bsc:float
            Basic power quotient

        self.PQ_adj:float
            Adjusted power quotient

        self.RT_bsc:float
            Basic runtime

        self.RT_ajd:float
            Adjusted runtime

        self.Pe_cic:float
            Corrected installed electrical capacity

        self.GS_eleq:float
            Gas storage electrical equivalent

        self.GS_tP_min:float
            Gas storage duration under P_min [h]

        self.GS_tP_max:float
            Gas storage duration under P_max [h]
        """

        self.P_brti         = self.Pe_bic / self.Pe_eta
        self.Pt_bic         = self.P_brti * self.Pt_eta
        self.PQ_bsc         = self.Pe_bic / self.Pe_brc
        self.RT_bsc         = 24 / self.PQ_bsc
        self.RT_adj         = 24 // self.PQ_bsc
        self.PQ_adj         = 24 / self.RT_adj
        self.Pe_aic         = self.Pe_brc * self.PQ_adj
        self.Pt_CHPU_aic    = self.Pt_bic * self.PQ_adj / self.PQ_bsc
        self.Pt_brc         = self.Pt_bic / self.PQ_bsc
        self.GS_eleq        = self.GS_gov * (1 - self.GS_ds) / self.CV_gas_norm_f * self.CH4_conc * self.CV_methane_lhv * self.Pe_eta
        self.GS_tP_min      = self.GS_eleq / self.Pe_brc
        self.GS_tP_max      = self.GS_eleq / (self.Pe_aic - self.Pe_brc )


    def f_rc_ranges(self):
        """>>>Computing key values for the given range of of rated capacity  Pe_brc * +/- RC_ssf

        Parameters:
        ===========
        self.Pe_lrc:float
            Lower  rated capacity   Pe_brc * 1 - RC_ssf

        self.Pe_urc:float
            Upper rated capacity    Pe_brc * 1 + RC_ssf

        self.RT_adj_list:list
            List of adjusted runtimes within the given range for rated capacity factors

        self.Pe_arc_list:list
            List of adjusted rated capacities within given bounds for rated capacity upper and lower
            limit factors (RC_llf & RC_ulf)

        self.Pe_nrc_list:list
            List of normalised rated capacities related to basic rated capacity (Pe_brc = 1)

        self.Pe_nrc_diffs:list
            List of differences of normalised capacities realted to basic rated capacity (Pe_brc = 1)
        """


        self.Pe_lrc = self.Pe_brc * (1 - self.RC_ssf)
        self.Pe_urc = self.Pe_brc * (1 + self.RC_ssf)

        #local / intermediate parameters
        RT_adj_ll   = self.Pe_lrc * 24 / self.Pe_bic // 1
        RT_adj_ul   = self.Pe_urc * 24 / self.Pe_bic // 1


        #set class variable for possible adjuested runtimes
        self.RT_adj_list    = list(range(int(RT_adj_ll) + 1 , int(RT_adj_ul) + 1))
        self.Pe_arc_list    = [rt  * self.Pe_bic / 24 for rt in self.RT_adj_list]
        self.Pe_nrc_list    = [x / self.Pe_brc for x in self.Pe_arc_list]
        self.Pe_nrc_diffs   = [x - self.Pe_brc for x in self.Pe_arc_list]



    def f_plant_scheme(self, lang='en', show=True, save_png=False):
        """
        Function to create a biogas plant scheme based on precalculated values.


        Optional Parameters:
        --------------------
        lang='en'
            language selector. 'en' | 'de'

        show:bool
            variable to define if the scheme should be plotted.

        save_png:bool
            False if scheme should not be safed, otherwise variabel represents the path for save png-file.

        """



        #bilingual labels
        lbls    = { 'ttl'   : { 'en' : f'flexible biogas plant - scheme and key values | {self.ta_en}',
                                'de' : f'Flexible Biogasanlage - Schema und Kennzahlen | {self.ta_de}'},
                    'comp1' : { 'en' : 'fermenter',
                                'de' : 'Fermenter'},
                    'c1_p1' : { 'en' : 'gas production (constant) \n rated capacity $P_{rated}$=',
                                'de' : 'Gaserzeugung (konstant) \n Bemessungsleistung $P_{Bem}$='},
                    'comp2' : { 'en' : 'gas storage',
                                'de' : 'Gasspeicher'},
                    'c2_p1' : { 'en' : 'gros operating volume $V_{gov}$=',
                                'de' : 'Bruttobetriebsvolumen $V_{bbv}$='},
                    'c2_p2' : { 'en' : 'storage electrical equivalent $C_{eleq}$=',
                                'de' : 'Speicher elektrisches Äquivalent $C_{eleq}$='},
                    'comp3' : { 'en' : 'chp-unit',
                                'de' : 'BHKW'},
                    'c3_p1' : { 'en' : 'electrical capacity $P_{inst}$=',
                                'de' : 'Elektrische Leistung $P_{inst}$='},
                    'c3_p2' : { 'en' : 'electrical efficiency ' + r'$\eta_{el}$=',
                                'de' : 'Elektrischer Wirkungsgrad ' + r'$\eta_{el}$='},
                    'c3_p3' : { 'en' : 'thermal capacity ' + r'$P_{th}$=',
                                'de' : 'thermische Leistung ' + r'$P_{th}$='},
                    'c3_p4' : { 'en' : 'thermal efficiency ' + r'$\eta_{th}$=',
                                'de' : 'Thermischer Wirkungsgrad ' + r'$\eta_{th}$='},
                    'ring1' : { 'en' : 'chpu-runtime [h/d] \n (power quotient)',
                                'de' : 'BHKW-Laufzeit [h/d] \n (Leistungsquotient)'},
                    'ring2' : { 'en' : 'storage duration $t$ for $P_{min}$',
                                'de' : 'Speicherdauer $t$ bei $P_{min}$'},
                    'ring3' : { 'en' : 'storage duration $t$ for $P_{max}$',
                                'de' : 'Speicherdauer $t$ bei $P_{max}$'}}



        #define png-names and directory
        png_n_scheme    = 'BGP_scheme_blank.png'
        png_n_chpu_rt   = 'chpu_r1s.png'
        png_n_ptmin     = 'ptmin_r3s.png'
        png_n_ptmax     = 'ptmax_r3s.png'


        #create png-figures for ring-plots
        radial_chart_1s(self.RT_bsc,    lang=lang, png     = osp.join(png_dir, png_n_chpu_rt))
        radial_chart_3s(self.GS_tP_min, lang=lang, png     = osp.join(png_dir, png_n_ptmin))
        radial_chart_3s(self.GS_tP_max, lang=lang, png     = osp.join(png_dir, png_n_ptmax))


        #define image-objects
        im_scheme   = img.imread(osp.join(png_dir, png_n_scheme))
        im_chpu_rt  = img.imread(osp.join(png_dir, png_n_chpu_rt))
        im_ptmin    = img.imread(osp.join(png_dir, png_n_ptmin))
        im_ptmax    = img.imread(osp.join(png_dir, png_n_ptmax))


        #create basic figure
        fig     = plt.figure(figsize=[15,7.5])
        fig.suptitle(lbls['ttl'][lang], fontsize=fs_XL, fontweight='bold')
        ax      = fig.add_subplot(111)
        fig.subplots_adjust(top=0.9)


        #position preset for vertical alignment
        y0_bs   = 4.0
        y1_bs   = y0_bs + 1.5
        y2_bsa  = y1_bs + 1.75
        y3_rl1  = y0_bs - 2
        y4_rl2  = y0_bs - 4


        #create axes for png-images
        ax.imshow(im_scheme, extent=(0, 15, y0_bs, y1_bs))  #axis with basic scheme
        ax.imshow(im_ptmin, extent=(3.5, 5.5, 2, 4))        #axis for storage time of min power
        ax.imshow(im_ptmax, extent=(9.5, 11.5, 2, 4))       #axis for storage time of max power
        ax.imshow(im_chpu_rt, extent=(6.5, 8.5, 0, 2))      #axis for chpu runtime


        #annotaion for component 1
        ax.text(1.55, 7.625, lbls['comp1'][lang], fontsize=fs_L, ha='center', va='center')
        ax.annotate(f"{lbls['c1_p1'][lang]} {self.Pe_brc:.0f} kW", xy=(1.55, y1_bs), xytext=(1.55, y2_bsa),
                    arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)

        #annotation for conmponent 2
        ax.text(7.5, 7.625, lbls['comp2'][lang], fontsize=fs_L, ha='center', va='center')
        ax.annotate(f"{lbls['c2_p1'][lang]} {self.GS_gov:.0f} m³ \n{lbls['c2_p2'][lang]} {self.GS_eleq/1E3:.0f} MWh",   xy=(7.5, y1_bs), xytext=(7.5, y2_bsa), arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)

        #annotation for component 3
        ax.text(13.5, 7.625, lbls['comp3'][lang], fontsize=fs_L, ha='center', va='center')
        ax.annotate(f"{lbls['c3_p1'][lang]} {self.Pe_bic:.0f} kW \n{lbls['c3_p2'][lang]} {self.Pe_eta:.1%} \n{lbls['c3_p3'][lang]} {self.Pt_bic:.0f} kW \n{lbls['c3_p4'][lang]} {self.Pt_eta:.1%}", xy=(13.5, y1_bs), xytext=(13.5, y2_bsa), arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)


        #empty annotations for ring-figures
        arrow_prps={'arrowstyle' : '-', 'ls' : 'dotted',  'connectionstyle' : 'angle,angleA=-90,angleB=-180,rad=10'}

        # connectors for ring figure 1 (chpu runtime)
        ax.annotate(None, xy=(6.5, 1), xytext=(1, y0_bs), arrowprops=arrow_prps)
        ax.annotate(None, xy=(8.5, 1), xytext=(14, y0_bs), arrowprops=arrow_prps)

        # connectors for ring figure 2 (tPmin)
        ax.annotate(None, xy=(3.5, 3), xytext=(2, y0_bs), arrowprops=arrow_prps)
        ax.annotate(None, xy=(5.5, 3), xytext=(7, y0_bs), arrowprops=arrow_prps)

        # connectors for ring figure 3(tPmin)
        ax.annotate(None, xy=(9.5, 3), xytext=(8, y0_bs), arrowprops=arrow_prps)
        ax.annotate(None, xy=(11.5, 3), xytext=(13, y0_bs), arrowprops=arrow_prps)


        #annotation for ring figure 1 (chpu-runtime)
        ax.annotate(lbls['ring1'][lang], xy=(7.525, 1.75), xytext=(7.525, 2.25),
                    arrowprops={'arrowstyle' : '-'}, ha='center', fontsize=fs_M)

        #annotation for ring figure 2 (ptmin)
        ax.annotate(lbls['ring2'][lang], xy=(4.55, 2.25), xytext=(4.55, 1.5),
                    arrowprops={'arrowstyle' : '-'}, ha='center', fontsize=fs_M)

        #annotation ring figure 1 (ptmax)
        ax.annotate(lbls['ring3'][lang], xy=(10.55, 2.25), xytext=(10.55, 1.5),
                    arrowprops={'arrowstyle' : '-'}, ha='center', fontsize=fs_M)

        #axis dimensions and turn of axis visability
        ax.axis([0, 15, 0, 7.5])
        ax.axis('off')

        plt.tight_layout()

        #handling show / plotting options
        if save_png !=False:   #switch for save the figure
            plt.savefig(save_png)

            if show != True:
                plt.clf()

        if show==True:  #switch for show the figure
            plt.show()




class steam_cycle_bpt:
    """ Class object for biomas power plants based on a steam cycle process using a back pressure turbine
    ----------------------------------
    """

    #class variables (CV)


    def __init__(self, ta_en, ta_de, Pb_brti:float, Pb_beta:float, Pb_meta:float,
                    PTe_beta:float, PTe_meta:float, PTt_beta:float, PTt_meta:float, Pb_minf:float=0.6):
        """>>> Initiayse biomass SCBPT object by given primary parameters and compute secondary parameters.

        Parameters:
        ===========
        (self.)ta_en:str
            title apendix in "english"

        (self.)ta_de:str
            title apendix in "deutsch"

        (self.)Pb_brti:int
            Basic rated thermal input for steam boiler [MW]

        (self.)Pb_beta:float
            Basic boiler efficiency for Pe_brti

        (self.)Pb_meta:float
            Boiler efficiency for minimum boiler power (Pe_brti * Pb_min)

        (self.)PTe_beta:float
            Turbine Electrical efficiency for basic boiler load

        (self.)PTe_meta:float
            Turbine Electrical efficiency for minimum boiler load

        (self.)PTt_beta:float
            Turbine thermal efficiency for basic boiler load

        (self.)PTt_meta:float
            Turbine thermal efficiency for minimum boiler load

        (self.)Pb_minf=0.6:float
            Minimum power factor for boiler load.
        """

        #technical key values for biogas_plant
        self.ta_en      = ta_en
        self.ta_de      = ta_de
        self.Pb_brti    = Pb_brti
        self.Pb_beta    = Pb_beta
        self.Pb_meta    = Pb_meta
        self.PTe_beta   = PTe_beta
        self.PTe_meta   = PTe_meta
        self.PTt_beta   = PTt_beta
        self.PTt_meta   = PTt_meta
        self.Pb_minf    = Pb_minf

        #assertations for key values

        #call seconary calculation within method definition
        self.f_secondary_key_vals()


    def f_secondary_key_vals(self):
        """>>> Computing secondary key values within __init__

        Parameters:
        ===========

        (self.)P_mrti:int
            Minimum boiler load

        (self.)PT_bic:float
            Baic installed capacity for steam turbine

        (self.)PT_min:float
            Minimum capacity for steam turbine

        (self.)Pt_min_eta:float
            Thermal efficiency for minimum capacity

        """

        self.Pb_mrti    = self.Pb_brti * self.Pb_minf
        self.PTe_bic    = self.Pb_brti * self.Pb_beta * self.PTe_beta
        self.PTe_min    = self.Pb_mrti * self.Pb_meta * self.PTe_meta
        self.PTt_bic    = self.Pb_brti * self.Pb_beta * self.PTt_beta
        self.PTt_min    = self.Pb_mrti * self.Pb_meta * self.PTt_meta


    def f_operating_point(self, Pb_opp:float):
        """Compute operating point keyvalues.

        Parameters:
        ===========
        Pb_opp:float
            Linear scaling factor for boiler load. Assertion test for Pb_minf <= Pb_lsc <= 1
        """
        assert  self.Pb_minf <= Pb_opp, 'Constraint violation:  Pb_minf <= Pb_opp <= 1'
        assert  Pb_opp  <=  1, 'Constraint violation:  Pb_minf <= Pb_opp <= 1'

        #scaled rti / boiler load @ operating point
        Pb_opp      = self.Pb_brti * Pb_opp

        #relative position between Pb_mrti and Pb_brti
        opp_rel     = (Pb_opp - self.Pb_mrti) / (self.Pb_brti - self.Pb_mrti)

        #linear interpolation of efficiencies @ operating point
        Pb_seta     = self.Pb_meta + (self.Pb_beta - self.Pb_meta) *  opp_rel
        PTe_seta    = self.PTe_meta + (self.PTe_beta - self.PTe_meta) *  opp_rel
        PTt_seta    = self.PTt_meta + (self.PTt_beta - self.PTt_meta) *  opp_rel

        #computing key values
        self.Pb_opp     = Pb_opp
        self.PTe_opp    = Pb_opp * Pb_seta * PTe_seta
        self.PTt_opp    = Pb_opp * Pb_seta * PTt_seta


        #return operating point parameters
        return(self.Pb_opp, self.PTe_opp, self.PTt_opp)


    def f_characteristic_cuve(self, lang='en'):
        """Plot of the characteristic curve of a the certain biomass SCBPT

        Parameters:
        ===========

        lang='en'
            Language selector for the plot 'en' | 'de'
        """

        #compute performance values for the given modulation range
        mrange_f            = list(range(int(self.Pb_minf * 100), 101))
        mrange_rti          = [self.Pb_brti * x / 100 for x in mrange_f]

        Pb_eta_step         = (self.Pb_beta - self.Pb_meta) / (len(mrange_f) - 1)
        mrange_Pb_beta      = [self.Pb_meta + (Pb_eta_step * i) for i, f in enumerate(mrange_f)]

        PTe_eta_step        = (self.PTe_beta - self.PTe_meta) / (len(mrange_f) - 1)
        mrange_PTe_beta     = [self.PTe_meta + (PTe_eta_step * i) for i, f in enumerate(mrange_f)]

        mrange_STe   = [rti * mrange_Pb_beta[i] * mrange_PTe_beta[i] for i, rti in enumerate(mrange_rti)]


        #dictionary for plot labels
        lbls_dict   = { 'ttl'   : { 'en' : 'Characteristic curve for electrical power',
                                    'de' : 'Kennlinie der elektrischen Leistung'},
                        'xlbl'  : { 'en' : f'rated thermal input ($P_{{in}}$) [MW]',
                                    'de' : f'Feuerungswärmeleistung ($P_ {{in}}$) [MW]'},
                        'ylbl'  : { 'en' : f'electrical output ($P_{{el}}$) [MW]',
                                    'de' : f'Ausgangsleistung elektrisch ($P_{{el}}$) [MW]'}  }

        tbox_01     = f'$P_{{in}}$={self.Pb_brti:.0f} MW \n$\eta_{{el}}$={self.PTe_beta:.1%} \n$P_{{el}}$={self.PTe_bic:.2f} MW'
        tbox_02     = f'$P_{{in}}$={self.Pb_mrti:.0f} MW \n$\eta_{{el}}$={self.PTe_meta:.1%} \n$P_{{el}}$={self.PTe_min:.2f} MW'

        fig     = plt.figure('characteristic curve', figsize=(5,5))

        plt.plot(mrange_rti, mrange_STe, color='mediumblue')
        plt.title(lbls_dict['ttl'][lang])
        plt.xlabel(lbls_dict['xlbl'][lang])
        plt.ylabel(lbls_dict['ylbl'][lang])
        plt.grid(ls=':', alpha=0.333)

        #define arrow porperties for annotations
        arrowprops=dict(arrowstyle='-', linestyle='-', color='dimgrey', linewidth=0.5)


        #add annotation for operating point @ full load capacity
        plt.annotate(tbox_01, (self.Pb_brti * 0.99, self.PTe_bic), (0.2, 0.75),
                        arrowprops=arrowprops,
                        xycoords= 'data', textcoords='figure fraction')

        #add annotation for operating point @ partial load
        plt.annotate(tbox_02, (self.Pb_mrti * 1.01, self.PTe_min), (0.7, 0.15),
                        arrowprops=arrowprops,
                        xycoords= 'data', textcoords='figure fraction')

        #show plot
        plt.show()



    def f_plant_scheme(self, lang='en', show=True, save_png=False):
        """Plot of the plant scheme of a the certain biomass SCBPT with annotations for key values.

        Parameters:
        ===========

        lang='en'
            Language selector for the plot 'en' | 'de'
        """

        #define png-names and directory
        png_n_scheme    = 'WPP_SCBPT_scheme_blank.png'


        #bilingual labels
        lbls    = { 'ttl'   : { 'en' : f'CHP steam cycle process using back pressure turbine - scheme and key values | {self.ta_en}',
                                'de' : f'KWK Dampfkreisprozess mit Gegendruckturbine - Schema und Kennzahlen | {self.ta_en}'},
                    'blr'   : { 'en' : f'boiler', 'de' : f'Dampfkessel'},
                    'blr_p' : { 'en' : f'$P_{{rti}}$ = {self.Pb_brti:.2f}MW \
                                        \n$\u03BC_{{th}}$ = {self.Pb_beta:.1%} \
                                        \n$P_{{steam}}$ = {(self.Pb_brti * self.Pb_beta):.2f} MW',
                                'de' : f'$P_{{FWL}}$ = {self.Pb_brti:.2f}MW \
                                        \n$\u03B7_{{th}}$ = {self.Pb_beta:.1%} \
                                        \n$P_{{Dampf}}$ = {(self.Pb_brti * self.Pb_beta):.2f} MW'},
                    'trb'   : { 'en' : f'steam turbine - generator', 'de' : f'Dampfturbine - Generator'},
                    'trb_p' : { 'en' : f'$\u03B7_{{el}}$ = {self.PTe_beta:.1%} \
                                        \n$P_{{el}}$ = {self.PTe_bic:.2f}MW',
                                'de' : f'$\u03B7_{{el}}$ = {self.PTe_beta:.1%} \
                                        \n$P_{{el}}$ = {self.PTe_bic:.2f}MW' },
                    'hex'   : { 'en' : f'heat exchanger', 'de' : f'Wärmetauscher'},
                    'hex_p' : { 'en' : f'$\u03B7_{{th}}$ = {self.PTt_beta:.1%} \
                                        \n$P_{{th}}$ = {self.PTt_bic:.2f}MW',
                                'de' : f'$\u03B7_{{th}}$ = {self.PTt_beta:.1%} \
                                        \n$P_{{th}}$ = {self.PTt_bic:.2f}MW' },
                    'fwp'   : { 'en' : f'feedwater pump',
                                'de' : f'Speisewasserpumpe'}    }



        #create basic figure
        fig     = plt.figure('WPP SCBPT scheme', figsize=[15,7.5])
        fig.suptitle(lbls['ttl'][lang], fontsize=16, fontweight='bold')
        ax      = fig.add_subplot(111)
        fig.subplots_adjust(top=0.9)


        #define image-objects
        im_scheme   = img.imread(osp.join(png_dir, png_n_scheme))

        #position preset for vertical alignment
        y0_bs   = 1.25
        y1_bs   = y0_bs + 5.25
        y2_bsa  = y1_bs + 1
        y3_rl1  = y0_bs - 2
        y4_rl2  = y0_bs - 4


        #create axes for png-images
        ax.imshow(im_scheme, extent=(0, 15, y0_bs, y1_bs))  #axis with basic scheme


        #text for the boiler and its properties
        ax.text(0, 7.375, lbls['blr'][lang], size=14, ha='left', va='top')
        ax.text(0, 7, lbls['blr_p'][lang], size=12, ha='left', va='top')


        #text for the steam turbine-generator and its properties
        ax.text(11, 7.375, lbls['trb'][lang], size=14, ha='left', va='top')
        ax.text(11, 7, lbls['trb_p'][lang], size=12, ha='left', va='top')


        #text for the heat exchanger and its properties
        ax.text(9.5, 3.227, lbls['hex'][lang], size=14, ha='left', va='top')
        ax.text(9.5, 2.9, lbls['hex_p'][lang], size=12, ha='left', va='top')

        #text for the feedwater pump
        ax.text(0, 2, lbls['fwp'][lang], size=14, ha='left', va='top')


        #axis dimensions and turn of axis visability
        ax.axis([0, 15, 0, 7.5])
        ax.axis('off')


        #handling show / plotting options
        if save_png !=False:   #switch for save the figure
            plt.savefig(save_png)

            if show != True:
                plt.clf()

        if show==True:  #switch for show the figure
            plt.show()



@dataclass
class heat_supply_system:
    """Data class for heat supply systems

    Paraemters:
    ==========

    ta_en           : str       | title appendix "enlish"
    ta_de           : str       | title appendix "deutsch"
    Pt_CHPU_aic     : float     | adjusted thermal installed capacity of the CHPU [kW]
    Pt_PLB          : float     | thermal power of the peak load boiler [kW]
    Pt_eta_PLB      : float     | thermal efficiency of the peak load boiler [-]
    HS_cap          : float     | heat storage capacity [kWh]
    HS_vol          : float     | heat storage volume [m³]
    HS_T_max        : float     | heat storage maximum (flow) temperature [°C]
    HS_T_min        : float     | heat storage minumum (return flow) temperature [°C]
    Fuel_type_de    : str       | fuel type for the peak load boiler "english"
    Fuel_type_en    : str       | fuel type for the peak load boiler "deutsch"
    Fuel_price      : float     | fuel price [€/MWh]
    HLP_Pt_max      : float     | peak demand of the given heat load profile
    HLP_Wt_sum      : float     | annual heat demand of the given heat load profile
    HLP_name_de     : str       | name / description for the given heat load profiel "english"
    HLP_name_en     : str       | name / description for the given heat load profiel "deutsch"


    Functions:
    ==========

    f_HS_vol2cap()              | converting heat storage volume to capacity
    f_HS_cap2vol()              | converting heat storage capacity to volume
    f_systeme_scheme()          | plotting hss scheme and its key values

    """

    ta_en           : str       = ''
    ta_de           : str	    = ''
    Pt_CHPU_aic     : float     = 0
    Pt_PLB          : float     = 0
    Pt_eta_PLB      : float     = 0.85
    HS_cap          : float     = None
    HS_vol          : float     = None
    HS_T_max        : float     = 95
    HS_T_min        : float     = 70
    Fuel_type_de    : str       = None
    Fuel_type_en    : str       = None
    Fuel_price      : float     = 0
    HLP_Pt_max      : float     = 0
    HLP_Wt_sum      : float     = 0
    HLP_name_de     : str       = None
    HLP_name_en     : str       = None


    #post init function to compute missing value for heat storage properties
    def __post_init__(self):
        if self.HS_cap == None and self.HS_vol != None:
            self.f_HS_vol2cap()

        elif self.HS_vol == None and self.HS_cap != None:
            self.f_HS_cap2vol()

        elif self.HS_vol != None and self.HS_cap != None:
            pass

        else:
            raise InputError('Heat storage must be defined either by volume or cpacity!')




    def f_HS_vol2cap(self):
        "Converting heat storage volume [m³] to energy capacity [kWh]"
        self.HS_cap     = (self.HS_T_max - self.HS_T_min) * 4200 / 3.6 * self.HS_vol / 1E3



    def f_HS_cap2vol(self):
        "Converting heat storage capacity [kWh] to volume [m³]"
        self.HS_vol     = self.HS_cap / ((self.HS_T_max - self.HS_T_min) * 4.2 / 3.6)



    def f_system_scheme(self, lang='en', show=True, save_png=False):

        """
        Function to create a heating system scheme based on precalculated values.


        Optional Parameters:
        --------------------
        lang='en'
            language selector. 'en' | 'de'

        """

        #compute buffer times
        bt_plb          = self.HS_cap / self.Pt_PLB
        bt_chpu         = self.HS_cap / self.Pt_CHPU_aic


        #define png-names and directory
        png_n_scheme    = 'HSS_scheme_blank.png'
        png_n_bt_plb    = 'bt_plb_r3s.png'
        png_n_bt_chpu   = 'bt_chpu_r3s.png'


        #create png-figures for ring-plots
        radial_chart_3s(bt_plb, lang=lang, png     = osp.join(png_dir, png_n_bt_plb))
        radial_chart_3s(bt_chpu, lang=lang, png     = osp.join(png_dir, png_n_bt_chpu))


        #define image-objects
        im_scheme   = img.imread(osp.join(png_dir, png_n_scheme))
        im_bt_plb   = img.imread(osp.join(png_dir, png_n_bt_plb))
        im_bt_chpu  = img.imread(osp.join(png_dir, png_n_bt_chpu))




        #bilingual labels
        lbls    = { 'ttl'   : { 'en' : f'heat supply system - scheme and key values | {self.ta_en}',
                                'de' : f'Wärmeversorgungsssystem - Schema und Kennzahlen | {self.ta_de}'},
                    'comp1' : { 'en' : 'Fuel',
                                'de' : 'Brennstoff'},
                    'c1_p1' : { 'en' : f'type: {self.Fuel_type_en}',
                                'de' : f'Typ: {self.Fuel_type_de}'},
                    'c1_p2' : { 'en' : f'price $p_{{fuel}}$={self.Fuel_price}€/MWh',
                                'de' : f'Preis: $p_{{Brennstoff}}$={self.Fuel_price} €/MWh'},
                    'comp2' : { 'en' : 'peak load boiler',
                                'de' : 'Spitzenlastkessel'},
                    'c2_p1' : { 'en' : f'thermal capacity $P_{{th}}$={self.Pt_PLB:.0f} kW',
                                'de' : f'Wärmeleistung $P_{{th}}$={self.Pt_PLB:.0f} kW'},
                    'c2_p2' : { 'en' : f'thermal efficiency $\eta_{{th}}$={self.Pt_eta_PLB:.1%}',
                                'de' : f'Thermischer Wirkungsgrad $\eta_{{th}}$={self.Pt_eta_PLB:.1%}'},
                    'comp3' : { 'en' : 'heat storage',
                                'de' : 'Wärmespeicher'},
                    'c3_p1' : { 'en' : f'storage capacity $C_{{th}}$={self.HS_cap:.0f} kWh',
                                'de' : f'Speicherkapazität $C_{{th}}$={self.HS_cap:.0f} kWh'},
                    'c3_p2' : { 'en' : f'storage volume $V_{{storage}}$={self.HS_vol:.1f} m³',
                                'de' : f'Speichervolumen $V_{{Speicher}}$={self.HS_vol:.1f} m³'},
                    'c3_p3' : { 'en' : f'temperature spread $T_{{delta}}$={self.HS_T_max - self.HS_T_min} K',
                                'de' : f'Temperaturspreizung $T_{{delta}}$={self.HS_T_max - self.HS_T_min} K'},
                    'comp4' : { 'en' : 'CHPU',
                                'de' : 'BHKW'},
                    'c4_p1' : { 'en' : f'thermal capacity (adjusted) $P_{{th}}$={self.Pt_CHPU_aic:.0f} kW',
                                'de' : f'Thermische Leistung (korrigiert) $P_{{th}}$={self.Pt_CHPU_aic:.0f} kW'},
                    'ring1' : { 'en' : 'buffer time peak load boiler $t_{plb}$',
                                'de' : 'Pufferdauer Spitzenlastkessel $t_{SLK}$'},
                    'ring2' : { 'en' : 'buffer time CHPU $t_{CHPU}$',
                                'de' : 'Pufferdauer BHKW $t_{BHKW}$'},
                    'comp5' : { 'en' : 'heat sink',
                                'de' : 'Wärmesenke'},
                    'c5_p1' : { 'en' : f'thermal peak demand $P_{{th}}$={self.HLP_Pt_max:.0f} kW',
                                'de' : f'Thermische Spitzenlast(korrigiert) $P_{{th}}$={self.HLP_Pt_max:.0f} kW'},
                    'c5_p2' : { 'en' : f'annual heat demand $W_{{th}}$={self.HLP_Wt_sum / 1E6:.0f} GWh',
                                'de' : f'Jahreswärmebedarf $W_{{th}}$={self.HLP_Wt_sum / 1E6:.0f} GWh'},
                    'c5_p3' : { 'en' : f'heat load profile: {self.HLP_name_en}',
                                'de' : f'Wärmelastprofil: {self.HLP_name_de}'}}


        #create basic figure
        fig     = plt.figure(figsize=[17.5,7])
        fig.suptitle(lbls['ttl'][lang], fontsize=fs_XL, fontweight='bold')
        ax      = fig.add_subplot(111)
        fig.subplots_adjust(top=0.9)



        #create axes for png-images
        ax.imshow(im_scheme, extent=(0, 17.5, 0, 5))      #axis with basic scheme
        ax.imshow(im_bt_plb, extent=(7.75, 9.75, 2.75, 4.75))           #axis for storage time of min power
        ax.imshow(im_bt_chpu, extent=(12.5, 14.5, 2.75, 4.75))         #axis for storage time of max power


        #annotaion for component 1
        ax.text(1.4, 6.75, lbls['comp1'][lang], fontsize=fs_L, ha='center', va='bottom')
        ax.annotate(f"{lbls['c1_p1'][lang]} \n{lbls['c1_p2'][lang]}", xy=(1.4, 6.75), xytext=(1.4, 6),
                    arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)

        #annotation for conmponent 2
        ax.text(6.3, 6.75, lbls['comp2'][lang], fontsize=fs_L, ha='center', va='bottom')
        ax.annotate(f"{lbls['c2_p1'][lang]}\n{lbls['c2_p2'][lang]}",   xy=(6.3, 6.75), xytext=(6.3, 6), arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)


        #annotation for component 3
        ax.text(11.25, 6.75, lbls['comp3'][lang], fontsize=fs_L, ha='center', va='bottom')
        ax.annotate(f"{lbls['c3_p1'][lang]}\n{lbls['c3_p2'][lang]}\n{lbls['c3_p3'][lang]}", xy=(11.25, 6.75), xytext=(11.25, 6), arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)


        #annotation for component 4
        ax.text(16.25, 6.75, lbls['comp4'][lang], fontsize=fs_L, ha='center', va='bottom')
        ax.annotate(f"{lbls['c4_p1'][lang]}", xy=(16.25, 6.75), xytext=(16.25, 6), arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)


        #annotation for component 5
        ax.text(3.75, 2.75, lbls['comp5'][lang], fontsize=fs_L,  ha='center', va='bottom')
        ax.annotate(f"{lbls['c5_p1'][lang]}\n{lbls['c5_p2'][lang]}\n{lbls['c5_p3'][lang]}", xy=(3.75, 2.5), xytext=(3.75, 2), arrowprops={'arrowstyle' : '-'}, ha='center', va='top', fontsize=fs_M)


        #empty annotations for ring-figures
        arrow_prps={'arrowstyle' : '-', 'ls' : 'dotted'}


        # connectors for ring figure 1 (bt_plb)
        ax.annotate(None, xy=(7.1, 3.75), xytext=(7.9, 3.75), arrowprops=arrow_prps)
        ax.annotate(None, xy=(9.6, 3.75), xytext=(10.4, 3.75), arrowprops=arrow_prps)

        # connectors for ring figure 2(bt_chpu)
        ax.annotate(None, xy=(12.1, 3.75), xytext=(12.7, 3.75), arrowprops=arrow_prps)
        ax.annotate(None, xy=(14.4, 3.75), xytext=(14.8, 3.75), arrowprops=arrow_prps)


        #annotation for ring figure 1 (bt_plb)
        ax.annotate(lbls['ring1'][lang], xy=(8.75, 2.9), xytext=(8.75, 2),
                    arrowprops={'arrowstyle' : '-'}, ha='center', fontsize=fs_M)

        #annotation for ring figure 2 (bt_plb)
        ax.annotate(lbls['ring2'][lang], xy=(13.5, 2.9), xytext=(13.5, 2),
                    arrowprops={'arrowstyle' : '-'}, ha='center', fontsize=fs_M)


        #axis dimensions and turn of axis visability
        ax.axis([0, 17.5, 0, 7])
        ax.axis('off')
        #ax.grid()

        #compose a tight layout
        fig.tight_layout()

        #handling show / plotting options
        if save_png !=False:   #switch for save the figure
            plt.savefig(save_png)

            if show != True:
                plt.clf()

        if show==True:  #switch for show the figure
            plt.show()



##Dummy plant objects for testing purposes

def BGP_Dummy(lang='en'):
    """
    Function to create a heat supply system dummy, with default key values.
    """

    BGP_d   = biogas_plant(     'dummy',
                                'Dummy',
                                1700,
                                2.5*1700,
                                0.4,
                                0.4,
                                6000)

    BGP_d.f_plant_scheme()

    return(BGP_d)



def SCP_Dummy(lang='en'):
    """
    Function to create a steam_cycle_bpt dummy, with default key values.
    """

    SCP_d   = steam_cycle_bpt(     ta_en       = 'dummy',
                                    ta_de       = 'Dummy',
                                    Pb_brti     = 20E3,
                                    Pb_beta     = 0.95,
                                    Pb_meta     = 0.9,
                                    PTe_beta    = 0.4,
                                    PTe_meta    = 0.3,
                                    PTt_beta    = 0.5,
                                    PTt_meta    = 0.6,
                                    Pb_minf     = 0.6)

    SCP_d.f_plant_scheme()

    return(SCP_d)




def HSS_dummy(lang='en'):
    """
    Function to create a heat supply system dummy, with default key values.
    """

    HSS_d =     heat_supply_system( ta_en           = 'dummy',
                                    ta_de           = 'Dummy',
                                    Pt_CHPU_aic     = 600,
                                    Pt_PLB          = 900,
                                    Pt_eta_PLB      = 0.85,
                                    HS_cap          = 4 * 900,
                                    Fuel_type_de    = 'Holzhackschnitzel',
                                    Fuel_type_en    = 'wood chips',
                                    Fuel_price      = '30',
                                    HLP_Pt_max      = 900,
                                    HLP_Wt_sum      = 900 * 2000,
                                    HLP_name_en     = 'dummy heat load profile',
                                    HLP_name_de     = 'Dummy Wärmelastprofil'
                                        )

    HSS_d.f_HS_cap2vol()

    HSS_d.f_system_scheme()


    return(HSS_d)