"""
Module to assess beforehand computed optimisation results for flexible biogas plant (bgp) concepts
using the smart-force-optimisation (sfo) aproach.
"""

import math
import numpy                    as np
from matplotlib import pyplot   as plt
import matplotlib.dates         as mdt
import matplotlib.gridspec      as gsp
from matplotlib.patches     import Rectangle        as rct
from matplotlib.colors      import ListedColormap   as lcm

from dataclasses            import dataclass


@dataclass
class SFO_analytics_dc:
    """Data class for analytical data of SFOr assessment"""


def rds(str_in, lang):
    """Function for replace decimal separators (rds) in number formatting from English to German"""

    #keep decimal separators unchanged if lang=='en'
    if lang=='en':
        str_out = str_in


    #replace dot by comma and vice versa in lang == 'de'
    elif lang == 'de':
        str_out = str_in.replace(',','#').replace('.',',').replace('#','.')


    #pseudo exception line if lang != 'en' OR lang != 'de'
    else:
        str_out = str_in


    return(str_out)



def yticks_align(ax1, ax2):
    """Function to align yticks for two axes and adjust ylim to min max of yticks"""

    #create lists for yticks
    ax1_tl 	= list(ax1.get_yticks())
    ax2_tl 	= list(ax2.get_yticks())


    #if length of both lists is already equal set them to resulting extend lists arguments
    if len(ax1_tl) == len(ax2_tl):
        ax1_etl 	= ax1_tl
        ax2_etl 	= ax2_tl

    else:

        #get ylims for axes
        ax1_yl 	= ax1.get_ylim()
        ax2_yl 	= ax2.get_ylim()


        #compute scales for both axes
        ax1_scl = ax1_tl[1] - ax1_tl[0]
        ax2_scl = ax2_tl[1] - ax2_tl[0]


        def list_expander(ax_tl, ax_yl, ax_scl, t_len):
            """Subfunction to incrementally expand the shorter list of the given ytick-lists"""

            #check wich end of ylim is closer to the first / last tick and set start index for inserting
            if abs(ax_tl[0] - ax_yl[0]) > abs(ax_tl[-1] - ax_yl[-1]):
                sii 	= 0
            else:
                sii 	= -1

            #initiate extended ticks list
            ax_etl 	= list(ax_tl)


            #extend list, switich between first and last postition until length is equal to target length
            while len(ax_etl) < t_len:

                #compute new tick value, insert and flip ssi
                if sii == 0:
                    new_tick 	= ax_etl[0] - ax_scl
                    ax_etl.insert(sii, new_tick)
                    sii = 1

                else:
                    new_tick 	= ax_etl[-1] + ax_scl
                    ax_etl.append(new_tick)
                    sii = 0

            return(ax_etl)


        #apply list extension function to ax1 or ax2 and set the other list
        if len(ax1_tl) < len(ax2_tl):
            ax1_etl 	= list_expander(ax1_tl, ax1_yl, ax1_scl, len(ax2_tl))
            ax2_etl 	= ax2_tl

        else:
            ax1_etl 	= ax1_tl
            ax2_etl 	= list_expander(ax2_tl, ax2_yl, ax2_scl, len(ax1_tl))


    #set new properties (yticks and ylims)
    ax1.set_yticks(ax1_etl)
    ax1.set_ylim(ax1_etl[0], ax1_etl[-1])

    ax2.set_yticks(ax2_etl)
    ax2.set_ylim(ax2_etl[0], ax2_etl[-1])


    return(ax1, ax2)




def SFO_analytics(SFOr, BGP, year, lang='en', carpet=False, timespan=False):
    """
    Function for anylysing sfo-results, with different diagramming options.

    Parameters:
    ----------

    SFOr:object
        dataclass object containing the smart-force-optimisation resluts

    BGP:object
        chp-plant-class object containing the biogas plant properties

    year:int
        argument for specifying the year of the analysis

    lang:str
        language specifier 'en' | 'de'

    carpet:bool/key
        switch for creating a carpet-plot with following options:
            False       |   supress the subfunction (no figure was created)
            'show'      |   show the created figure
            png_path    |   save the created figure as png file

    timespan:bool/key
        siwtch for creating a plot over a given timespan with following options:
            False       |   supress the subfunction (no figure was created)
            'show'      |   show the created figure
            png_path    |   save the created figure as png file

    """

    #dataclass object for SFO analytics
    SFOa            = SFO_analytics_dc()


    #add specific name / short description for the calculated set up
    HSS_None_dict   = {     'en'    :  'no CHP considered' , 'de'    : 'ohne KWK Berücksichtigung'}

    if 'HSS_name' in SFOr.__dict__.keys():
        HSS_spec = SFOr.HSS_name
    else:
        HSS_spec = HSS_None_dict[lang]



    #bilingual labels dictionary
    lbls = { 'crpt_ttl' : { 'en' : f"schedule and specific extra revenues | {BGP.name} / {HSS_spec}",
                            'de' : f"Fahrplan und spezifische Mehrerlöse | {BGP.name} / {HSS_spec}"},
            'ax0_ttl'   : { 'en' : 'carpet plot for schedule',
                            'de' : 'Rasterdiagramm des Fahrplans'},
            'ax0_ylbl'  : { 'en' : 'hours',
                            'de' : 'Stunden'},
            'ax1_ttl'   : { 'en' : 'key values',
                            'de' : 'Kennwerte'},
            'ax1_ant1'  : { 'en' : 'year: ',
                            'de' : 'Jahr: '},
            'ax1_ant3'  : { 'en' : 'runtime (corr.): ',
                            'de' : 'Laufzeit (korr.): '},
            'ax1_ant5'  : { 'en' : 'strokes: ',
                            'de' : 'Startvorgänge: '},
            'ax1_ant6'  : { 'en' : 'sp. extra: ',
                            'de' : 'sp. Mehrerlös: '},
            'ax1_ant7'  : { 'en' : 'max. extra: ',
                            'de' : 'max. Merherl.: '},
            'ax1_ant8'  : { 'en' : 'min. extra: ',
                            'de' : 'min. Mehrerl.: '},
            'ax1_ant9'  : { 'en' : 'abs. extra: ',
                            'de' : 'abs. Mehrerl.: '},
            'ax2_xlbl'  : { 'en' : 'days',
                            'de' : 'Tage'},
            'ax2_ylbl'  : { 'en' : 'specific extra earnings [€/MWh]',
                            'de' : 'Spezifische Mehrerlöse [€/MWh]'},
            'ax2_lgnd'  : { 'en' : ['daily price order (DPO)','smart force optimisation (SFO)'],
                            'de' : ['Daily-Preis-Order (DPO)','Smart-Force-Optimierung (SFO)']},
            'ax3_ylbl'  : { 'en' : 'anual mean of specific extra [€/MWh]',
                            'de' : 'Jahresmittel der spezifischen Mehrerlöse [€/MWh]'},
            'tmsp_ttl'  : { 'en' : 'comparison of days with best and least extra revenues in ',
                            'de' : 'Vergleich der Tage mit dem höchsten und niedrigsten Mehrerlös in '},
            'ax4_ttl'   : { 'en' : 'day of most extra - share of DPO: ',
                            'de' : 'Tag der höchsten Mehrerlöse - Anteil DPO: '},
            'ax4_ylbl'  : { 'en' : 'power | storage level - [kW] | [kWh]',
                            'de' : 'Leistung | Speicherfüllstand - [kW] | [kWh]'},
            'ax4_lgd'   : { 'en' : [ 'rated capacity',
                                        'gas storage fill level',
                                        'gas storage limit',
                                        'chpu load',
                                        'epex price (secondary y-axis)'],
                            'de' : [ 'Bemessungsleistung',
                                        'Gasspeicher Füllstand',
                                        'Gassspeicher Limit',
                                        'BHKW-Leistung',
                                        'EPEX-Preis (sekundäre Y-Achse)']},
            'ax5_ttl'   : { 'en' : 'day of least extra - share of DPO: ',
                            'de' : 'Tag der niedrigsten Mehrerlöse - Anteil DPO: '},
            'ax5_ylbl'  : { 'en' : 'epex spot price [€/MWh]',
                            'de' : 'EPEX-Spot Preis [€/MWh]'}}


    #precalculations
    A_sfo_bin   = SFOr.CHPU_binopt
    A_prices    = np.array(SFOr.EPEX_ppp)
    A_prices    = np.nan_to_num(A_prices, np.nanmean(A_prices))
    strokes_real= np.diff(SFOr.CHPU_binopt != 0).sum()/2

    SFOa.strokes_real = strokes_real



    #potential for extra earnings based on the daily price order (DPO)
    A_p_order   = A_prices.argsort(axis=1)
    A_p_ranks   = A_p_order.argsort(axis=1)
    A_prm       = (A_p_ranks > (23 - BGP.RT_adj)) * A_prices
    A_prm_bin   = np.where(A_prm > 0, 1, 0)

    A_sfo       = SFOr.CHPU_binopt * A_prices
    V_prm_xtr   = np.true_divide(A_prm.sum(1), (A_prm!=0).sum(1))-A_prices.mean(1)
    V_sfo_xtr   = np.true_divide(A_sfo.sum(1), (A_sfo!=0).sum(1))-A_prices.mean(1)
    i_max_xtr   = np.where(V_sfo_xtr==np.nanmax(V_sfo_xtr))[0][0]
    i_min_xtr   = np.where(V_sfo_xtr==np.nanmin(V_sfo_xtr))[0][0]
    prm_xtr     = np.nanmean(V_prm_xtr)
    sfo_xtr     = np.nanmean(V_sfo_xtr)
    abs_xtr     = (SFOr.CHPU_Pe_schedule  / 1E3 * A_prices).sum() - (SFOr.CHPU_Pe_schedule.sum() / 1E3 * A_prices.mean())


    SFOa.prm_xtr_spc = prm_xtr
    SFOa.sfo_xtr_spc = sfo_xtr
    SFOa.sfo_xtr_abs = abs_xtr


    ##carpet-plot for the schedule
    if carpet!= False:

        #create carpet plot figure
        fig = plt.figure(figsize=(16, 9))
        plt.suptitle(lbls['crpt_ttl'][lang], fontsize=18)
        gs0  = gsp.GridSpec( 2, 2,  width_ratios=(13, 3), height_ratios=(1, 1), wspace=0.05, hspace=0.1)

        #colormaps for carpetplot
        cm_prm_0        = lcm(['white', 'orange'])
        cm_prm_1        = cm_prm_0(np.arange(cm_prm_0.N))
        cm_prm_1[:,-1]  = np.linspace(0, 0.8, cm_prm_0.N)       # Set alpha
        cm_prm_1        = lcm(cm_prm_1)


        cm_sfo_0        = lcm(['white', 'dodgerblue'])
        cm_sfo_1        = cm_sfo_0(np.arange(cm_sfo_0.N))
        cm_sfo_1[:,-1]  = np.linspace(0, 0.6, cm_sfo_0.N)       # Set alpha
        cm_sfo_1        = lcm(cm_sfo_1)


        #subplot for carpet plot of schedule
        ax0     = plt.subplot(gs0[0])
        ax0.set_title(lbls['ax0_ttl'][lang], fontsize=16)
        ax0.pcolor(A_prm_bin.T, cmap=cm_prm_1)
        ax0.pcolor(A_sfo_bin.T, cmap=cm_sfo_1)
        ax0.set_ylim(24, 0)
        ax0.grid(axis='both', color='lightgrey', linestyle='--', alpha=0.5)
        ax0.set_yticks(ticks=[0, 3, 6, 9, 12, 15, 18, 21, 24],)
        ax0.set_ylabel(lbls['ax0_ylbl'][lang])


        #subplot for annotations
        ax1     = plt.subplot(gs0[1])
        ax1.axis([0, 2, 0, 4.5])
        ax1.set_title(lbls['ax1_ttl'][lang], fontsize=16)
        ax1.text(0, 4.25, f"{lbls['ax1_ant1'][lang]} {year}", ha='left', va='top', fontsize=12)
        ax1.text(0, 3.75, f"$P_{{inst}}$ (corr.): " + rds(f"{BGP.Pe_aic:,.0f} kW", lang), ha='left', va='top', fontsize=12)
        ax1.text(0, 3.25, f"{lbls['ax1_ant3'][lang]}" + rds(f"{BGP.RT_adj} h", lang), ha='left', va='top', fontsize=12)
        ax1.text(0, 2.75, f"PQ (corr.): "  + rds(f"{BGP.PQ_adj}", lang) , ha='left', va='top', fontsize=12)
        ax1.text(0, 2.25, f"{lbls['ax1_ant5'][lang]}" + rds(f"{strokes_real:,.0f}", lang), ha='left', va='top', fontsize=12)
        ax1.text(0, 1.75, f"{lbls['ax1_ant6'][lang]}" + rds(f"{sfo_xtr:,.1f}", lang) + ' €/MWh', ha='left', va='top', fontsize=12)
        ax1.add_patch(rct((0, 1), 2, 0.4, color='magenta', alpha=0.75))
        ax1.text(0, 1.25, f"{lbls['ax1_ant7'][lang]}" + rds(f"{V_sfo_xtr.max():,.1f}", lang) + ' €/MWh',
                ha='left', va='top', fontsize=12)
        ax1.add_patch(rct((0, 0.5), 2, 0.4, color='cyan', alpha=0.75))
        ax1.text(0, 0.75, f"{lbls['ax1_ant8'][lang]}" + rds(f"{V_sfo_xtr.min():,.1f}", lang) + ' €/MWh',
                ha='left', va='top', fontsize=12)
        ax1.text(0, 0.25, f"{lbls['ax1_ant9'][lang]}" + rds(f"{abs_xtr:,.0f} €", lang) , ha='left', va='top', fontsize=12)

        ax1.axis('off')


        #subplot for extra earnings as bar chart
        ax2     = plt.subplot(gs0[2], sharex=ax0)
        prm_bar = ax2.bar(range(0,len(V_sfo_xtr)),V_prm_xtr, width=1, color='orange', align='edge')
        sfo_bar = ax2.bar(range(0,len(V_sfo_xtr)),V_sfo_xtr, width=1, color='dodgerblue', align='edge')
        ax2.set_ylim(math.floor(V_sfo_xtr.min()/5)*5, math.ceil(V_prm_xtr.max()/5)*5)
        ax2yl   = ax2.get_ylim()
        ax2.add_patch(rct((i_min_xtr,ax2yl[0]), 1 , ax2yl[1]-ax2yl[0], color='cyan', zorder=0))
        ax2.add_patch(rct((i_min_xtr,ax2yl[0]), 1 , ax2yl[1]-ax2yl[0], color='cyan', alpha=0.16))
        ax2.add_patch(rct((i_max_xtr,ax2yl[0]), 1 , ax2yl[1]-ax2yl[0], color='magenta', zorder=0))
        ax2.add_patch(rct((i_max_xtr,ax2yl[0]), 1 , ax2yl[1]-ax2yl[0], color='magenta', alpha=0.16))
        ax2.grid(axis='both', color='lightgrey', linestyle='--', alpha=0.5)
        ax2.set_xlabel(lbls['ax2_xlbl'][lang])
        ax2.set_ylabel(lbls['ax2_ylbl'][lang])
        ax2.set_xlim(0, len(V_sfo_xtr))
        ax2.legend((prm_bar[0], sfo_bar[0]), lbls['ax2_lgnd'][lang])


        #subplot for anual extra earnings
        ax3     = plt.subplot(gs0[3], sharey=ax2)
        ax3.bar('DPO', prm_xtr, color='orange', align='center')
        ax3.bar('SFO', sfo_xtr, color='dodgerblue', align='center')
        ax3.text('DPO', prm_xtr*0.9, str(round(prm_xtr,1)), va='top', ha='center', rotation=90)
        ax3.text('DPO', prm_xtr*1.1, '100 %', va='bottom', ha='center', rotation=90)
        ax3.text('SFO', sfo_xtr*0.9, str(round(sfo_xtr,1)), va='top', ha='center', rotation=90)
        ax3.text('SFO', sfo_xtr*1.1, str(round((sfo_xtr/prm_xtr*100),1)) + ' %',
                va='bottom', ha='center', rotation=90)
        ax3.set_ylabel(lbls['ax3_ylbl'][lang])
        ax3.yaxis.tick_right()
        ax3.yaxis.set_label_position("right")
        ax3.grid(axis='y', color='lightgrey', linestyle='--', alpha=0.5)

        plt.setp(ax0.get_xticklabels(), visible=False)


        if carpet=='show' and timespan!='show':
            plt.show()

        elif carpet[-4:]=='.png':
           plt.savefig(carpet, bbox_inches='tight')
           plt.close()


    ##timespan-plot for extreme days (best and least valuable days in terms of extra earnings)
    if timespan != False:

        #data preparation
        V_price_b   = A_prices[i_max_xtr]
        V_price_b   = np.append(V_price_b, V_price_b[23])
        V_price_l   = A_prices[i_min_xtr]
        V_price_l   = np.append(V_price_l, V_price_l[23])
        V_chpu_b    = A_sfo_bin[i_max_xtr] * BGP.Pe_aic
        V_chpu_b    = np.append(V_chpu_b, 0)
        V_chpu_l    = A_sfo_bin[i_min_xtr]* BGP.Pe_aic
        V_chpu_l    = np.append(V_chpu_l, 0)
        V_pr        = [BGP.Pe_brc] * 25
        V_gs_floor  = [BGP.GS_eleq] * 25
        V_gs_prfl_b = np.zeros(25)
        V_gs_prfl_l = np.zeros(25)
        x_rng       = range(0,25)
        prm_sfo_b   = V_sfo_xtr[i_max_xtr] / V_prm_xtr[i_max_xtr]
        prm_sfo_l   = V_sfo_xtr[i_min_xtr] / V_prm_xtr[i_min_xtr]


        for i in range(0, 25):
            if i==0:
                V_gs_prfl_b[i] = BGP.GS_eleq * SFOr.sfo_GS_start
                V_gs_prfl_l[i] = BGP.GS_eleq * SFOr.sfo_GS_start
            else:
                V_gs_prfl_b[i] = V_gs_prfl_b[i-1] + V_pr[i-1] - V_chpu_b[i-1]
                V_gs_prfl_l[i] = V_gs_prfl_l[i-1] + V_pr[i-1] - V_chpu_l[i-1]


        #create timespan figure
        fig2 = plt.figure(figsize=(16, 9))
        plt.suptitle(f"{lbls['tmsp_ttl'][lang]} {year}", fontsize=18)
        gs1  = gsp.GridSpec(2, 2,  width_ratios=(1, 1), height_ratios=(8, 1), wspace=0.05, hspace=0.1) #optional 2nd row for schedules
        ticks=[0, 3, 6, 9, 12, 15, 18, 21, 24]


        #subplot for best day load course
        ax4         = plt.subplot(gs1[0], facecolor='magenta')
        ax4.set_title(lbls['ax4_ttl'][lang] + format(prm_sfo_b, '.0%'), fontsize=16)
        ax_pr       = ax4.plot(x_rng, V_pr, ':', color='goldenrod', lw=2)
        ax_chpu     = ax4.bar(x_rng, V_chpu_b, color='dodgerblue', alpha=0.75, align='edge', width=0.9)
        ax_gs       = ax4.plot(x_rng, V_gs_prfl_b, '--', color='orange')
        ax_gs_fl    = ax4.plot(x_rng, V_gs_floor, color='red', alpha=0.5)

        ax4_sec     = ax4.twinx()
        ax_epex     = ax4_sec.step(x_rng, V_price_b, where='post', color='dimgray')
        #dummy plot for cover the epex price range of both days
        ax4_sec.step(x_rng, V_price_l, where='post', color='dimgray', alpha=0)

        ax4.grid(axis='both', color='lightgrey', linestyle='--', alpha=0.5)
        ax4.patch.set_alpha(0.05)
        ax4.set_xticks(ticks=ticks)
        ax4_sec.tick_params(axis='y', which='both', length=0)
        plt.setp(ax4_sec.get_yticklabels(), visible=False)

        ax4.set_ylabel(lbls['ax4_ylbl'][lang])
        ax4.legend((ax_pr[0], ax_gs[0], ax_gs_fl[0], ax_chpu[0], ax_epex[0]), lbls['ax4_lgd'][lang]
                    , loc='upper left')
        ax4.set_xlim(0, 24)


        #syncronise the two major y-axes by the function grid_sync_y()
        ax4, ax4_sec = yticks_align(ax4, ax4_sec )


        #subplot for least day load course
        ax5     = plt.subplot(gs1[1], facecolor='cyan', sharey=ax4)
        ax5.set_title(lbls['ax5_ttl'][lang] + format(prm_sfo_l, '.0%'), fontsize=16)
        ax5.plot(x_rng, V_pr, ':', color='goldenrod', lw=2)
        ax5.bar(x_rng, V_chpu_l, color='dodgerblue', alpha=0.75, align='edge', width=0.9)
        ax5.plot(x_rng, V_gs_prfl_l, '--', color='orange', lw=1.5)
        ax5.plot(x_rng, V_gs_floor, color='red', alpha=0.5)

        ax5_sec = ax5.twinx()
        ax5_sec.step(x_rng, V_price_l, where='post', color='dimgray')
        #dummy plot for cover the epex price range of both days
        ax5_sec.step(x_rng, V_price_b, where='post', color='dimgray', alpha=0)

        ax5.grid(axis='both', color='lightgrey', linestyle='--', alpha=0.5)
        ax5.patch.set_alpha(0.05)
        ax5.tick_params(axis='y', which='both', length=0)
        ax5.set_xticks(ticks=ticks)
        plt.setp(ax5.get_yticklabels(), visible=False)
        #ax5_sec.get_shared_y_axes().join(ax5_sec, ax4_sec)

        ax5_sec.set_ylabel(lbls['ax5_ylbl'][lang])
        ax5.set_xlim(0, 24)


        #subplot for comparing DPO and SFO schedule for best day load curve
        ax6     = plt.subplot(gs1[2], facecolor='magenta')
        ax6.bar(x_rng[:-1], np.where(A_prm[i_max_xtr]!=0,0.5,0), color='orange', alpha=0.75, align='edge', width=0.9)
        ax6.bar(x_rng, np.where(V_chpu_b!=0, 0.5, 0), color='dodgerblue', bottom=0.5, alpha=0.75, align='edge', width=0.9)

        ax6.set_xticks(ticks=ticks)
        ax6.grid(axis='both', color='lightgrey', linestyle='--', alpha=0.5)
        ax6.patch.set_alpha(0.05)
        ax6.tick_params(axis='y', which='both', length=0)
        ax6.set_ylim(0, 1)
        ax6.set_ylabel('DPO | SFO')
        plt.setp(ax6.get_yticklabels(), visible=False)
        ax6.set_xlim(0, 24)


        #subplot for comparing DPO and SFO schedule for least day load curve
        ax7     = plt.subplot(gs1[3], facecolor='cyan')
        ax7.bar(x_rng[:-1], np.where(A_prm[i_min_xtr]!=0,0.5,0), color='orange', alpha=0.75, align='edge', width=0.9)
        ax7.bar(x_rng, np.where(V_chpu_l!=0, 0.5, 0), color='dodgerblue', alpha=0.75, align='edge', bottom=0.5, width=0.9)

        ax7.set_xticks(ticks=ticks)
        ax7.grid(axis='both', color='lightgrey', linestyle='--', alpha=0.5)
        ax7.patch.set_alpha(0.05)
        ax7.tick_params(axis='y', which='both', length=0)
        ax7.set_ylim(0,1)
        plt.setp(ax7.get_yticklabels(), visible=False)
        ax7.set_xlim(0, 24)


        #syncronise the two major y-axes by the function grid_sync_y()
        ax5, ax5_sec = yticks_align(ax5, ax5_sec )


        if timespan=='show' and carpet!='show':
            plt.show()

        elif timespan[-4:]=='.png':
            plt.savefig(timespan, bbox_inches='tight')
            plt.close()

    #conditional presentation of both figures if 'show' is set for both parameters
    if carpet=='show' and timespan=='show':
        plt.show()


    return(SFOa)



def SFOr_4plots(SFOr, spec=None, lang='en', show=True, save=False):
    """ Function to generate a composite plot for 6 timeseries of the SFO-results

    Parameters:
    ==========

    SFOr:object
        smart-force-optimization results dataclass-object

    spec:str
        optional specification string

    lang:str
        language specifier 'en' | 'de'

    show:bool
        argument to show the plot

    save:bool / str
        argument to save the plot, if != None, its a file path
    """


    if spec == None:
        spec_str = ''
    else:
        spec_str = f' | {spec}'


    #bilingual labels dicitonary
    lbls    =   {   'ttl' : {'en' : f'Heat maps for timeseries{spec_str}', 'de' : f'Heat maps für Zeitreihen{spec_str}' },
                    'xl'  : {'en' : 'days', 'de' : 'Tage' },
                    'yl1' : {'en' : 'EPEX-spot', 'de' : 'EPEX-Spot' },
                    'yl2' : {'en' : 'CHPU', 'de' : 'BHKW' },
                    'yl3' : {'en' : 'heat load', 'de' : 'Wärmelast' },
                    'yl4' : {'en' : 'boiler', 'de' : 'Kessel' },
                                                    }

    fig = plt.figure(figsize=(12, 9))

    fig.suptitle(lbls['ttl'][lang], fontsize=14)

    #subplot for EPEX-prices
    ax1     = fig.add_subplot(411)
    npA1    = np.array(SFOr.EPEX_ppp).T

    ax1.pcolor(npA1, cmap='turbo')
    ax1.set_ylabel(lbls['yl1'][lang], fontsize=11)


    #subplot for CHPU schedule
    ax2 = fig.add_subplot(412)

    ax2.pcolor(SFOr.CHPU_Pe_schedule.T, cmap='binary')
    ax2.set_ylabel(lbls['yl2'][lang], fontsize=11)


    #subplot for heat load profile
    ax3 = fig.add_subplot(413)
    npA3    = np.array(SFOr.HSS_hlp).T

    ax3.pcolor(npA3, cmap='gist_heat')
    ax3.set_ylabel(lbls['yl3'][lang], fontsize=11)


    #subplot for peak load boiler
    if hasattr(SFOr, 'PLB_schedule') == True:
        ax4 = fig.add_subplot(414)

        ax4.pcolor(SFOr.PLB_schedule.T, cmap='hot_r')
        ax4.set_ylabel(lbls['yl4'][lang], fontsize=11)

        ax4.set_xlabel(lbls['xl'][lang])


    for ax in fig.axes:

        if ax != fig.axes[-1]:
            ax.xaxis.set_ticklabels([])

        ax.yaxis.set_ticks([0,6,12,18,24])
        ax.yaxis.set_ticklabels([0,6,12,18,24])
        ax.yaxis.tick_right()
        ax.invert_yaxis()


    plt.tight_layout()


    if show == True and (save == None or save == False):
        plt.show()

    elif show == True and (save != None or save != False):
        plt.savefig(save)
        plt.show()

    elif show == False and (esave != None or save != False):
        plt.savefig(save)
        plt.close(fig)

    else:
        pass