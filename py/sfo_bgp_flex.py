"""
Module to calculate schedules and technoeconomic parameters for flexible biogas plant (bgp) concepts
using the smart-force-optimisation (sfo) aproach. Contain functions for precallutaions for
gas-storage boundries and the schedule planning. The Module is embedded by the sfo_main.py - script.
"""

import os
import math
import numpy                    as np
import pandas                   as pd
import datetime                 as dt
import sqlite3                  as sq3
import matplotlib.pyplot        as plt
import matplotlib.dates         as mdt
import matplotlib.gridspec      as gsp

import sfo_db_setup             as dbs
import plant_classes            as pcl
import sfo_main                 as mm

from os.path import join        as opj
from scipy.linalg import solve  as slv

from matplotlib.patches     import Rectangle as rct
from dataclasses            import dataclass



class ConstraintError(Exception):
    """Exception raised for errors if sfo is getting non feasible constraints.

    Attributes:
        message     -- explanation of the error
    """

    def __init__(self, message):
        self.message = message


    def __str__(self):
        return (self.message)


@dataclass
class SFO_results:
    """Data class for smart-force-optimisation results
    """




def Insert_row_(row_number, DF, DF_ins):
    """ From https://www.geeksforgeeks.org/insert-row-at-given-position-in-pandas-dataframe/"""
    # Slice the upper half of the dataframe
    df1 = pd.DataFrame(DF[0:row_number])

    # Store the result of lower half of the dataframe
    df2 = pd.DataFrame(DF[row_number:])

    # Concat the two dataframes
    df_result = pd.concat([df1, DF_ins, df2], ignore_index=True)

    # # Reassign the index labels
    # df_result.reset_index()

    # Return the updated dataframe
    return df_result






def hss_precalc(hlp, HSS, hs_dur):
    """Precalculations for heat-supply-system.

    hlp:list

    HSS:dataclass

    hs_dur:


    """

    #define thermal power of the peak load boiler as equivalent of annual peak demand, flat list
    HSS.Pt_plb  = max([i for sl in hlp for i in sl])


    #calculate heat storage capacit in terms of n hours of P_plb
    HSS.HS_dur  = hs_dur
    HSS.HS_cap  = HSS.Pt_plb * hs_dur


    return(HSS)



def db_price_query(db_path:str, year:int):
    """
    Function to create a nested price list based on a database query.

    Parameters:
    -----------
    db_path:str
        path for the data base which contains the power prices

    year:int
        year for the price query on the data base

    Results:
    --------
    price_list:list
        nested list of prices (24h / 365d - 366d)
    """


    #db querry for valid schedules
    sql_query=("""
    SELECT price, dt_CET
    FROM v_price
    WHERE strftime('%Y', dt_CET)='{yr}'
    """).format(yr=year)

    con = sq3.connect(db_path)
    con_c = con.cursor()
    con_c.execute(sql_query)
    query=con_c.fetchall()


    price_list=[]
    sublist=[]
    for i in query:
        if i[1][-8:-6]=='23':
            sublist.append(i[0])
            if len(sublist)==23:
                sublist.insert(1, sublist[1])
            elif len(sublist)==25:
                sublist.pop(1)
            else:
                pass

            price_list.append(sublist)
            sublist=[]
        else:
            sublist.append(i[0])

    #change empty field by zeros
    i_ind=0
    j_ind=0
    for i in price_list:
        i_ind+=1
        for j in i:
            j_ind+=1
            if j=='':
                price_list[i_ind-1][j_ind-1]=0
            elif j=='nan':
                price_list[i_ind-1][j_ind-1]=0
        j_ind=0

    return(year, price_list)



def ppp_hlp_nl(ppp_path='csv_default', hlp_path='csv_default'):
    """Function to get default time series for power prices as well as for heat load profiles.


    Parameters:
    ==========
    ppp_path:str
        Path for the default  power-price-profile csv-file.
        If ppp_path == 'default' EPEX_2020 timeseries is choosen.

    hlp_path:str
        Path for the default  heat-load-profile csv-file.
        If hlp_path == 'default' heat load profile for 500kW thermal capacity and 40% CHP share
        for historical temperatures of DWD-station Leipzig Holzhausen in 2020 and a mixed
        standard heat load profile (SFH:MFH:TCS - 1:1:1) were choosen.

    Returns:
    ========
    ppp_nl:list
        Nested list (24h by 365/366 days) representing the power price profile.

    hlp_nl:list
        Nested list (24h by 365/366 days) representing the heat load profile. """



    #pirmary DataFrame for power price profile

    if ppp_path == 'csv_default':
        ppp_path     = opj(dbs.local_dir, 'csv', 'PPP_EPEX_day_ahead_2020.csv')

    DF_ppp  = pd.read_csv(ppp_path, sep=';')



    DF_ppp['pd_dt_UTC']     = pd.to_datetime(DF_ppp['dt_UTC'])
    DF_ppp['pd_dt_CET']     = pd.to_datetime(DF_ppp['dt_CET'])
    DF_ppp['diff_CET-UTC']  = (DF_ppp['pd_dt_CET'] - DF_ppp['pd_dt_UTC']) / np.timedelta64(1, 'h')
    DF_ppp['loc_CET-CEST']  = np.roll(DF_ppp['diff_CET-UTC'], 0) - np.roll(DF_ppp['diff_CET-UTC'], -1)


    #find and fill time series gap for switch from CEST to CET
    i_ST_plus               = DF_ppp.index[DF_ppp['loc_CET-CEST'] == -1].tolist()[0]
    ts_ST_plus              = DF_ppp['pd_dt_CET'].iloc[i_ST_plus] + pd.Timedelta(hours=1)
    ets_ST_plus             = int(ts_ST_plus.to_numpy().tolist()/1E9)
    dt_ST_plus              = dt.datetime.utcfromtimestamp(ets_ST_plus)
    dtstr_ST_plus           = dt.datetime.strftime(dt_ST_plus, '%Y-%m-%d %H:%M:%S')
    price_ST_plus           = float(DF_ppp['price'][i_ST_plus])
    pdei_ST_plus            = float(DF_ppp['pdei'][i_ST_plus])
    DF_ST_plus              = pd.DataFrame([[ets_ST_plus, 0, dtstr_ST_plus, price_ST_plus, pdei_ST_plus, 0, dtstr_ST_plus, 0, 0]], columns=list(DF_ppp.columns))
    DF_ppp                  = Insert_row_(i_ST_plus+1, DF_ppp, DF_ST_plus)


    #find time series doublette for switch from CET to CEST
    i_ST_minus              = DF_ppp.index[DF_ppp['loc_CET-CEST'] == 1].tolist()[0]
    DF_ppp                  = DF_ppp.drop(i_ST_minus)


    #create pivot reshape for 24 h by 365 days
    DF_ppp['pd_dt_CET']     = pd.to_datetime(DF_ppp['dt_CET'])
    DF_ppp['date']          = DF_ppp['pd_dt_CET'].dt.date
    DF_ppp['hour']          = DF_ppp['pd_dt_CET'].dt.hour
    DF_ppp_pivot            = DF_ppp.pivot(index='date', columns='hour', values='price')


    #create nested list from DataFrame
    ppp_nl                  = DF_ppp_pivot.values.tolist()



    #pirmary DataFrame for heat load profiles
    if hlp_path == 'csv_default':
        hlp_path     = opj(dbs.local_dir, 'csv', 'HLP_DWD02928_2020_mix_500_04.csv')

    DF_hlp  = pd.read_csv(hlp_path, sep=';')


    #create pivot reshape for 24 h by 365 days
    DF_hlp['pd_dt_CET']     = pd.to_datetime(DF_hlp['datetime'])
    DF_hlp['date']          = DF_hlp['pd_dt_CET'].dt.date
    DF_hlp['hour']          = DF_hlp['pd_dt_CET'].dt.hour
    DF_hlp_pivot            = DF_hlp.pivot(index='date', columns='hour', values='scaled')


    #create nested list from DataFrame
    hlp_nl                  = DF_hlp_pivot.values.tolist()


    return(ppp_nl, hlp_nl)



def optimization (BGP:object, HSS:object, ppp:list, hlp:list, GS_start=0.5, strk_max=3, HS_start=0.5,
                    xl_out=False, csv_out=False, sfo_db = dbs.sfo_sel_db_path):

    """
    Smart Force Optimisation by checking all possible schedules for maximum revenue.

    Parameters:
    -----------
    bgp_prm:dict
        parameter set for a model biogas plant

    gs_start:float
        gas storage starting level, realive to sotorage maximum

    strk_max.int
        maximum number of strokes for schedule optimisation for each day

    ppp:list
        power-price-profile as nested list of prices in the shape of 24h by n-days

    hlp:list
        heat-load-profile as nested list of prices in the shape of 24h by n-days

    GS_start:float
        gas storage starting level, realive to sotorage maximum

    strk_max:int
        Maximum number of strokes

    HS_start:float
        starting value of the heat storage

    xl_out:bool/str
        argument for suffix of the optional output of the pd.DataFrame as an xlsx-file,
        if xl_out==False (default state) the export is suppressed.

    Returns:
    --------
        SFOr as an sfo_main.SFO_results object @dataclass

            'bin_ary'   | Array for binary codes schedules (day-wise)
            'bin_str'   | List of string for binary codes schedules
            'pd_DF'     | Padas DataFrame with daily results
            'compt_t'   | computation time

    """

    #set timestamp for runtime measurement
    t0          = dt.datetime.now()

    #create empty SFO_results object dataclass
    SFOr            = SFO_results()


    #save some of the inputs to SFOr dataclass
    SFOr.sfo_GS_start   = GS_start
    SFOr.sfo_HS_start   = HS_start
    SFOr.sfo_strokes    = strk_max
    SFOr.EPEX_ppp       = ppp
    SFOr.HSS_hlp        = hlp


    #normalised (refering to installed capacity) gas storage boundries
    BGP.GS_norm_min     = 0 - (BGP.GS_eleq / BGP.Pe_aic) * GS_start
    BGP.GS_norm_max     = (BGP.GS_eleq / BGP.Pe_aic) - (BGP.GS_eleq / BGP.Pe_aic) * GS_start



    #db querry for valid schedules
    _SQL=("""
    SELECT bin24h, intervals ,ID FROM bincomb
    WHERE runtime = {rnt}
    AND intervals < {int}
    AND store_min > {min}
    AND store_max < {max}
    """).format(rnt=BGP.RT_adj, int=strk_max+1, min=BGP.GS_norm_min, max=BGP.GS_norm_max)

    con     = sq3.connect(dbs.sfo_sel_db_path)
    con_c   = con.cursor()
    con_c.execute(_SQL)
    query   = con_c.fetchall()


    if len(query) == 0:

        raise ConstraintError('No possible schedules for the given set of constraints.')


    #create nested list befor translate into a numpy array
    nl_query    = []
    for i in query:
        sublist = []
        for j in range(0,24):
            sublist.append(int(i[0][j]))
        nl_query.append(sublist)


    #array for possible binary combinations
    A_bincomb       = np.array(nl_query)

    SFOr.bc_population = A_bincomb.shape[0]


    #if HSS parameters where given compute opportunity costs for fullfilling heat obligations
    ##simplyfied simulation of a heat supply system (CHPU, peak load boiler, heat storage)
    if HSS != None:


        #calulate the fuel costs of the peak load boiler for constand thermal generation by the CHPU
        HS_level        = HSS.HS_cap
        HS_flc          = []
        PLB_lc          = []

        hlp_flat        = [i for sublist in hlp for i in sublist]

        #incremental modelling of the peak load boiler
        for i, h in enumerate(hlp_flat):

            #step I: calculate thermal residual load
            h_residual  = h - BGP.Pt_brc

            #step II: substract residual load from storage level @i-1
            HS_level    = HS_level - h_residual

            #step III: trim storage fill levels above maximum
            if HS_level > HSS.HS_cap:
                HS_level    = HSS.HS_cap

            #step IV: use peak load boiler to balance storage levels below lower limits and trim storage fill level below minimum
            if HS_level < 0:
                PLB_load    = -HS_level
                HS_level    = 0
            else:
                PLB_load    = 0

            HS_flc.append(HS_level)
            PLB_lc.append(PLB_load)

        HSS.PLB_loadc_ref   = PLB_lc
        HSS.Fuel_costs_ref  = sum(PLB_lc) / 1E3 / HSS.Pt_eta_PLB * HSS.Fuel_price

        #generate an array (365, 24) and a vector of the daily sums
        A_PLB_ref           = np.array(PLB_lc).reshape(len(hlp), 24)
        V_PLB_ref           = A_PLB_ref.sum(axis=1)
        V_fuel_drc          = V_PLB_ref / 1E3 / HSS.Pt_eta_PLB  * HSS.Fuel_price


        #daily heat generation by CHPU
        Wt_dg           = BGP.Pt_CHPU_aic * BGP.RT_adj

        #heat storage start capacity / level
        HS_cap_start    = HSS.HS_cap * HS_start

        #array for the heat load profile
        A_hlp           = np.array(hlp)


        #define shape numbers for array transformations
        shp_x           = A_hlp.shape[0]
        shp_y           = A_hlp.shape[1]
        shp_z           = A_bincomb.shape[0]


        #Simple heat supply simulation with priority for CHP and thermal storage and peak load
        #preparation of input arrays

        #2d and 3d demand array based on daily heat load profiles + vector of daily sums
        A3d_demand      = np.tile(A_hlp.T, (shp_z, 1, 1)).T
        V_demand_ds     = A_hlp.sum(1)

        #2d and 3d demand array based on possible generation patterns of A_bincomb
        A_th_gen        = A_bincomb * BGP.Pt_CHPU_aic
        A3d_th_gen      = np.tile(A_th_gen.T, (shp_x, 1, 1))


        #compute array of remaining generation and demand for each hour of all days
        #build 24x24 array for indicate remainung hours
        V_0 = np.full(24, 0)
        V_1 = np.full(24, 1)

        #upper triangular matrix for 24x24
        A_remain_utm        = np.triu(np.full((24,24),1))
        A_remain_utm        = np.append(A_remain_utm, [[0] * 24], 0)


        #compute cumulative remaining generation
        A3d_th_gen_remain   = np.dot(np.transpose(A3d_th_gen, (2,0,1)), A_remain_utm.T)
        A3d_th_gen_remain   = np.transpose(A3d_th_gen_remain, (1,2,0))


        #compute cumulative remaining demand
        A3d_demand_remain   = np.dot(np.transpose(A3d_demand, (2,0,1)), A_remain_utm.T)
        A3d_demand_remain   = np.transpose(A3d_demand_remain, (1,2,0))


        #compute minimum storage level depending on cumulative remaining generation and cumulative remaining demand
        A3d_storage_min     = np.where(A3d_th_gen_remain < HS_cap_start, HS_cap_start - A3d_th_gen_remain, 0)


        #compute minimum storage level depending on cumulative remaining generation and cumulative remaining demand
        A3d_storage_max     = np.where(A3d_demand_remain < (HSS.HS_cap - HS_cap_start),
                                        (HSS.HS_cap - HS_cap_start) + A3d_demand_remain, HSS.HS_cap)


        #3d zero arrays for residual load, storage and peakload
        A3d_residual    = np.zeros(A3d_th_gen.shape)
        A3d_storage     = np.zeros(A3d_th_gen.shape)
        A3d_peakload    = np.zeros(A3d_th_gen.shape)


        #iterative calculation as simple heat supply simulation for 24 hours of each day
        for i in range(0, A3d_storage.shape[1]):

            #use start value of the thermal storage in the first cycle
            if i == 0:
                A_storage_ti        = np.full((shp_x, shp_z), HS_cap_start)
            else:
                A_storage_ti        = A3d_storage[:, i-1, :]

            #step I: calculate thermal residual load
            A3d_residual[:, i, :]   = np.subtract(A3d_demand[:, i, :], A3d_th_gen[:, i, :])

            #step II: substract residual load from storage level @i-1
            A3d_storage[:, i, :]      = np.subtract(A_storage_ti, A3d_residual[:, i, :])

            #step III: trim storage fill levels above dynamic maximum
            A3d_storage[:, i, :]      = np.where(A3d_storage[:, i, :] > A3d_storage_max[:, i + 1, :],
                                                    A3d_storage_max[:, i + 1, :], A3d_storage[:, i, :])


            #step V: use peak load boiler to balance storage levels below lower limits (max plb load)
            A3d_peakload[:, i, :]     = np.where(A3d_storage[:, i, :] < A3d_storage_min[:, i + 1, :],
                                                    A3d_storage_min[:, i + 1, :] - A3d_storage[:, i, :], 0)

            #step VI: trim storage fill level below minimum plus buffer for the next day store min
            A3d_storage[:, i, :]      = np.where(A3d_storage[:, i, :] < A3d_storage_min[:, i + 1, :],
                                                    A3d_storage_min[:, i + 1, :], A3d_storage[:, i, :])


        #calculate fuel consumption and the related costs for each single hour
        A3d_fuel_c  = np.multiply(A3d_peakload, (1/HSS.Pt_eta_PLB * HSS.Fuel_price / 1E3))

        #build daily sums of fuel costs for n-schedules
        A_fuel_c    = A3d_fuel_c.sum(axis=1)


        #delete variables not further needed
        del A_th_gen
        del A3d_th_gen_remain
        del A3d_fuel_c
        del A3d_residual
        #del A3d_th_gen
        #del A3d_peakload
        #del A3d_storage


    else:
        #if no HSS is given, just generate a dummy array for fuel costs = 0
        A_fuel_c            = np.zeros((len(ppp), len(ppp[0])))
        #HSS.Fuel_costs_ref = 0


    #build array for prices of all days
    A_price         = np.array(ppp)

    #substitute nan values with zeros
    A_price         = np.nan_to_num(A_price)


    #matrix operations for finding optimal schedules
    #compute 3d-array of prices*bincombs specifically for 1MW (power price unit is €/MWh)
    A3d_rev_1MW     = A_price[:, :, np.newaxis] * A_bincomb.T

    #scale revenues to corrected installed capacity for given runtime
    A3d_rev_pi_corr = np.multiply(A3d_rev_1MW, BGP.Pe_aic / 1E3)

    #calculate daily sums of revenues
    A_rev_sums      = A3d_rev_pi_corr.sum(1)

    #calclulate epex-extra revenues
    A_price_sums    = np.tile(A_price.sum(1) * BGP.RT_adj/24 * BGP.Pe_aic/1E3, (A_rev_sums.shape[1], 1)).T
    A_rev_xtr       = A_rev_sums - A_price_sums

    #substract opportunity costs (difference) for fullfilling heat obligations (basic fuel costs - simulated fuel costs)
    if HSS != None:
        A_rev_net       = A_rev_xtr - (A_fuel_c  - V_fuel_drc.reshape(shp_x, 1))
    else:
        A_rev_net       = A_rev_xtr


    #compute maximum net revenue values
    V_rev_net_max   = A_rev_net.max(1)

    #locate optimal schedules for all days of one year
    A_opt_1_0       = np.equal(A_rev_net, np.tile(V_rev_net_max, (A_rev_net.shape[1],1 )).T)

    #find index of optimal schedule
    V2_indexes       = np.where(A_opt_1_0==True)


    #select just first hits, if for some reason in one day two schedules gain the same revenue
    V2_idx_unq, idl = np.unique(V2_indexes[0], return_index=True)
    V_indexes       = V2_indexes[1][idl]


    #Array of optimal binary comb's + add to results dataclass
    A_binopt                    = A_bincomb[V_indexes, :]
    SFOr.CHPU_binopt            = A_binopt
    SFOr.CHPU_Pe_schedule       = (A_binopt * BGP.Pe_aic).astype(int)


    #Vector of extra revenues
    SFOr.EPEX_extra_gross       = np.take(A_rev_xtr, V_indexes)
    SFOr.EPEX_extra_net         = np.take(A_rev_net, V_indexes)


    #Vector of realized intervals
    V_intervals         = ((np.roll(A_binopt, 1, axis=1)==A_binopt)==False).sum(1)/2
    SFOr.CHPU_strokes   = V_intervals


    #List of optimal binary combinations
    L_binopt=[]
    for i in range(0, len(A_binopt)):
        L_binopt.append(''.join([str(x) for x in (A_binopt[i].tolist())]))
    SFOr.binopt_strlist     = L_binopt    #expand results dataclass object


    #collect results for heat supply system
    if HSS != None:
        #Array for thermal generation of the CHPU
        A_th_gen_opt        = A3d_th_gen[np.arange(A3d_th_gen.shape[0]), :, V_indexes]
        SFOr.CHPU_Pt_gen    = A_th_gen_opt


        #Array for optimal peak load boiler schedules
        A2d_peakload        = A3d_peakload[np.arange(A3d_peakload.shape[0]),:,V_indexes]
        SFOr.PLB_schedule   = A2d_peakload

        #Array for optimal heat storage filling level course
        A2d_heat_storage    = A3d_storage[np.arange(A3d_storage.shape[0]),:,V_indexes]
        SFOr.HS_flc         = A2d_heat_storage

        #Array of daily gross costs for fuel of optimal schedules
        V_fuel_dgc          = A_fuel_c[np.arange(A_fuel_c.shape[0]),V_indexes]
        SFOr.PLB_fuel_dgc   = V_fuel_dgc

        #Array of daily net costs for fuel per day of optimal schedules (comapring to a CHPU band generation)
        SFOr.PLB_fuel_dnc   = V_fuel_dgc - V_fuel_drc.reshape(shp_x, 1)

        # #Array for residual thermal generation
        # A_residual          = A3d_residual[np.arange(A3d_residual.shape[0]), :, V_indexes]
        # SFOr.CHPU_Pt_res    = A_residual


    #Optional export of the DataFrame to Excel
    if xl_out != False:
        db_dir  = os.path.dirname(dbs.sfo_sel_db_path)
        xl_path = os.path.join(db_dir, 'xlsx', f'{dt.datetime.now():%Y_%M_%d-%H_%m}_sfo_{xl_out}.xlsx')
        DF_daily.to_excel(xl_path, sheet_name='SFO_results')


    #optional export of the CHPU-schedule to a csv-file
    if csv_out != False:
        db_dir  = os.path.dirname(dbs.sfo_sel_db_path)
        csv_path = os.path.join(db_dir, 'csv', f'{dt.datetime.now():%Y_%m_%d-%H_%m}_sfo_CHPU_{csv_out}.csv')
        np.savetxt(csv_path, SFOr.CHPU_Pe_schedule, delimiter=';', fmt='%.f')


    # #declare specifications for used BGP-object as well as HSS.object
    # SFOr.BGP_name   = BGP.name
    # if HSS == None:
    #     SFOr.HSS_name   = None
    # else:
    #     SFOr.HSS_name   = HSS.name

    #total computation time
    comp_t                  = dt.datetime.now()-t0
    SFOr.SFO_time            = comp_t       #expand results dictionary


    return(SFOr)