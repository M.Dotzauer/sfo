"""
Module to create the data bases of the smart-force-optimisation (sfo) aproach. Contain functions for
creating db-scheme, binary combinations and to generate a condensed data base for faster querries.
The Module is embedded by the sfo_main.py - script.
"""

import os
import sys
import inspect
import itertools
import sqlite3              as sq3
import datetime             as dt
import datetime             as dt
import numpy                as np
import psutil               as psu
import matplotlib.pyplot    as plt

from matplotlib.patches import Rectangle    as rct
from os.path import join                    as opj



##dir and path variables
local_dir           = os.path.dirname(__file__)     #local directory
db_dir              = opj(os.path.dirname(local_dir), 'db')
sfo_raw_db_path     = opj(db_dir, 'sfo_raw.db')  #path for raw database
sfo_sel_db_path     = opj(db_dir, 'sfo_sel.db')  #path for the database of selected data sets
db_sheme_path       = opj(db_dir, 'concept', 'sql', 'sfo_raw_db.sql')



def db_create():
    """ Create the database scheme and save the certain SQL-statement"""


    #create the SQL statement including a SQL-comment with a timestamp
    _SQL    = ("""
    /* COMMENT SECTION
    Scheme of database for A_bincombs optimisation for 24 h.
    date and time (CET) of creation: '{datetime}' .
    */

    DROP TABLE IF EXISTS "bincomb";
    CREATE TABLE IF NOT EXISTS "bincomb" (
        "ID"                INTEGER PRIMARY KEY UNIQUE ,
        "bin24h"            TEXT,
        "runtime"           INTEGER,
        "intervals"         REAL,
        "store_min"         REAL,
        "store_max"         REAL,
        "store_delta"        REAL);


    CREATE INDEX "main_selectors" ON "bincomb" (
        "runtime"	ASC,
        "intervals"	ASC,
        "store_min"	DESC,
        "store_max"	ASC);
        """).format(datetime=str(dt.datetime.now())[:19])


    #create database connection and execute the SQL statement
    con     = sq3.connect(sfo_raw_db_path)
    con_c   = con.cursor()
    con_c.executescript(_SQL)
    con.commit()
    con.close()
    print('SQL-script  for database scheme executed')


    #save SQL scheme to file
    fo  = open(db_sheme_path, mode='w')
    fo.write(_SQL)
    fo.close()
    print('SQL-file saved to: ' + db_sheme_path)



def bin_combs():
    """ Function to create the basic data and transfer it to the database.
    The script needed aproximately 12 GB of RAM under Win10.
    """


    #Check if installed nd availible RAM is sufficient for computation
    RAM_tot     = round(psu.virtual_memory().total/pow(2,30), 2)
    if RAM_tot < 11.9:
        print('Total installed RAM({RAM_tot:.2f} GB) is less than 12 GB, which is the minimal requirement for this application.')
        raise MemoryError
        #sys.exit()


    RAM_free    = round(psu.virtual_memory().available/pow(2,30), 2)
    if RAM_free < 11.2:
        print(f'Aivailable RAM({RAM_free:.2f} GB) is less than 11.2 GB, please close other applications and try again.')
        raise MemoryError
        #sys.exit()


    tn      = dt.datetime.now      #shortform for now()
    t00     = tn()


    #creating intial 2^24 combinations
    t0  = tn()
    print('create A_bincombs-combinations-array ("A_bincombs")')

    binitlist   = list(itertools.product([0,1],repeat=24))
    A_bincomb   = np.array(binitlist,dtype='int8')
    n_tot       = len(A_bincomb)

    print('create A_bincombs in')


    t0  = tn()
    #calculate runtime vector
    print('caculate runtimes (numer of -1-) per row')
    V_runt  = A_bincomb.sum(1)


    #calculate capacity factors (1/PQ) vector(V) depending on runtime
    print('calculate capacity factor vector')
    V_capf  = V_runt/24


    #calculalate half the numer of changing for neighbouring values (start / stop intervals)
    print('calculate number of intervals for rows')
    V_int   = ((np.roll(A_bincomb, 1, axis=1)==A_bincomb)==False).sum(1)/2


    #calculate storage array(A)
    print('caculate storage key values')
    A_stor  = np.zeros((n_tot, 24))    #nd array with zeros

    for i in range(0,24):
        if i==0:
            A_stor[:, i]=V_capf - A_bincomb[:, i]
        else:
            A_stor[:, i] = A_stor[:, i-1] + V_capf - A_bincomb[:, i]
    V_stor_min   = A_stor.min(1)
    V_stor_max   = A_stor.max(1)
    V_stor_delta = V_stor_max - V_stor_min

    print('process key values')


    #create nested list for data aggregation
    t0=tn()
    print('create nested list of aggregated data ("nl_bincom")')



    #delete A_bincombs-array to save RAM-space
    del A_bincomb
    del A_stor                  #delete storage-array to save RAM-space



    nl_bincom = []
    for i in range(0,n_tot):
        ID          = i                                           #ID / primary key for the database
        bin24h      = ''.join([str(x) for x in binitlist[i]])     #string of 24 integer values (1, 0)
        runt        = int(V_runt[i])                              #int of runtime
        intvls      = V_int[i]                                    #number of intervals per row / day
        stor_min    = V_stor_min[i]                               #minimum storage level
        stor_max    = V_stor_max[i]                               #maximum storage level
        stor_delta  = V_stor_delta[i]                             #storage delta
        nl_bincom.append([ID, bin24h, runt, intvls, stor_min, stor_max, stor_delta])


    print('create nl_bincom in: ' + str(tn()-t0)[:10])


    #transfer nested list to database by a SQL-batch-transfer (execute many)
    t0      = tn()
    print('start data transfer to db')
    con     = sq3.connect(sfo_raw_db_path)
    con_c   = con.cursor()
    con_c.executemany("""  INSERT OR REPLACE INTO bincomb (ID, bin24h, runtime, intervals, store_min, store_max, store_delta)
                            VALUES(?, ?, ?, ?, ?, ?, ?)""", nl_bincom)
    con.commit()
    con.close()

    print('total runtime for bincomb_basic: ' + str(tn()-t00)[:10])



def select_db(rt_min:int, rt_max:int, intv_max:int):
    """ Function to create a db with selected schedules for faster querries and a more compact file.

    Parameters:
    -----------

    rt_min:int
        runtime minimum

    rt_max:int
        runtime maximum

    intv_max:int
        number of maximum intervals
    """


    tn      = dt.datetime.now      #shortform for now()
    t0      = tn()                 #set t0 to now()


    #SQL statement for reset or create the selction database
    _SQL    = ("""
    DROP TABLE IF EXISTS "bincomb";
    CREATE TABLE IF NOT EXISTS "bincomb" (
        "ID"                INTEGER PRIMARY KEY UNIQUE ,
        "bin24h"            TEXT,
        "runtime"           INTEGER,
        "intervals"         REAL,
        "store_min"         REAL,
        "store_max"         REAL,
        "store_delta"       REAL);

    CREATE INDEX "main_selectors" ON "bincomb" (
        "runtime"	ASC,
        "intervals"	ASC,
        "store_min"	DESC,
        "store_max"	ASC);
        """).format(datetime=str(dt.datetime.now())[:19])


    #create database connection and execute the SQL statement
    con     = sq3.connect(sfo_sel_db_path)
    con_c   = con.cursor()
    con_c.executescript(_SQL)
    con.commit()

    print('SQL-script for database reset/creation was executed')


    #transfer selection from basic data base to selected data base
    print('start data transfer from raw db to selection db')


    _SQL    = ("""
    DELETE FROM main.bincomb;
    VACUUM;

    ATTACH DATABASE '{dbi}' AS 'source_db';

    INSERT OR REPLACE INTO main.bincomb
    SELECT * FROM source_db.bincomb
    WHERE   runtime     > {rtl}
    AND     runtime     < {rtu}
    AND     intervals   < {ntu};

    DETACH DATABASE 'source_db'
    """).format(dbi=sfo_raw_db_path, dbo=sfo_sel_db_path, rtl=rt_min-1 , rtu=rt_max+1 , ntu=intv_max+1)

    con_c.executescript(_SQL)
    con.commit()
    con.close()

    print('data transfer from raw db to selection db in '  + str(tn()-t0)[:10])



def ratio_plot(runtime:int, strokes_max:int, sym_gs_norm:float, lang='en'):
    """
    Function for creating a plot of the ratios of the basic population of 2^24 binary combinations,
    and selected subpopulations for given constraints.

    Parameters:
    -----------
    runtime:int
        defined runtime for CHPU

    strokes_max:int
        number of maximum tolerable strokes for the CHPU.

    sym_gs_norm:float
        symetric (start level is 0.5) gas storage amount as normalised value, which is the
        electrical equivalent referring to the installed capacity. """


    lbls    = { 'plt_ttl'   : { 'en' : 'Ratios of the amount of binary combinations for smart-force-optimisation',
                                'de' : 'Mengenverhältnisse der Binärkombinationen für die Smart-Force-Optimierung'},
                'P0_l1'     : { 'en' : 'P0 - basic population for: \n',
                                'de' : 'P0 - Basispopulation für: \n'},
                'P0_l2'     : { 'en' : 'all binary combinations',
                                'de' : 'alle Binärkombinationen'},
                'P1_l1'     : { 'en' : 'P1 - selected population for: \n',
                                'de' : 'P1 - selektierte Population für: \n'},
                'P1_l2'     : { 'en' : 'runtime = ',
                                'de' : 'Laufzeit = '},
                'P2_l1'     : { 'en' : 'P2 - selected population for: \n',
                                'de' : 'P2 - selektierte Population für: \n'},
                'P2_l3'     : { 'en' : 'AND strokes ≤ ' ,
                                'de' : 'UND Startzahl ≤ '},
                'P3_l1'     : { 'en' : 'P3 - selected population for: \n',
                                'de' : 'P3 - selektierte Population für: \n'},
                'P3_l4'     : { 'en' : 'AND 0 ≤ gas storage bounds ≤ ' ,
                                'de' : 'UND 0 ≤ Gasspeichergrenze  ≤ '}}


    #create database connection and execute the SQL statement
    con         = sq3.connect(sfo_raw_db_path)
    con_c       = con.cursor()


    #SQL query for count of rutime constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt} """).format(rt=runtime)
    con_c.execute(_SQL)
    qry_1       = con_c.fetchone()


    #SQL query for count of rutime and strokes_max constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1""").format(rt=runtime, st=strokes_max)
    con_c.execute(_SQL)
    qry_2       = con_c.fetchone()


    #SQL query for count of rutime, strokes_max and gas storage constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1
                    AND store_min > {gs_min}
                    AND store_max < {gs_max}
                    """).format(rt=runtime, st=strokes_max, gs_min=-sym_gs_norm/2, gs_max=sym_gs_norm/2)
    con_c.execute(_SQL)
    qry_3       = con_c.fetchone()


    #set the amounts of the different populations
    P0          = pow(2, 24)
    P1          = int(qry_1[0])
    P2          = int(qry_2[0])
    P3          = int(qry_3[0])


    #calculate edge lengths for squares
    P0_el       = pow(P0, 0.5)
    P1_el       = pow(P1, 0.5) / P0_el
    P2_el       = pow(P2, 0.5) / P0_el
    P3_el       = pow(P3, 0.5) / P0_el


    #calcultate the ratios for subpopulations referring to P0
    P0_rat      = P0/P0
    P1_rat      = P1/P0
    P2_rat      = P2/P0
    P3_rat      = P3/P0


    #label for P0
    P0_lbl      =   ( lbls['P0_l1'][lang]
                    + lbls['P0_l2'][lang] + ' ($2^{24}$) \n'
                    + '$n_{total}= $' +  format(P0, '.2E') + ' | ' +  format(P0_rat, '.2%') )

    #label for P1
    P1_lbl      =   ( lbls['P1_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + '$n_{total}= $' +  format(P1, '.2E') + ' | ' +  format(P1_rat, '.2%'))

    #label for P2
    P2_lbl      =   ( lbls['P2_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + '$n_{total}= $' +  format(P2, '.2E') + ' | ' +  format(P2_rat, '.2%'))

    #label for P3
    P3_lbl      =   ( lbls['P3_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + lbls['P3_l4'][lang] + str(sym_gs_norm) + '\n'
                    + '$n_{total}= $' + format(P3, '.2E') + ' | ' +  format(P3_rat, '.2%'))


    #create figure
    fig, ax = plt.subplots(figsize=(16, 9))
    plt.suptitle(lbls['plt_ttl'][lang], fontsize=18)
    ax = plt.subplot(111)
    ax.axis([0, 16, -0.05, 8.05])

    #create rectangular patches which areas representing the ratios of the populations
    ax.add_patch(rct((0, 0), 8, 8, color='black', fill=False, lw=0.5))
    ax.add_patch(rct((8 - P1_el, 0), P1_el, P1_el, color='black', fill=False, lw=0.5))
    ax.add_patch(rct((8 - P2_el, 0), P2_el, P2_el, color='black', fill=False, lw=0.5))
    ax.add_patch(rct((8 - P3_el, 0), P3_el, P3_el, color='black', fill=False, lw=0.5))

    #arrow properties
    arrow_prps = dict(arrowstyle='-', lw=0.5, shrinkA=5, shrinkB=5)

    #annotation for P0
    ax.annotate(None, xy=(8, 8), xytext=(11, 7), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P0_lbl, xy=(0,0), xytext=(11, 7), textcoords='data', fontsize=14, va='center')

    #annotation for P1
    ax.annotate(None, xy=(8, P1_el), xytext=(11, 5), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P1_lbl, xy=(0, 0), xytext=(11, 5), textcoords='data', fontsize=14, va='center')

    #annotation for P2
    ax.annotate(None, xy=(8, P2_el), xytext=(11, 3), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P2_lbl, xy=(0, 0), xytext=(11, 3), textcoords='data', fontsize=14, va='center')

    #annotation for P3
    ax.annotate(None, xy=(8, P3_el), xytext=(11, 1), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P3_lbl, xy=(0, 0), xytext=(11, 1), textcoords='data', fontsize=14, va='center')

    #turn axis of to just show the rectangles and the annotations
    ax.axis('off')

    #show the figure
    plt.show()



def ratio_log(runtime:int=8, strokes_max:int=3, sym_gs_norm:float=4, lang='en', show=True, save=False):
    """
    Function for creating a plot of the ratios of the basic population of 2^24 binary combinations,
    and selected subpopulations for given constraints.

    Parameters:
    -----------
    runtime:int
        defined runtime for CHPU

    strokes_max:int
        number of maximum tolerable strokes for the CHPU.

    sym_gs_norm:float
        symetric (start level is 0.5) gas storage amount as normalised value, which is the
        electrical equivalent referring to the installed capacity. """


    lbls    = { 'plt_ttl'   : { 'en' : 'Amounts of binary combinations for different constraints',
                                'de' : 'Binärkombinationen für verschiedene Nebenbedingungen'},
                'plt_xl'    : { 'en' : 'populations', 'de' : 'Populationen'},
                'plt_pyl'   : { 'en' : 'binary combinations (absolute)',
                                'de' : 'Binärkombinationen (absolut)'},
                'plt_syl'   : { 'en' : 'binary combinations (relative)',
                                'de' : 'Binärkombinationen (relativ)'},
                'P0_l1'     : { 'en' : 'P0 - basic population for: \n',
                                'de' : 'P0 - Basispopulation für: \n'},
                'P0_l2'     : { 'en' : 'all binary combinations',
                                'de' : 'alle Binärkombinationen'},
                'P1_l1'     : { 'en' : 'P1 - selected population for: \n',
                                'de' : 'P1 - selektierte Population für: \n'},
                'P1_l2'     : { 'en' : 'runtime = ',
                                'de' : 'Laufzeit = '},
                'P2_l1'     : { 'en' : 'P2 - selected population for: \n',
                                'de' : 'P2 - selektierte Population für: \n'},
                'P2_l3'     : { 'en' : 'AND strokes ≤ ' ,
                                'de' : 'UND Startzahl ≤ '},
                'P3_l1'     : { 'en' : 'P3 - selected population for: \n',
                                'de' : 'P3 - selektierte Population für: \n'},
                'P3_l4'     : { 'en' : 'AND 0 ≤ gas storage bounds ≤ ' ,
                                'de' : 'UND 0 ≤ Gasspeichergrenze  ≤ '}}


    #create database connection and execute the SQL statement
    con         = sq3.connect(sfo_raw_db_path)
    con_c       = con.cursor()


    #SQL query for count of rutime constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt} """).format(rt=runtime)
    con_c.execute(_SQL)
    qry_1       = con_c.fetchone()


    #SQL query for count of rutime and strokes_max constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1""").format(rt=runtime, st=strokes_max)
    con_c.execute(_SQL)
    qry_2       = con_c.fetchone()


    #SQL query for count of rutime, strokes_max and gas storage constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1
                    AND store_min > {gs_min}
                    AND store_max < {gs_max}
                    """).format(rt=runtime, st=strokes_max, gs_min=-sym_gs_norm/2, gs_max=sym_gs_norm/2)
    con_c.execute(_SQL)
    qry_3       = con_c.fetchone()


    #set the amounts of the different populations
    P0          = pow(2, 24)
    P1          = int(qry_1[0])
    P2          = int(qry_2[0])
    P3          = int(qry_3[0])

    populations = [P0, P1, P2, P3]
    x_locs = list(range(0,len(populations)))

    #calcultate the ratios for subpopulations referring to P0
    P0_rat      = P0/P0
    P1_rat      = P1/P0
    P2_rat      = P2/P0
    P3_rat      = P3/P0

    ratios_list = [P0_rat, P1_rat, P2_rat, P3_rat]


    #label for P0
    P0_lbl      =   ( lbls['P0_l1'][lang]
                    + lbls['P0_l2'][lang] + ' ($2^{24}$) \n'
                    + '$n_{abs}= $' +  format(P0, '.2E') + ' | ' +  format(P0_rat, '.2%') )

    #label for P1
    P1_lbl      =   ( lbls['P1_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + '$n_{abs}= $' +  format(P1, '.2E') + ' | ' +  format(P1_rat, '.2%'))

    #label for P2
    P2_lbl      =   ( lbls['P2_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + '$n_{abs}= $' +  format(P2, '.2E') + ' | ' +  format(P2_rat, '.2%'))

    #label for P3
    P3_lbl      =   ( lbls['P3_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + lbls['P3_l4'][lang] + str(sym_gs_norm) + '\n'
                    + '$n_{abs}= $' + format(P3, '.2E') + ' | ' +  format(P3_rat, '.2%'))


    #create figure
    fig, ax = plt.subplots(figsize=(9, 4.5))
    plt.suptitle(lbls['plt_ttl'][lang], fontsize=16)
    ax = plt.subplot(121)

    fs  = 12

    ax.bar(x_locs, populations)
    ax.set_yscale('log')
    ax.set_xlabel(lbls['plt_xl'][lang], fontsize=fs)
    ax.set_ylabel(lbls['plt_pyl'][lang], fontsize=fs)
    ax.set_xticks(x_locs)
    ax.set_xticklabels(['P0', 'P1', 'P2', 'P3'], fontsize=fs)
    ax.yaxis.set_tick_params(labelsize=fs)

    axs=ax.twinx()
    axs.bar(x_locs, ratios_list)
    axs.set_ylabel(lbls['plt_syl'][lang], fontsize=fs)
    axs.set_yscale('log')
    axs.yaxis.set_tick_params(labelsize=fs)

    axa = plt.subplot(122)
    axa.set_axis_off()


    #annotations props
    tc  = 'axes fraction'
    va  = 'top'


    #annotation for P0
    axa.annotate(P0_lbl, xy=(0,0), xytext=(0.2, 1.0), textcoords=tc, fontsize=fs, va=va)

    #annotation for P1
    axa.annotate(P1_lbl, xy=(0, 0), xytext=(0.2, 0.7), textcoords=tc, fontsize=fs, va=va)

    #annotation for P2
    axa.annotate(P2_lbl, xy=(0, 0), xytext=(0.2, 0.4), textcoords=tc, fontsize=fs, va=va)

    #annotation for P3
    axa.annotate(P3_lbl, xy=(0, 0), xytext=(0.2, 0.1), textcoords=tc, fontsize=fs, va=va)

    plt.tight_layout()


    #optional show or and sagve figure
    if show == True and save == Fasle:
        plt.show()

    elif show == True and save != False:
        plt.savefig(save)
        plt.show()

    elif show == False and save != False:
        plt.savefig(save)
        plt.close(fig)

    else:
        pass