"""
Main script / module for creating the basic data base and the condensed data base of binary combinations for
so called 'smart force optimisation' .
"""

#import extra libraries
import copy
import numpy                                as np
import pandas                               as pd
import sqlite3                              as sq3
from os import path                         as osp
from datetime import datetime               as dt
from matplotlib import pyplot               as plt
from matplotlib.colors import LightSource   as LS


#import smart force optimization modules
import sfo_db_setup                         as dbs
import sfo_bgp_flex                         as flx
import sfo_bgp_assess                       as ass
import plant_classes                        as pcl


#define directories and static paths
loc_dir     = osp.dirname(__file__)
par_dir     = osp.dirname(loc_dir)
png_dir     = osp.join(par_dir, 'png')



def create_db():
    """
    Function for initial database setup with default values.
    """


    inp = input('Database creation will use approximately 11.2 GB of free RAM and takes araound 6 minutes. \
                    \nTo proceed enter "1" or any other number or character for cacel: ')

    if inp=='1':


        #create
        dbs.db_create()

        #calculate binary combinations and wirte to database
        dbs.bin_combs()

        #generate database with condensed data set for
        #(minmum runtime, maximum, runtime, maximum start intervals)
        dbs.select_db(3, 18, 4)

    else:
        print('Process for create_db() canceld by user.')



def unit_aggregator(CHPU_kvl:list):

    """
    Function to build weighted averages of multiple CHPU-units for a biogas plant

    Parameters:
    ===========

    CHPU_kvl:list
        List of key values for several CHPUs
        [ [ rated electrical capacity, instaled electrical capacity, electrical efficiency, thermal efficiency], ...]

    """

    #define initial values of key values
    Pe_brc = 0
    Pe_bic = 0
    P_brti = 0
    Pt_bic = 0


    #loop over sublist items
    for sl in CHPU_kvl:
        Pe_brc  += sl[0]
        Pe_bic  += sl[1]
        P_brti  += sl[1] / sl[2]
        Pt_bic  += (sl[1] / sl[2]) * sl[3]

    #compute electrical and thermal efficiency: electrical / thermal capacity divided by rated thermal input (brti)
    Pe_eta      = Pe_bic / P_brti
    Pt_eta      = Pt_bic / P_brti

    #return results
    return([Pe_brc, Pe_bic, Pe_eta, Pt_eta])



def sfo_BGP_demo(lang='de', plot_scheme=False):
    """Sample script for creating a single biogas plant object (BGP) and plotting the plant scheme"""
    #create entity for a sample biogas plant
    #condierung the following key values: rated capacity, installed capacity, CHP-share, gas storage capacity


    #dictionary for two different CHPU set ups, conatainig 2 CHPU's each
    CHPU_set_ups = {'CHPU_su0'  : [ [1700, 2000, 0.39, 0.44],
                                    [0, 1825, 0.41, 0.41]    ],
                    'CHPU_su1'  : [ [1700, 2000, 0.39, 0.44],
                                    [0, 5650, 0.41, 0.41]    ] }


    for su in CHPU_set_ups:

        #generating CHPU-objetct like CHPU_su{No.}
        globals()[su] = unit_aggregator(CHPU_set_ups[su])


    #create biogas plant objects
    BGP_0           = pcl.biogas_plant( 'basic set up', 'Basis set-up', *CHPU_su0, 6E3)
    BGP_0.name      = 'BGA_0'

    BGP_1           = pcl.biogas_plant( 'extended gas storage', 'erweiterter Gasspeicher', *CHPU_su0, 12E3)
    BGP_1.name      = 'BGA_1'

    BGP_2           = pcl.biogas_plant( 'extended gas storage & double installed capacity', 'erweiterter Gasspeicher & Verdoppelung der inst. Leistung', *CHPU_su1, 12E3)
    BGP_2.name      = 'BGA_2'

    BGP_list        = [BGP_0, BGP_1, BGP_2]


    #plot plant schemes without showing them (save directly to png)
    if plot_scheme == True:
        for i, BGP in enumerate(BGP_list):
            png_path    = osp.join(png_dir, f'BGP{i}_plant_scheme_{lang}.png' )
            BGP.f_plant_scheme(lang=lang, show=False, save_png=png_path)


    #paths to power-price-profile (ppp) and heat-load-profile (hlp)
    ppp_path    = osp.join(par_dir, 'csv', 'PPP_EPEX_day_ahead_2020.csv')
    hlp_path    = osp.join(par_dir, 'csv', 'HLP_Jena_2020_1M8_075.csv')


    #create nested lists for power price profile and heat load profile
    ppp, hlp    = flx.ppp_hlp_nl(ppp_path=ppp_path, hlp_path=hlp_path)



    #create bare object for heat supply system
    for i, BGP in enumerate(BGP_list):

        globals()[f'HSS_{i}'] =    pcl.heat_supply_system(      ta_en           = f'BGP - {BGP_list[i].ta_en} | HSS basic set up',
                                                                ta_de           = f'BGP - {BGP_list[i].ta_de} | WVS Basis set-up',
                                                                Pt_CHPU_aic     = BGP_list[i].Pt_CHPU_aic,
                                                                Pt_PLB          = max([max(x) for x in hlp]),
                                                                Pt_eta_PLB      = 0.85,
                                                                HS_vol          = 0.8,
                                                                Fuel_type_en    = 'wood chips',
                                                                Fuel_type_de    = 'Holzhackschnitzel',
                                                                Fuel_price      = 30,
                                                                HLP_Pt_max      = max([max(x) for x in hlp]),
                                                                HLP_Wt_sum      = sum([sum(x) for x in hlp]),
                                                                HLP_name_en     = 'synthetic hlp - Jena 2020',
                                                                HLP_name_de     = 'Synthetisches WLP - Jena 2020')

        #compute capacity of the heat storage
        globals()[f'HSS_{i}'].f_HS_vol2cap()

        #add name to the HSS objects
        globals()[f'HSS_{i}'].name = f'WVS_{i}'

    #first set of HSS equal to None to do the assessment withour heat supply obligations
    HSS_list = [None] * len(BGP_list)

    for i, BGP in enumerate(BGP_list):
        HSS_list.append(globals()[f'HSS_{i}'])


    #create derivates of the HSS objetct with increased heat storage capacity
    for i in range(len(BGP_list), len(HSS_list)):
        n                   = i-3
        HSS                 = copy.deepcopy(globals()[f'HSS_{n}'])
        HSS.ta_en           = ' | expanded heat storage to 4h of peak load capacity'
        HSS.tap_de          = ' | erweiterte Wärmespeicherkapazität, auf 4h der Spitzenlastkesselkapazität'
        HSS.name            = globals()[f'HSS_{n}'].name + '_WS+'
        HSS.HS_cap          = globals()[f'HSS_{n}'].Pt_PLB * 4
        HSS.f_HS_cap2vol()

        HSS_list.append(HSS)


    #show plant scheme with technical key values
    if plot_scheme == True:
        for HSS in HSS_list:
            png_path    = osp.join(png_dir, f'{HSS.name}_plant_scheme_{lang}.png' )
            HSS.f_system_scheme(lang=lang, show=False, save_png=png_path)


    #optimisation loop and plotting of results.
    SFOr_list = []

    #mapping different heat supply systems to certain biogas plants
    HSS_BGP_map     = { 0:0, 1:1, 2:2,
                        3:0, 4:1, 5:2,
                        6:0, 7:1, 8:2}


    for h, HSS in  enumerate(HSS_list):

        BGP_i   = HSS_BGP_map[h]

        BGP     = BGP_list[BGP_i]
        HSS     = HSS_list[h]



        if HSS == None:
            if lang == 'en':
                HSS_label = 'HSS_NONE'
            else:
                HSS_label = 'WVS_ohne'

        else:
            HSS_label = HSS.name

        print(f'SFO for {BGP.name} and {HSS_label} ...')

        #try-except construction for catching non matching constraint Errors
        #and incremental increase maximum number of strokes
        strokesl    = [2, 3, 4, 5, 6]
        for s in strokesl:
            try:

                globals()[f'SFOr_{BGP.name}_{HSS_label}'] = flx.optimization(BGP, HSS, ppp, hlp, strk_max=s)

                break
            except:
                continue


        globals()[f'SFOr_{BGP.name}_{HSS_label}'].name   = f'SFOr{h}_{BGP.name}_{HSS_label}'

        SFOr_list.append(globals()[f'SFOr_{BGP.name}_{HSS_label}'])


        carpet_path     = osp.join(png_dir, f'SFOr_{BGP.name}_{HSS_label}_carpet.png')
        timespan_path   = osp.join(png_dir, f'SFOr_{BGP.name}_{HSS_label}_timespan.png')

        ass.SFO_analytics(globals()[f'SFOr_{BGP.name}_{HSS_label}'], BGP, year=2020, carpet=carpet_path, timespan=timespan_path, lang=lang)


    return(SFOr_list)



def SFOr_export(SFOr_list, export_dir=r'C:\user\sfo_directory'):
    """Function to export CHPU schedules to .csv-files"""


    for SFOr in SFOr_list:

        csv_path    = osp.join(export_dir, f'{SFOr.name}.csv')

        DF  = pd.DataFrame(SFOr.CHPU_Pe_schedule)

        DF.to_csv(csv_path, sep=';', header=False, index=False)