"""
Module for metanalytics of different sfo-aspects and plotting a directory tree, for documentation.
"""

import os
import pathlib
import sys

import sqlite3              as sq3
import matplotlib.pyplot    as plt


from matplotlib.patches import Rectangle    as rct
from graphviz           import Digraph      as dgp
from os.path            import join         as opj


#point to local graphviz library
os.environ["PATH"] += os.pathsep + os.path.realpath('C:/PA/Graphviz/bin')


#dir and path variables
local_dir           = os.path.dirname(__file__)             #local directory
parent_dir          = os.path.dirname(local_dir)
db_dir              = opj(os.path.dirname(local_dir), 'db')
wiki_dir            = opj(os.path.dirname(local_dir), 'concept', 'gitlab_wiki')
sfo_raw_db_path     = opj(db_dir, 'sfo_raw.db')             #path for raw database
sfo_sel_db_path     = opj(db_dir, 'sfo_sel.db')             #path for the database of selected data sets



def ratio_lin(runtime:int=8, strokes_max:int=3, sym_gs_norm:float=4, lang='en'):
    """
    Function for creating a plot of the ratios of the basic population of 2^24 binary combinations,
    and selected subpopulations for given constraints.

    Parameters:
    -----------
    runtime:int
        defined runtime for CHPU

    strokes_max:int
        number of maximum tolerable strokes for the CHPU.

    sym_gs_norm:float
        symetric (start level is 0.5) gas storage amount as normalised value, which is the
        electrical equivalent referring to the installed capacity. """


    lbls    = { 'plt_ttl'   : { 'en' : 'Ratios of the amount of binary combinations for smart-force-optimisation',
                                'de' : 'Mengenverhältnisse der Binärkombinationen für die Smart-Force-Optimierung'},
                'P0_l1'     : { 'en' : 'P0 - basic population for: \n',
                                'de' : 'P0 - Basispopulation für: \n'},
                'P0_l2'     : { 'en' : 'all binary combinations',
                                'de' : 'alle Binärkombinationen'},
                'P1_l1'     : { 'en' : 'P1 - selected population for: \n',
                                'de' : 'P1 - selektierte Population für: \n'},
                'P1_l2'     : { 'en' : 'runtime = ',
                                'de' : 'Laufzeit = '},
                'P2_l1'     : { 'en' : 'P2 - selected population for: \n',
                                'de' : 'P2 - selektierte Population für: \n'},
                'P2_l3'     : { 'en' : 'AND strokes ≤ ' ,
                                'de' : 'UND Startzahl ≤ '},
                'P3_l1'     : { 'en' : 'P3 - selected population for: \n',
                                'de' : 'P3 - selektierte Population für: \n'},
                'P3_l4'     : { 'en' : 'AND 0 ≤ gas storage bounds ≤ ' ,
                                'de' : 'UND 0 ≤ Gasspeichergrenze  ≤ '}}


    #create database connection and execute the SQL statement
    con         = sq3.connect(sfo_raw_db_path)
    con_c       = con.cursor()


    #SQL query for count of rutime constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt} """).format(rt=runtime)
    con_c.execute(_SQL)
    qry_1       = con_c.fetchone()


    #SQL query for count of rutime and strokes_max constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1""").format(rt=runtime, st=strokes_max)
    con_c.execute(_SQL)
    qry_2       = con_c.fetchone()


    #SQL query for count of rutime, strokes_max and gas storage constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1
                    AND store_min > {gs_min}
                    AND store_max < {gs_max}
                    """).format(rt=runtime, st=strokes_max, gs_min=-sym_gs_norm/2, gs_max=sym_gs_norm/2)
    con_c.execute(_SQL)
    qry_3       = con_c.fetchone()


    #set the amounts of the different populations
    P0          = pow(2, 24)
    P1          = int(qry_1[0])
    P2          = int(qry_2[0])
    P3          = int(qry_3[0])


    #calculate edge lengths for squares
    P0_el       = pow(P0, 0.5)
    P1_el       = pow(P1, 0.5) / P0_el
    P2_el       = pow(P2, 0.5) / P0_el
    P3_el       = pow(P3, 0.5) / P0_el


    #calcultate the ratios for subpopulations referring to P0
    P0_rat      = P0/P0
    P1_rat      = P1/P0
    P2_rat      = P2/P0
    P3_rat      = P3/P0


    #label for P0
    P0_lbl      =   ( lbls['P0_l1'][lang]
                    + lbls['P0_l2'][lang] + ' ($2^{24}$) \n'
                    + '$n_{total}= $' +  format(P0, '.1E') + ' | ' +  format(P0_rat, '.2%') )

    #label for P1
    P1_lbl      =   ( lbls['P1_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + '$n_{total}= $' +  format(P1, '.1E') + ' | ' +  format(P1_rat, '.2%'))

    #label for P2
    P2_lbl      =   ( lbls['P2_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + '$n_{total}= $' +  format(P2, '.1E') + ' | ' +  format(P2_rat, '.2%'))

    #label for P3
    P3_lbl      =   ( lbls['P3_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + lbls['P3_l4'][lang] + str(sym_gs_norm) + '\n'
                    + '$n_{total}= $' + format(P3, '.1E') + ' | ' +  format(P3_rat, '.2%'))


    #create figure
    fig, ax = plt.subplots(figsize=(16, 9))
    'meta_ratio_lin',
    plt.suptitle(lbls['plt_ttl'][lang], fontsize=18)
    ax = plt.subplot(111)
    ax.axis([0, 16, -0.05, 8.05])

    #create rectangular patches which areas representing the ratios of the populations
    ax.add_patch(rct((0, 0), 8, 8, color='black', fill=False, lw=0.5))
    ax.add_patch(rct((8 - P1_el, 0), P1_el, P1_el, color='black', fill=False, lw=0.5))
    ax.add_patch(rct((8 - P2_el, 0), P2_el, P2_el, color='black', fill=False, lw=0.5))
    ax.add_patch(rct((8 - P3_el, 0), P3_el, P3_el, color='black', fill=False, lw=0.5))

    #arrow properties
    arrow_prps = dict(arrowstyle='-', lw=0.5, shrinkA=5, shrinkB=5)

    #annotation for P0
    ax.annotate(None, xy=(8, 8), xytext=(11, 7), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P0_lbl, xy=(0,0), xytext=(11, 7), textcoords='data', fontsize=14, va='center')

    #annotation for P1
    ax.annotate(None, xy=(8, P1_el), xytext=(11, 5), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P1_lbl, xy=(0, 0), xytext=(11, 5), textcoords='data', fontsize=14, va='center')

    #annotation for P2
    ax.annotate(None, xy=(8, P2_el), xytext=(11, 3), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P2_lbl, xy=(0, 0), xytext=(11, 3), textcoords='data', fontsize=14, va='center')

    #annotation for P3
    ax.annotate(None, xy=(8, P3_el), xytext=(11, 1), xycoords='data', arrowprops=arrow_prps)
    ax.annotate(P3_lbl, xy=(0, 0), xytext=(11, 1), textcoords='data', fontsize=14, va='center')

    #turn axis of to just show the rectangles and the annotations
    ax.axis('off')

    #show the figure
    plt.show()



def ratio_log(runtime:int=8, strokes_max:int=3, sym_gs_norm:float=4, lang='en', title=True, show=True, save=False):
    """
    Function for creating a plot of the ratios of the basic population of 2^24 binary combinations,
    and selected subpopulations for given constraints.

    Parameters:
    -----------
    runtime:int
        defined runtime for CHPU

    strokes_max:int
        number of maximum tolerable strokes for the CHPU.

    sym_gs_norm:float
        symetric (start level is 0.5) gas storage amount as normalised value, which is the
        electrical equivalent referring to the installed capacity. """


    lbls    = { 'plt_ttl'   : { 'en' : 'Amounts of binary combinations for different constraints',
                                'de' : 'Binärkombinationen für verschiedene Nebenbedingungen'},
                'plt_xl'    : { 'en' : 'populations', 'de' : 'Populationen'},
                'plt_pyl'   : { 'en' : 'binary combinations (absolute)',
                                'de' : 'Binärkombinationen (absolut)'},
                'plt_syl'   : { 'en' : 'binary combinations (relative)',
                                'de' : 'Binärkombinationen (relativ)'},
                'P0_l1'     : { 'en' : 'P0 - basic population for: \n',
                                'de' : 'P0 - Basispopulation für: \n'},
                'P0_l2'     : { 'en' : 'all binary combinations',
                                'de' : 'alle Binärkombinationen'},
                'P1_l1'     : { 'en' : 'P1 - selected population for: \n',
                                'de' : 'P1 - selektierte Population für: \n'},
                'P1_l2'     : { 'en' : 'runtime = ',
                                'de' : 'Laufzeit = '},
                'P2_l1'     : { 'en' : 'P2 - selected population for: \n',
                                'de' : 'P2 - selektierte Population für: \n'},
                'P2_l3'     : { 'en' : 'AND strokes ≤ ' ,
                                'de' : 'UND Startzahl ≤ '},
                'P3_l1'     : { 'en' : 'P3 - selected population for: \n',
                                'de' : 'P3 - selektierte Population für: \n'},
                'P3_l4'     : { 'en' : 'AND 0 ≤ gas storage bounds ≤ ' ,
                                'de' : 'UND 0 ≤ Gasspeichergrenze  ≤ '}}


    #create database connection and execute the SQL statement
    con         = sq3.connect(sfo_raw_db_path)
    con_c       = con.cursor()


    #SQL query for count of rutime constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt} """).format(rt=runtime)
    con_c.execute(_SQL)
    qry_1       = con_c.fetchone()


    #SQL query for count of rutime and strokes_max constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1""").format(rt=runtime, st=strokes_max)
    con_c.execute(_SQL)
    qry_2       = con_c.fetchone()


    #SQL query for count of rutime, strokes_max and gas storage constrained combinations
    _SQL        = (""" SELECT count(ID)
                    FROM bincomb
                    WHERE runtime={rt}
                    AND intervals < {st} + 1
                    AND store_min > {gs_min}
                    AND store_max < {gs_max}
                    """).format(rt=runtime, st=strokes_max, gs_min=-sym_gs_norm/2, gs_max=sym_gs_norm/2)
    con_c.execute(_SQL)
    qry_3       = con_c.fetchone()


    #set the amounts of the different populations
    P0          = pow(2, 24)
    P1          = int(qry_1[0])
    P2          = int(qry_2[0])
    P3          = int(qry_3[0])

    populations = [P0, P1, P2, P3]
    x_locs = list(range(0,len(populations)))

    #calcultate the ratios for subpopulations referring to P0
    P0_rat      = P0/P0
    P1_rat      = P1/P0
    P2_rat      = P2/P0
    P3_rat      = P3/P0

    ratios_list = [P0_rat, P1_rat, P2_rat, P3_rat]


    #label for P0
    P0_lbl      =   ( lbls['P0_l1'][lang]
                    + lbls['P0_l2'][lang] + ' ($2^{24}$) \n'
                    + '$n_{abs}= $' +  format(P0, '.1E') + ' | ' +  format(P0_rat, '.2%') )

    #label for P1
    P1_lbl      =   ( lbls['P1_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + '$n_{abs}= $' +  format(P1, '.1E') + ' | ' +  format(P1_rat, '.2%'))

    #label for P2
    P2_lbl      =   ( lbls['P2_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + '$n_{abs}= $' +  format(P2, '.1E') + ' | ' +  format(P2_rat, '.2%'))

    #label for P3
    P3_lbl      =   ( lbls['P3_l1'][lang]
                    + lbls['P1_l2'][lang] + str(runtime) + '\n'
                    + lbls['P2_l3'][lang] + str(strokes_max) + '\n'
                    + lbls['P3_l4'][lang] + str(sym_gs_norm) + '\n'
                    + '$n_{abs}= $' + format(P3, '.1E') + ' | ' +  format(P3_rat, '.2%'))


    #create figure
    fig, ax = plt.subplots(figsize=(9, 4.5))

    if title == True:
        plt.suptitle(lbls['plt_ttl'][lang], fontsize=16)

    ax = plt.subplot(121)

    fs  = 12

    ax.bar(x_locs, populations)
    ax.set_yscale('log')
    ax.set_xlabel(lbls['plt_xl'][lang], fontsize=fs)
    ax.set_ylabel(lbls['plt_pyl'][lang], fontsize=fs)
    ax.set_xticks(x_locs)
    ax.set_xticklabels(['P0', 'P1', 'P2', 'P3'], fontsize=fs)
    ax.yaxis.set_tick_params(labelsize=fs)

    axs=ax.twinx()
    axs.bar(x_locs, ratios_list)
    axs.set_ylabel(lbls['plt_syl'][lang], fontsize=fs)
    axs.set_yscale('log')
    axs.yaxis.set_tick_params(labelsize=fs)

    axa = plt.subplot(122)
    axa.set_axis_off()


    #annotations props
    tc  = 'axes fraction'
    va  = 'top'


    #annotation for P0
    axa.annotate(P0_lbl, xy=(0,0), xytext=(0.2, 1.0), textcoords=tc, fontsize=fs, va=va)

    #annotation for P1
    axa.annotate(P1_lbl, xy=(0, 0), xytext=(0.2, 0.7), textcoords=tc, fontsize=fs, va=va)

    #annotation for P2
    axa.annotate(P2_lbl, xy=(0, 0), xytext=(0.2, 0.4), textcoords=tc, fontsize=fs, va=va)

    #annotation for P3
    axa.annotate(P3_lbl, xy=(0, 0), xytext=(0.2, 0.1), textcoords=tc, fontsize=fs, va=va)

    plt.tight_layout()


    #optional show or and sagve figure
    if show == True and save == False:
        plt.show()

    elif show == True and save != False:
        plt.savefig(save)
        plt.show()

    elif show == False and save != False:
        plt.savefig(save)
        plt.close(fig)

    else:
        pass